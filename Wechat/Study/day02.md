# 1.2开发工具的使用

这节主要讲解一下开发工具的使用
再小程序开发过程中
这个工具是陪伴我们的最忠实的伙伴了
今天就介绍一下这位伙伴
首先先看一下这一栏

第一个是 模拟
有了这个模拟器我们就能时时预览编写代码了

可以通过点击切换是否显示模拟器
编辑器就是写代码的这块区域
第三个是调试器
打开这里我们可以查看一些网络，
console 日志，以及代码报错信息
样式，类似浏览器的控制台

可视化，几乎用不到，可以不用看
云开发；这里就是云开发的服务器相关的配置及数据管理

这里是选择 当前项目的类型，默认小程序开发，当然可以切换到插件开发
这个是编译方式，选默认就可

这个是编译，点击编译会基于我们当前最心代码重新编译
预览 把当前最新编译的内容打包，便于在真机是查看效果
预览可以通过二维码，和自动编译两种形式
注意：想要预览需要在微信管理平台上添加为开发人员才能预览
真机调试：是把真实的手机当作模拟器来运行
通过这种方式可以处理一些特定机型报错问题
清缓存：作用是清除一些编译及数据存储
上传：把当前最新代码打包上传的管理平台
上传之后可以发布上线，可以预览调试
这里的预览只需要配置预览权限就可以
预览权限是要比开发权限低一级
通常都是给测试配置这个权限

版本管理：这里使用的是微信的git仓库
一般开发中不会使用这个
云测：这个是收费的功能
详情：这里是一些项目配置信息，
和性能分析，及设置。
在这里面可以做代码分析，
小程序主包最大只支持2M
在实际开发中主包存放内容会很谨慎
一般都会吧一些功能放到分包中开发
避免主包过大
可以通过这里的分析工具分析文件体积

上面还有一栏菜单栏
常用的是 设置 和 插件
这里大家简单看一下就可以了

这节视频就到这里了