派生表是从SELECT语句返回的虚拟表。
派生表类似于临时表，但在SELECT语句中使用派生表比临时表简单得多，因为它不需要创建临时表的步骤。

术语派生表和子查询通常可互换使用。当在SELECT语句的FROM子句中使用独立子查询时，我们将其称为派生表。

> 注意：独立子查询是一个子查询，它可以独立于包含它的语句执行。

与子查询不同，派生表必须要有别名，以便您稍后可以在查询中引用其名称。如果派生表没有别名，MySQL将发出以下错误：

Every derived table must have its own alias. 
以下说明了使用派生表的SQL语句：
```sql
SELECT column_list
FROM (SELECT column_list FROM table_1) derived_table_name;
WHERE derived_table_name.c1 > 0; 
```
括号中的即是派生表，使用派生表必须有一个别名