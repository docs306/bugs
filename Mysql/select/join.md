关系数据库由使用公共列（称为外键列）链接在一起的多个相关表组成。因此，从业务角度来看，每个表中的数据都是不完整的。

例如，在示例数据库中，我们有使用列orderNumber链接的orders和orderdetails表。<br/>
要获得完整订单的数据，需要查询orders和orderdetails两个表的数据。
这就是使用MySQL JOIN的原因。

MySQL连接是一种基于表之间的公共列的值在一个（自连接）或更多表之间链接数据的方法。

MySQL支持以下类型的连接：

- CROSS JOIN
- INNER JOIN
- LEFT JOIN
- RGIHT JOIN

要关联表可以使用CROSS JOIN，INNER JOIN，LEFT JOIN或RIGHT JOIN语句相应类型的联接。

join子句用在子句SELECT后面的FROM语句中。

> 注：MySQL不支持完全外连接。


为了便于您理解每种类型的连接，我们将使用具有以下结构的t1和t2表：
```sql
CREATE TABLE t1 (
    id INT PRIMARY KEY,
    pattern VARCHAR(50) NOT NULL
);
 
CREATE TABLE t2 (
    id VARCHAR(50) PRIMARY KEY,
    pattern VARCHAR(50) NOT NULL
);
```

### CROSS JOIN
CROSS JOIN使得从多个表行笛卡尔积。假设使用CROSS JOIN连接t1和t2表，结果集将包括t1表中行与 t2表中行的组合 。

要执行交叉连接，请使用CROSS JOIN以下语句中的子句：
```sql
SELECT t1.id, t2.id FROM t1
CROSS JOIN t2; 
```
以下显示了查询的结果集：
```
+----+----+
| id | id |
+----+----+
|  1 | A  |
|  2 | A  |
|  3 | A  |
|  1 | B  |
|  2 | B  |
|  3 | B  |
|  1 | C  |
|  2 | C  |
|  3 | C  |
+----+----+
```
如您所见，t1表中的每一行都与t2表中的行组合以形成笛卡尔积


### INNER JOIN
要形成一个INNER JOIN，必需要一个连接字段条件。INNER JOIN要求两个连接表中的行具有匹配的值。INNER JOIN返回的记录是通过两表连接字段相同的记录。

要连接两个表，请将  INNER JOIN第一个表中的每一行与第二个表中的每一行进行比较，以查找满足连接谓词的行对。每当通过匹配非NULL值来满足连接字段时，两个表中每个匹配行对的列值都包含在结果集中。

以下语句使用INNER JOIN子句连接t1和t2表：
```sql
SELECT t1.id, t2.id FROM t1
INNER JOIN t2 ON t1.pattern = t2.pattern; 
```
上图t1和t2表中的行必须在pattern列中具有相同的值才能包含在结果中。


### LEFT JOIN
与INNER JOIN类似，LEFT JOIN也需要连接条件。使用LEFT JOIN连接两个表时，会引入左表和右表的概念。

与INNER JOIN不同， LEFT JOIN返回左表中的所有行，包括满足连接条件行和不满足连接条件的行。对于与条件不匹配的行，NULL将出现在结果集中右表的列中。

以下语句使用LEFT JOIN子句连接t1和t2表：
```sql
SELECT t1.id, t2.id FROM t1
LEFT JOIN t2 ON t1.pattern = t2.pattern
ORDER BY t1.id; 
```
t1表中的所有行都包含在结果集中。对于t1表（左表）中没有表中任何匹配行t2（右表）的行，NULL用于表中的t2列


### RIGHT JOIN
RIGHT JOIN 也类似于LEFT JOIN表格的处理相反的情况。使用  RIGHT JOIN，右表（t2）中的每一行都将出现在结果集中。对于右表中没有左表（t1）中匹配行的行，左表( t1) 中的列显示NULL。

以下语句连接t1和t2表使用RIGHT JOIN：
```sql
SELECT t1.id, t2.id FROM t1
RIGHT JOIN t2 ON t1.pattern = t2.pattern
ORDER BY t1.id; 
```
右表（t2）中的所有行都显示在结果集中。对于右表（t2）中左表（t1）中没有匹配行的行，左表(t1) 的列显示NULL 。


### SELF JOIN
有一种特殊情况需要将表连接到自身，这称为自联接。

如果要将行与同一表中的其他行组合，可以使用自联接。要执行自联接操作，必须使用表别名来帮助MySQL在单个查询中区分左表和同一表的右表。

MySQL自联接，允许您通过使用INNER JOIN或LEFT JOIN子句将表连接到自身


