### SELECT的语法：
```sql
SELECT column_1, column_2, ...
FROM table_1
[INNER | LEFT |RIGHT] JOIN table_2 ON conditions
WHERE conditions
GROUP BY column_1
HAVING group_conditions
ORDER BY column_1
LIMIT offset, length; 
```
以下是SELECT的使用及相关的组合关键字：

- SELECT 后跟逗号分隔列或星号（*）列表，表示返回所有列。
- FROM 指定要查询数据的表或视图。
- JOIN 根据特定的连接条件从其他表中获取相关数据。
- WHERE 子句过滤结果集中的行。
- GROUP BY  子句将一组行分组到组中，并在每个组上应用聚合函数。
- HAVING  子句根据GROUP BY子句定义的组过滤组。
- ORDER BY  子句指定用于排序的列的列表。
- LIMIT 约束返回的行数。

在查询语句中要求`必须`包含 `SELECT` 和 `FROM` ,其它都是可选项。

> SELECT语句 允许您通过在 SELECT子句 中指定 逗号 分隔列 的列表来查询表的 部分数据。

例如，如果您只想查看员工的名字，姓氏和职位，则使用以下查询：
```sql
SELECT lastname, firstname, jobtitle FROM employees; 
```
即使 employees表有很多列，SELECT语句 只返回表中所有行的三列数据

如果要获取表中所有列的数据employees，可以列出SELECT子句中的所有列名。或者您只需使用星号（*）表示您希望从表的所有列获取数据，如下面的查询：

```sql
SELECT * FROM employees; 
```

在实例使用中不推荐使用（*）原因如下：

- 星号（*）会返回可能不使用的列中。这会浪费MySQL数据库产生不必要的I/O磁盘和网络流量。
- 明确指定列，结果集更易于预测且更易于管理。当使用星号（*）有人通过添加更多列更改表时，将得到与预期不同的结果集。
- 使用星号（*）可能会将敏感信息暴露给未经授权的用户。


### MySQL 别名
MySQL支持两种别名，称为列别名和表别名.

有时列的名称是不太好理解和记忆的，使得查询的输出很难理解。要为列提供描述性名称，请使用列别名
```sql
SELECT column_1 AS descriptive_name FROM table_name; 
/** 如果别名包含空格，则必须按以下方式引用它（使用（`）） */
SELECT column_1 AS `descriptive name` FROM table_name;
```

查询名字和姓氏并将它们组合在一起以生成全名。CONCAT_WS函数用于连接两个字符串。

```sql
SELECT CONCAT_WS(', ', lastName, firstname) FROM employees;
/**  使用别名 */
SELECT CONCAT_WS(', ', lastName, firstname) AS `Full name` FROM employees;
```

可以使用ORDER BY，GROUP BY和HAVING子句中来引用列的别名。
```sql
SELECT CONCAT_WS(', ', lastName, firstname) `Full name`
FROM employees ORDER BY `Full name`; 

SELECT orderNum `Order no`,SUM(priceEach * quantity) total
FROM orders GROUP BY `Order no` HAVING total > 60000;
```

1. MySQL表别名
可以使用别名为表提供不同的名称。使用AS关键字为表分配别名，如下：

> table_name AS table_alias 

表的别名称为表别名。与列别名一样，AS关键字是可选的，因此您可以省略它。
经常在INNER JOIN，LEFT JOIN，self JOIN子句和子查询的语句中使用表别名。

```sql
SELECT customer, COUNT(o.orderNum) total
FROM customers c
INNER JOIN orders o ON c.customerNo = o.customerNo
GROUP BY customer
ORDER BY total DESC;
```


### DISTINCT 子句

从表中查询数据时，可能会出现重复的行。要删除这些重复的行，在SELECT子句中请使用 `DISTINCT` 语句。

使用DISTINCT 使用如下：
```sql
SELECT DISTINCT column1 FROM table_name WHERE where_conditions; 
```
1. DISTINCT 和 NULL值
如果字段中有NULL值并且要对该列使用DISTINCT 子句，则MySQL仅保留一个NULL值，因为DISTINCT 子句将所有NULL值视为相同的值。

2. DISTINCT 多列
您可以将DISTINCT 子句与多个列一起使用。
在这种情况下，MySQL使用这些列中的值组合来确定结果集中行的唯一性。

```sql
SELECT DISTINCT state,city FROM customers 
WHERE state IS NOT NULL ORDER BY state,city; 
```

3. DISTINCT语句与GROUP BY 语句
如果在不使用聚合函数的情况下在SELECT语句中使用GROUP BY子句，则GROUP BY子句的行为与DISTINCT 子句类似。

下面这个sql效果一致
```sql
SELECT state FROM customers GROUP BY state; 

SELECT DISTINCT state FROM customers;
```

一般来说，DISTINCT 语句是GROUP BY语句的特殊情况。之间的区别DISTINCT 条款，GROUP BY条款是GROUP BY条款排序结果集，而DISTINCT 条款不。

如果将 ORDER BY 子句添加到使用 DISTINCT 子句的语句中，则结果集将进行排序，并且与使用 GROUP BY 子句的语句返回的结果集相同
```sql
SELECT DISTINCT state FROM customers ORDER BY state; 
```

4. DISTINCT和聚合函数
您可以将DISTINCT子句与聚合函数（如SUM，AVG和COUNT ） 一起使用，将聚合函数应用于结果集之前删除重复的行。

例如，要计算客户的不同的州数：
```sql
SELECT COUNT(DISTINCT state) FROM customers WHERE country = 'CN'; 
```

5. DISTINCT 和 LIMIT子句
如果您将DISTINCT 子句与LIMIT子句一起使用，MySQL会在找到LIMIT子句中指定的唯一行数时立即停止搜索。

以下是在 customers表中查询选择前五个非null唯一州。
```sql
SELECT DISTINCT state FROM customers 
WHERE state IS NOT NULL LIMIT 5; 
```


### ORDER BY 子句

在使用SELECT语句从表中查询数据时，结果集不按任何顺序排序。如果要对结果集进行排序，请使用该ORDER BY 子句。

ORDER BY 允许的操作：

- 按单列或多列对结果集进行排序。
- 按升序或降序对不同列的结果集进行排序。

ASC代表升序 和 DESC代表下降。
ORDER BY 如果未指定 ASC 或 DESC 显式，则子句按`升序`对结果集进行排序.

ORDER BY 子句的语法：
```sql
SELECT column1, column2,... FROM table_name
ORDER BY column1 [ASC|DESC], column2 [ASC|DESC],...
```

如果要按姓氏降序排序联系人，按升序排序第一个名称，在相应列中指定两者 DESC，ASC 如下：
```sql
SELECT lastname, firstname FROM customers
ORDER BY lastname DESC, firstname ASC; 
```

1. ORDER BY按表达式排序实例
ORDER BY 子句还允许您根据表达式对结果集进行排序。

查询从orders表中选择订单。计算每行项目的小计，并根据订单号，订单行号和小计进行排序
为了让查询更具可读性, 可以使用列别名, 把小计列命名为“subtotal”
```sql
SELECT orderNum, orderLineNum, quantity * priceEach AS subtotal
FROM orders ORDER BY orderNum, orderLineNum, subtotal; 
```

2. ORDER BY 使用自定义排序顺序
ORDER BY 子句可以使用 FIELD()  函数为列中的值定义自己的自定义排序顺序。

如果要按以下顺序基于以下状态对订单进行排序：
```
In Process
Canceled
Resolved
Shipped
```
您可以使用 FIELD 函数将这些值映射到数值列表，并使用这些数字进行排序;
```sql
SELECT orderNum, status FROM orders
ORDER BY FIELD(status, 'In Process', 'Cancelled', 'Resolved','Shipped');
```


### LIMIT 子句
SELECT语句中使用LIMIT 子句来约束结果集中的行数。LIMIT子句接受一个或两个参数。两个参数的值必须为零或正整数。

以下说明了LIMIT带有两个参数的子句语法：
```sql
SELECT column1,column2,...
FROM table_name LIMIT offset,count;
```
LIMIT句参数：

offset第一行的偏移规定返回。offset第一行的是0，而不是1。
count指定要返回行的最大数目。

1.LIMIT获取前N行
您可以使用LIMIT子句 从表中选择第N 行，如以下查询所示：
```sql
SELECT column1,column2,... FROM table_name LIMIT N; 
```


2. LIMIT获取最高和最低值
LIMIT经常与ORDER BY一起使用。
使用ORDER BY 子句根据特定条件对结果集进行排序，然后使用 LIMIT子句查找最低或最高值。
```sql
/** 额度最高的前五位客户 */
SELECT customerNo,name,level FROM customers
ORDER BY creditlimit DESC LIMIT 5; 
```

3. LIMIT获得第N个最高值
MySQL中最棘手的问题之一是选择结果集中的第N个最高值，例如，选择第二个（或第n个）最昂贵的产品，您无法使用MAX或MIN无法回答这些产品。但是，可以使用MySQL LIMIT来解决这些问题。

按降序对结果集进行排序。
使用LIMIT条款获得第n个最昂贵的产品。

常见的查询模式如下：
```sql
SELECT select_list FROM table_name
ORDER BY sort_expression DESC LIMIT nth-1, count; 
```


### GROUP BY子句
GROUP BY子句按行或表达式的值将一组行分组为一组摘要行。

GROUP BY子句为每个组返回一行。换句话说，它减少了结果集中的行数。

GROUP BY子句经常使用与聚合函数，例如SUM，AVG，MAX，MIN，和COUNT。
SELECT子句中显示的聚合函数提供有关每个组的信息。

GROUP BY子句语法：
```sql
SELECT c1, c2,...,agr_function(ci) FROM table_name
WHERE where_conditions GROUP BY c1 , c2,...; 
```
GROUP BY 必须出现在 FROM和 WHERE之后。继GROUP BY关键字是要作为标准组行用逗号分隔的列或表达式的列表。


将订单状态的值分组到子组中，请使用GROUP BY带有status列的子句作为以下查询：
```sql
SELECT status FROM orders GROUP BY status; 
```
GROUP BY 子句返回唯一status值的值。它的工作方式与 DISTINCT 运算符类似，如下：
```sql
SELECT DISTINCT status FROM orders; 
```

1. GROUP BY 和聚合函数
聚合函数允许您执行一组行的计算，并返回一个值。GROUP BY子句通常与聚合函数一起使用以执行计算并为每个子组返回单个值。

例如想知道每个状态中的订单数，可以使用带有GROUP BY子句的COUNT函数，如下：
```sql
SELECT status, COUNT(*) FROM orders GROUP BY status; 
```

要按状态得到的所有订单的总金额，使用INNSER JOIN 关联orders表与orderdetails表和使用SUM功能来计算总量。如下：
```sql
SELECT status, SUM(quantity * price) AS amount
FROM orders
INNER JOIN orderdetails USING (orderNum)
GROUP BY status;
```

同样，以下查询返回订单号和每个订单的总金额。
```sql
SELECT orderNum, SUM(quantity * price) AS total
FROM orderdetails GROUP BY orderNum;
```

2. GROUP BY 带有表达式实例
还可以按表达式对行进行分组。以下查询每年的总销售额。
```sql
SELECT YEAR(orderDate) AS year, SUM(quantity * price) AS total
FROM orders
INNER JOIN orderdetails USING (orderNum)
WHERE status = 'Shipped'
GROUP BY YEAR(orderDate); 
```
在示例中，使用YEAR函数从订单日期（orderDate）中提取年份数据。

我们仅包含shipped在总销售额中具有状态的订单。

注意，SELECT子句中出现的表达式必须与GROUP BY子句中的表达式相同。

3. GROUP BY和HAVING子句实例
要过滤GROUP BY子句返回的组，请使用 HAVING 子句。如下查询使用HAVING子句选择2003年后的年份总销售额。
```sql
SELECT YEAR(orderDate) AS year,SUM(quantity * price) AS total
FROM orders
INNER JOIN orderdetails USING (orderNum)
WHERE status = 'Shipped'
GROUP BY year HAVING year > 2013; 
```

4. GROUP BY子句：MySQL与标准SQL

- 标准SQL不允许您在GROUP BY子句中使用别名，但MySQL支持此功能。

- MySQL还允许按升序或降序对组进行排序，而标准SQL则不然。默认顺序为升序。

例如，如果要按状态获取订单数并按降序对状态进行排序，则可以使用GROUP BY子句DESC作为以下查询：
```sql
SELECT status, COUNT(*) FROM orders GROUP BY status DESC; 
```
> 注意：在GROUP BY 子句中使用DESC对字段 status按降序排序。还可以指定 ASC 按升序排序。


### HAVING子句
SELECT语句中使用 HAVING 子句来指定一组行或聚合的过滤条件。

HAVING子句通常与GROUP BY 子句一起使用，以根据指定的条件过滤组。

如果GROUP BY省略HAVING子句，则子句的行为类似于 WHERE子句
> 注意：HAVING子句将过滤条件应用于每组行，而WHERE子句将过滤条件应用于每个单独的行。

使用GROUP BY子句获取订单号，每个订单销售的商品数量以及每个订单的总销售额：

```sql
SELECT orderNum,SUM(quantity) AS count,SUM(priceeach*quantity) AS total
FROM orderdetails GROUP BY ordernumber; 
```
可以使用以下HAVING子句查找总销售额大于1000哪个订单：
```sql
SELECT orderNum,SUM(quantity) AS count,SUM(priceeach*quantity) AS total
FROM orderdetails GROUP BY ordernumber
HAVING total > 1000; 
/** 还可以使用逻辑运算符（如OR和AND）在HAVING中构造复杂条件 */
HAVING total > 1000 AND count > 600;
```
只有在将GROUP BY子句与HAVING子句一起使用才能生成高级报告的输出时，子句才有用


### ROLLUP子句
使用MySQL ROLLUP子句生成小计和总计。

按产品系列和年份汇总的订单值。
```sql
CREATE TABLE sales
SELECT
    productLine,
    YEAR(orderDate) orderYear,
    quantity * priceEach orderValue
FROM
    orderDetails
        INNER JOIN
    orders USING (orderNum)
        INNER JOIN
    products USING (productCode)
GROUP BY
    productLine ,
    YEAR(orderDate); 

/** 所有行sales */
SELECT * FROM sales;
```

1. ROLLUP概述
分组集是要分组的一组列。

例如，以下查询创建一个表示的分组集(productline)
```sql
SELECT productLine, SUM(orderValue) totalOrderValue
FROM sales GROUP BY productLine; 
```
以下查询创建一个空分组集，表示为()：
```sql
SELECT SUM(orderValue) totalOrderValue FROM sales; 
```
如果要在一个查询中一起生成两个或多个分组集，使用UNION ALL按如下方式使用运算符：
```sql
SELECT productLine, SUM(orderValue) totalOrderValue
FROM sales GROUP BY productLine 
UNION ALL
SELECT NULL, SUM(orderValue) totalOrderValue
FROM sales; 
```

因为UNION ALL要求所有查询具有相同数量的列，所以我们在第二个查询的选择列表中添加了NULL满足此要求的内容。

将NULL在productLine列中标识量线的总计。

此查询能够按产品线以及总计行生成总订单值。但是，它有两个问题：

- 查询非常冗长。
- 查询的性能可能不太好，因为数据库引擎必须在内部执行两个单独的查询并将结果集合并为一个。
要解决这些问题，可以使用ROLLUP子句。

ROLLUP子句是GROUP BY子句的扩展，具有以下语法：
```sql
SELECT select_list FROM table_name
GROUP BY c1, c2, c3 WITH ROLLUP; 
```
根据子句中ROLLUP指定的列或表达式生成多个分组集GROUP BY。
```sql
SELECT productLine, SUM(orderValue) totalOrderValue
FROM sales GROUP BY productline WITH ROLLUP;
```

ROLLUP子句不仅生成小计，还生成订单值的总计。

如果在GROUP BY子句中指定了多个列，则ROLLUP子句假定输入列之间存在优先级结构。

例如：
```
GROUP BY c1, c2, c3 WITH ROLLUP 
```
ROLLUP假设有以下优先级：
```
c1 > c2 > c3 
```
它会生成以下分组集：
```
(c1, c2, c3)
(c1, c2)
(c1)
() 
```
如果您在GROUP BY子句中指定了两列：
```
GROUP BY c1, c2 WITH ROLLUP 
```
然后ROLLUP生成以下分组集：
```
(c1, c2)
(c1)
() 
```
如下查询：
```sql
SELECT productLine,orderYear,SUM(orderValue) totalOrderValue
FROM sales GROUP BY productline, orderYear 
WITH ROLLUP; 
```

ROLLUP在每次产品线更改时生成小计行，在结果结束时生成总计。

在这种情况下的层次结构是：
```
productLine > orderYear 
```

如果您反转层次结构，如：
```sql
SELECT orderYear,productLine,SUM(orderValue) totalOrderValue
FROM sales GROUP BY orderYear,productline
WITH ROLLUP; 
```

ROLLUP每年更改时生成小计，并在结果集的末尾生成总计。

示例中的层级结构是：
```
orderYear > productLine 
```

1. GROUPING() 函数
要检查NULL结果集中是否表示小计或总计，请使用GROUPING()函数。

当NULL在超级聚合时，GROUPING()函数返回1 ，否则返回0。

GROUPING()函数可用于查询，HAVING子句和（从MySQL 8.0.12开始）ORDER BY子句。
```sql
SELECT 
    orderYear,
    productLine, 
    SUM(orderValue) totalOrderValue,
    GROUPING(orderYear),
    GROUPING(productLine)
FROM sales
GROUP BY orderYear,productline
WITH ROLLUP;
```

当NULL在orderYear 列发生在行聚合时， GROUPING(orderYear)时返回1 ，否则为0。

类似地，当NULL在productLine列发生行聚合时，GROUPING(productLine)当返回1 ，否则为0。

我们经常使用GROUPING()函数将有意义的标签替换为超级聚合NULL值，而不是直接显示它。

以下示例显示如何将IF()函数与GROUPING()函数组合以替换超聚合NULL值orderYear和productLine列中的标签：
```sql
SELECT 
    IF(GROUPING(orderYear),
        'All Years',
        orderYear) orderYear,
    IF(GROUPING(productLine),
        'All Product Lines',
        productLine) productLine,
    SUM(orderValue) totalOrderValue
FROM sales
GROUP BY orderYear, productline 
WITH ROLLUP;
```
