MySQL 函数

MySQL聚合函数
```
AVG - 计算一组值或表达式的平均值。
COUNT - 计算表中的行数。
INSTR - 返回字符串中第一次出现的子字符串的位置。
SUM  - 计算一组值或表达式的总和。
MIN - 在一组值中找到最小值
MAX - 在一组值中找到最大值
```

MySQL字符串函数
```
CONCAT - 将两个或多个字符串组合成一个字符串。
LENGTH＆CHAR_LENGTH  - 获取字符串的长度，以字节和字符为单位。
LEFT - 获取具有指定长度的字符串的左侧部分。
REPLACE - 搜索并替换字符串中的子字符串。
SUBSTRING - 从具有特定长度的位置开始提取子字符串。
TRIM - 从字符串中删除不需要的字符。
FIND_IN_SET - 在以逗号分隔的字符串列表中查找字符串。
FORMAT - 格式化具有特定区域设置的数字，四舍五入到小数位数
```

MySQL控制流功能
```
CASE - THEN如果WHEN满足分支中的条件，则返回分支中的相应结果，否则返回ELSE分支中的结果。
IF - 根据给定条件返回值。
IFNULL - 如果它不是NULL则返回第一个参数，否则返回第二个参数。
NULLIF - 如果第一个参数等于第二个参数，则返回NULL，否则返回第一个参数。
```

MySQL日期和时间函数
```
CURDATE - 返回当前日期。
DATEDIFF  - 计算两个DATE值之间的天数   。
DAY  - 获取指定日期的月份日期。
DATE_ADD  - 将日期值添加到日期值。
DATE_SUB - 从日期值中减去时间值。
DATE_FORMAT - 根据指定的日期格式格式化日期值。
DAYNAME - 获取指定日期的工作日名称。
DAYOFWEEK - 返回日期的工作日索引。
EXTRACT - 提取日期的一部分。
NOW - 返回执行语句的当前日期和时间。
MONTH - 返回表示指定日期月份的整数。
STR_TO_DATE - 根据指定的格式将字符串转换为日期和时间值。
SYSDATE - 返回当前日期。
TIMEDIFF - 计算两个TIME或DATETIME值之间的差异。
TIMESTAMPDIFF - 计算两个DATE或DATETIME值之间的差异。
WEEK - 返回一个星期的日期。
WEEKDAY  - 返回日期的工作日索引。
YEAR -返回日期值的年份部分。
```

MySQL比较功能
```
COALESCE - 返回第一个非null参数，这对于替换null非常方便。
GREATEST＆LEAST - 取n个参数并分别返回n个参数的最大值和最小值。
ISNULL - 如果参数为null，则返回1，否则返回零。
```

MySQL数学函数
```
ABS - 返回数字的绝对值。
CEIL - 返回大于或等于输入数字的最小整数值。
FLOOR - 返回不大于参数的最大整数值。
MOD - 返回数字的余数除以另一个。
ROUND  - 将数字四舍五入到指定的小数位数。
TRUNCATE - 将数字截断为指定的小数位数。
其他MySQL功能
LAST_INSERT_ID - 获取最后生成的最后一个插入记录的序列号。
CAST - 将任何类型的值转换为具有指定类型的值。
```

MySQL字符串函数
```
功能	描述
ASCII	返回特定字符的ASCII值
CHAR_LENGTH	返回字符串的长度（以字符为单位）
CHARACTER_LENGTH	返回字符串的长度（以字符为单位）
CONCAT	一起添加两个或多个表达式
CONCAT_WS	将两个或多个表达式与分隔符一起添加
FIELD	返回值列表中值的索引位置
FIND_IN_SET	返回字符串列表中字符串的位置
FORMAT	将数字格式化为“＃，###，###。##”等格式，舍入到指定的小数位数
INSERT	在指定位置的字符串中插入一个字符串，并插入一定数量的字符
INSTR	返回第一次出现在另一个字符串中的字符串的位置
LCASE	将字符串转换为小写
LEFT	从字符串中提取多个字符（从左开始）
LENGTH	返回字符串的长度（以字节为单位）
LOCATE	返回字符串中第一次出现子字符串的位置
LOWER	将字符串转换为小写
LPAD	左边用另一个字符串填充一个字符串到一定长度
LTRIM	从字符串中删除前导空格
MID	从字符串中提取子字符串（从任何位置开始）
POSITION	返回字符串中第一次出现子字符串的位置
REPEAT	按指定的次数重复一次字符串
REPLACE	使用新的子字符串替换字符串中所有出现的子字符串
REVERSE	反转字符串并返回结果
RIGHT	从字符串中提取多个字符（从右侧开始）
RPAD	右边用另一个字符串填充一个字符串到一定长度
RTRIM	从字符串中删除尾随空格
SPACE	返回指定数量的空格字符的字符串
STRCMP	比较两个字符串
SUBSTR	从字符串中提取子字符串（从任何位置开始）
SUBSTRING	从字符串中提取子字符串（从任何位置开始）
SUBSTRING_INDEX	在指定数量的分隔符出现之前返回字符串的子字符串
TRIM	从字符串中删除前导和尾随空格
UCASE	将字符串转换为大写
UPPER	将字符串转换为大写
```

MySQL数字函数
```
功能	描述
ABS	返回数字的绝对值
ACOS	返回数字的反余弦值
ASIN	返回数字的反正弦值
ATAN	返回一个或两个数字的反正切
ATAN2	返回两个数字的反正切
AVG	返回表达式的平均值
CEIL	返回> =到数字的最小整数值
CEILING	返回> =到数字的最小整数值
COS	返回数字的余弦值
COT	返回数字的余切
COUNT	返回select查询返回的记录数
DEGREES	将弧度值转换为度数
DIV	用于整数除法
EXP	返回e提升到指定数字的幂
FLOOR	返回<=到数字的最大整数值
GREATEST	返回参数列表的最大值
LEAST	返回参数列表的最小值
LN	返回数字的自然对数
LOG	返回数字的自然对数，或数字的对数到指定的基数
LOG10	返回数字的自然对数到10
LOG2	返回数字2的自然对数
MAX	返回一组值中的最大值
MIN	返回一组值中的最小值
MOD	返回数字的余数除以另一个数字
PI	返回PI的值
POW	返回被提升到另一个数的幂的数字的值
POWER	返回被提升到另一个数的幂的数字的值
RADIANS	将度数值转换为弧度
RAND	返回一个随机数
ROUND	将数字舍入到指定的小数位数
SIGN	返回数字的符号
SIN	返回数字的正弦值
SQRT	返回数字的平方根
SUM	计算一组值的总和
TAN	返回数字的正切值
TRUNCATE	将数字截断为指定的小数位数
MySQL日期函数
功能	描述
ADDDATE	将时间/日期间隔添加到日期，然后返回日期
ADDTIME	将时间间隔添加到时间/日期时间，然后返回时间/日期时间
CURDATE	返回当前日期
CURRENT_DATE	返回当前日期
CURRENT_TIME	返回当前时间
CURRENT_TIMESTAMP	返回当前日期和时间
CURTIME	返回当前时间
DATE	从日期时间表达式中提取日期部分
DATEDIFF	返回两个日期值之间的天数
DATE_ADD	将时间/日期间隔添加到日期，然后返回日期
DATE_FORMAT	格式化日期
DATE_SUB	从日期中减去时间/日期间隔，然后返回日期
DAY	返回给定日期的月中的某天
DAYNAME	返回给定日期的工作日名称
DAYOFMONTH	返回给定日期的月中的某天
DAYOFWEEK	返回给定日期的工作日索引
DAYOFYEAR	返回给定日期的一年中的某一天
EXTRACT	从给定日期提取部分
FROM_DAYS	从数字日期值返回日期
HOUR	返回给定日期的小时部分
LAST_DAY	提取指定日期的月份的最后一天
LOCALTIME	返回当前日期和时间
LOCALTIMESTAMP	返回当前日期和时间
MAKEDATE	根据年份和天数值创建并返回日期
MAKETIME	根据小时，分钟和秒值创建并返回时间
MICROSECOND	返回时间/日期时间的微秒部分
MINUTE	返回时间/日期时间的分钟部分
MONTH	返回给定日期的月份部分
MONTHNAME	返回给定日期的月份名称
NOW	返回当前日期和时间
PERIOD_ADD	在一段时间内添加指定的月数
PERIOD_DIFF	返回两个句点之间的差异
QUARTER	返回给定日期值的一年中的季度
SECOND	返回时间/日期时间的秒部分
SEC_TO_TIME	返回基于指定秒数的时间值
STR_TO_DATE	返回基于字符串和格式的日期
SUBDATE	从日期中减去时间/日期间隔，然后返回日期
SUBTIME	从日期时间中减去时间间隔，然后返回时间/日期时间
SYSDATE	返回当前日期和时间
TIME	从给定的时间/日期时间中提取时间部分
TIME_FORMAT	按指定格式格式化时间
TIME_TO_SEC	将时间值转换为秒
TIMEDIFF	返回两个时间/日期时间表达式之间的差异
TIMESTAMP	返回基于日期或日期时间值的日期时间值
TO_DAYS	返回日期和日期“0000-00-00”之间的天数
WEEK	返回给定日期的周数
WEEKDAY	返回给定日期的工作日编号
WEEKOFYEAR	返回给定日期的周数
YEAR	返回给定日期的年份部分
YEARWEEK	返回给定日期的年和周数
```

MySQL高级功能
```
功能	描述
BIN	返回数字的二进制表示形式
BINARY	将值转换为二进制字符串
CASE	通过条件并在满足第一个条件时返回值
CAST	将值（任何类型）转换为指定的数据类型
COALESCE	返回列表中的第一个非null值
CONNECTION_ID	返回当前连接的唯一连接ID
CONV	将数字从一个数字基本系统转换为另一个
CONVERT	将值转换为指定的数据类型或字符集
CURRENT_USER	返回服务器用于验证当前客户端的MySQL帐户的用户名和主机名
DATABASE	返回当前数据库的名称
IF	如果条件为TRUE则返回值，如果条件为FALSE则返回另一个值
IFNULL	如果表达式为NULL，则返回指定的值，否则返回表达式
ISNULL	返回1或0，具体取决于表达式是否为NULL
LAST_INSERT_ID	返回已在表中插入或更新的最后一行的AUTO_INCREMENT标识
NULLIF	比较两个表达式，如果它们相等则返回NULL。否则，返回第一个表达式
SESSION_USER	返回当前的MySQL用户名和主机名
SYSTEM_USER	返回当前的MySQL用户名和主机名
USER	返回当前的MySQL用户名和主机名
VERSION	返回MySQL数据库的当前版本
```