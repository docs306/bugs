# UPDATE JOIN

在MySQL中可以使用UPDATE语句中的JOIN子句来执行跨表更新。

MySQL的语法UPDATE JOIN  如下：
```sql
UPDATE T1, T2,
[INNER JOIN | LEFT JOIN] T1 ON T1.C1 = T2. C1
SET T1.C2 = T2.C2, T2.C3 = expr
WHERE condition 
```
让我们更详细地研究MySQL UPDATE JOIN 语法：

- 首先，在子句后面指定主表（T1）和主表要加入的表（T2）UPDATE。注意必须在 UPDATE 子句后指定至少一个表。表中未在 UPDATE 子句后指定的数据将不会更新。
- 接下来，指定一种加入想用即无论是INNER JOIN 或 LEFT JOIN和联接谓词。JOIN必须在UPDATE之后出现。
- 然后，将新值分配给要更新的T1，T2表中的列。
- 之后，在WHERE子句中指定条件以将行限制为行以进行更新。
如果按照UPDATE语句教程进行操作，会注意到还有另一种使用以下语法更新数据交叉表的方法：

```sql
UPDATE T1, T2
SET T1.c2 = T2.c2,T2.c3 = expr
WHERE T1.c1 = T2.c1 AND condition 
```
`UPDATE 语句UPDATE JOIN与隐式 INNER JOIN子句的作用相同。`

这意味着可以按如下方式重写上述语句：
```sql
UPDATE T1,T2
INNER JOIN T2 ON T1.C1 = T2.C1
SET T1.C2 = T2.C2,T2.C3 = expr
WHERE condition 
```


### UPDATE JOIN 示例

我们使用一个名为empdb 的数据库进行演示。此示例数据库包含两个表：

- employees表存储员工数据，包括员工ID，姓名，绩效和薪水。
- merits表存储员工绩效和绩效百分比。

以下语句在empdb示例数据库中创建和加载数据：
```sql
CREATE DATABASE IF NOT EXISTS empdb;
USE empdb;
 
-- create tables
CREATE TABLE merits (
    performance INT(11) NOT NULL,
    percentage FLOAT NOT NULL,
    PRIMARY KEY (performance)
);
 
CREATE TABLE employees (
    emp_id INT(11) NOT NULL AUTO_INCREMENT,
    emp_name VARCHAR(255) NOT NULL,
    performance INT(11) DEFAULT NULL,
    salary FLOAT DEFAULT NULL,
    PRIMARY KEY (emp_id),
    CONSTRAINT fk_performance FOREIGN KEY (performance)
    REFERENCES merits (performance)
);

-- insert data for merits table
INSERT INTO merits(performance, percentage)
VALUES(1,0),(2,0.01),(3,0.03),(4,0.05),(5,0.08);

-- insert data for employees table
INSERT INTO employees(emp_name,performance,salary)      
VALUES('Mary Doe', 1, 50000),
      ('Cindy Smith', 3, 65000),
      ('Sue Greenspan', 4, 75000),
      ('Grace Dell', 5, 125000),
      ('Nancy Johnson', 3, 85000),
      ('John Doe', 2, 45000),
      ('Lily Bush', 3, 55000); 
MySQL UPDATE JOIN 使用INNER JOIN子句实例
```
假设想根据员工的绩效调整员工的薪水。

绩效的百分比存储在merits表中，必须使用 UPDATE INNER JOIN语句根据employees 表中percentage 存储的内容调整merits 表中员工的工资。

employees 和merit表之间的链接是 performance字段。

```sql
UPDATE employees
INNER JOIN merits ON employees.performance = merits.performance 
SET salary = salary + salary * percentage; 
```
运行结果：
```
mysql> select * from employees;
+--------+---------------+-------------+--------+
| emp_id | emp_name      | performance | salary |
+--------+---------------+-------------+--------+
|      1 | Mary Doe      |           1 |  50000 |
|      2 | Cindy Smith   |           3 |  66950 |
|      3 | Sue Greenspan |           4 |  78750 |
|      4 | Grace Dell    |           5 | 135000 |
|      5 | Nancy Johnson |           3 |  87550 |
|      6 | John Doe      |           2 |  45450 |
|      7 | Lily Bush     |           3 |  56650 |
+--------+---------------+-------------+--------+
```
查询的工作原理。

只指定 employees表在 UPDATE子句之后，因为只想更新 employees表中的数据。

对于 employees表中的每一行，查询将根据表中性能列中的值检查性能列中的值 merits。如果匹配到了 merits表中 percentage在 ，并更新 employees表中 salary列 。

因为我们省略了UPDATE语句中的WHERE子句，所以employees表中的所有记录都会更新。

MySQL UPDATE JOIN 使用LEFT JOIN示例
假设公司雇用了两名员工：

```sql
INSERT INTO employees(emp_name,performance,salary)
VALUES('Jack William',NULL,43000),('Ricky Bond',NULL,52000); 
```
因为这些员工是新员工，所以他们的绩效数据不可用或者NULL。

运行结果：
```
mysql> select * from employees;
+--------+---------------+-------------+--------+
| emp_id | emp_name      | performance | salary |
+--------+---------------+-------------+--------+
|      1 | Mary Doe      |           1 |  50000 |
|      2 | Cindy Smith   |           3 |  66950 |
|      3 | Sue Greenspan |           4 |  78750 |
|      4 | Grace Dell    |           5 | 135000 |
|      5 | Nancy Johnson |           3 |  87550 |
|      6 | John Doe      |           2 |  45450 |
|      7 | Lily Bush     |           3 |  56650 |
|      8 | Jack William  |        NULL |  43000 |
|      9 | Ricky Bond    |        NULL |  52000 |
+--------+---------------+-------------+--------+
9 rows in set (0.00 sec)
```
要增加新员工的工资，不能使用 UPDATE INNER JOIN语句因为他们的绩效数据在 merit表中不可用。这就是使用 UPDATE LEFT JOIN的原因。

UPDATE LEFT JOIN语句在另一个表中没有对应的行时，语句基本上更新了表中的行。

可以使用以下语句将新员工的薪水提高15％：

```sql
UPDATE employees
LEFT JOIN merits ON employees.performance = merits.performance 
SET salary = salary + salary * 0.15
WHERE merits.percentage IS NULL; 
```
