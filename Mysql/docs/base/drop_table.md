MySQL DROP TABLE语句语法
要删除现有表，请使用MySQL DROP TABLE语句。DROP TABLE语法如下：
```
DROP [TEMPORARY] TABLE [IF EXISTS] table_name [, table_name] ...
[RESTRICT | CASCADE] 
```
DROP TABLE语句从数据库中永久删除表及其数据。在MySQL中，您还可以使用单个DROP TABLE语句删除多个表，每个表由逗号（，）分隔。

TEMPORARY选项允许您仅删除临时表。确保不会意外删除非临时表非常有用。

如果尝试删除不存在的表，IF EXISTS选项不会导致错误。当您使用IF EXISTS选项时，MySQL会生成一个NOTE，可以使用SHOW WARNING语句检索。请务必注意，当列表中存在不存在的表时，DROP TABLE语句将删除所有现有表并发出错误消息或NOTE。

如上所述，DROP TABLE语句仅删除表及其数据。它不会删除与已删除表关联的特定用户权限。因此，如果在此之后重新创建具有相同名称的表，则现有权限将应用于新表，这可能会带来安全风险。

RESTRICT和CASCADE  选项保留给MySQL的未来版本。

最后但并非最不重要的一点是，您必须拥有DROP要删除的表的权限。

MySQL DROP TABLE示例
我们将删除上一个教程中使用CREATE TABLE语句教程创建的tasks表。

此外，我们还删除了数据库中不存在的表以演示SHOW WARNING语句。删除tasks表的语句和名称不存在的表nonexistent_table如下：
```
DROP TABLE IF EXISTS tasks, nonexistent_table; 
```
如果检查数据库，您将看到tasks表已被删除。您可以使用以下SHOW WARNING语句检查由于不存在的表而由MySQL生成的NOTE ：
```
SHOW WARNINGS; 
+-------+------+-------------------------------------------+
| Level | Code | Message                                   |
+-------+------+-------------------------------------------+
| Note  | 1051 | Unknown table 'caracal.nonexistent_table' |
+-------+------+-------------------------------------------+
1 rows in set (0.01 sec)
```
MySQL DROP TABLE 基于模式
假设您有许多名称以test数据库开头的表，并且您希望通过使用单个DROP TABLE语句删除所有表来节省时间。不幸的是，MySQL没有提供DROP TABLE LIKE可以根据模式匹配删除表的语句，如下所示：
```
DROP TABLE LIKE '%pattern%' 
```
但是，有一些解决方法。我们将在此讨论其中一个供您参考。

让我们开始创建test *表以便演示。
```
CREATE TABLE IF NOT EXISTS test1(
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY(id)
);
 
CREATE TABLE IF NOT EXISTS test2 LIKE test1;
CREATE TABLE IF NOT EXISTS test3 LIKE test1;
CREATE TABLE IF NOT EXISTS test4 LIKE test1; 
```
我们创建了一个名为四个表test1，test2，test3 并test4 用相同的表结构。

假设您要一次删除所有test*表，可以按照以下步骤操作：

首先，声明两个接受数据库模式的变量和一个您希望表匹配的模式：
```
-- 设置表的数据库及匹配模式
SET @schema = 'mysqldemo';
SET @pattern = 'test%'; 
接下来，需要构建一个动态DROP TABLE语句：

-- 动态构建SQL (DROP TABLE tbl1, tbl2...;)
SELECT CONCAT('DROP TABLE ',GROUP_CONCAT(CONCAT(@schema,'.',table_name)),';')
INTO @droplike
FROM information_schema.tables
WHERE @schema = database()
AND table_name LIKE @pattern; 
```
基本上，查询MySQL的数据字典在information_schema表，它包含所有数据库中所有表的数据，并连接数据库@schema（mysqldemo ）中与pattern @pattern （test%）匹配所有表前缀加上DROP TABLE 关键字。GROUP_CONCAT函数创建一个逗号分隔的表列表。

然后，我们可以显示动态SQL以验证它是否正常工作：

-- 显示构建的SQL脚本
```
SELECT @droplike; 
+--------------------------------------------------------------------------------------------+
| @droplike                                                                                  |
+--------------------------------------------------------------------------------------------+
| DROP TABLE mysqldemo.test1,mysqldemo.test2,mysqldemo.test3,mysqldemo.test4; |
+--------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)
```
我们可以看预期的结果。

之后，使用MySQL中的prepared语句执行语句，如下所示：
```
-- 执行动态SQL
PREPARE stmt FROM @droplike;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 
有关MySQL预处理语句的更多信息，请查看MySQL预编译语句教程。

把它们放在一起。

-- 设置表的数据库及匹配模式
SET @schema = 'mysqldemo';
SET @pattern = 'test%';
 
-- 动态构建SQL (DROP TABLE tbl1, tbl2...;) 
SELECT CONCAT('DROP TABLE ',GROUP_CONCAT(CONCAT(@schema,'.',table_name)),';')
INTO @droplike
FROM information_schema.tables
WHERE @schema = database()
AND table_name LIKE @pattern;
 
-- 显示构建的SQL脚本 
SELECT @droplike;
 
-- 执行动态SQL 
PREPARE stmt FROM @dropcmd;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 
```
因此，如果要删除在数据库中具有特定模式的多个表，只需使用上面的脚本来节省时间。所有你需要做的是更换模式和数据库架构中@pattern和@schema变量。如果您经常需要处理此任务，则始终可以基于脚本开发存储过程并重用此存储过程。
