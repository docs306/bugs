# MySQL RIGHT JOIN

MySQL RIGHT JOIN 类似于 LEFT JOIN 是对表反转的处理。

以下语句使用以下RIGHT JOIN子句查询来自两个表t1和t2的数据：

```sql
SELECT * FROM t1
RIGHT JOIN t2 ON mode; 
```

t1是左表，t2是右表
mode是将左表（t1）上的行与右表（t2）上的行匹配的条件
mode写法：
```sql
t1.pk = t2.fk 
 // 或者 如果两个表的公共列具有相同的名称，可以使用下面语法：
USING (common_column); 
```
RIGHT JOIN子句的工作原理。

t2表中的所有行（右表）将在结果集中至少出现一次。
基于mode条件，如果t1表中没有匹配的行（左表），则t1表中的列中将显示NULL。

*重要：RIGHT JOIN 与 LEFT JOIN 子句在功能上是等效的，只要切换表顺序，它们就可以互相替换。*
*注：RIGHT OUTER JOIN 是 RIGHT JOIN 的同义词。*

### 示例

假设 t1 并 t2 表
```sql
CREATE TABLE t1 (
    id INT PRIMARY KEY,
    pattern VARCHAR(50) NOT NULL);
 
CREATE TABLE t2 (
    id VARCHAR(50) PRIMARY KEY,
    pattern VARCHAR(50) NOT NULL);
 
INSERT INTO t1(id, pattern)
VALUES(1,'Divot'),
      (2,'Brick'),
      (3,'Grid');
 
INSERT INTO t2(id, pattern)
VALUES('A','Brick'),
      ('B','Grid'),
      ('C','Diamond'); 
```
以下查询连接两个表t1并t2使用pattern列：

```sql
SELECT t1.id, t2.id
FROM t1
RIGHT JOIN t2 USING (pattern)
ORDER BY t2.id; 

+------+----+
| id   | id |
+------+----+
|    2 | A  |
|    3 | B  |
| NULL | C  |
+------+----+
```


查询获取销售代表及其客户：
```sql
SELECT concat(e.firstName,' ', e.lastName) salesman, e.jobTitle, e.customerName
FROM employees e
RIGHT JOIN customers c ON e.employeeNumber = c.employeeNumber AND e.jobTitle = 'Sales Rep'
ORDER BY customerName; 
```
运行结果：
```
+------------------+-----------+------------------------------------+
| salesman         | jobTitle  | customerName                       |
+------------------+-----------+------------------------------------+
| Gerard Hernandez | Sales Rep | Alpha Cognac                       |
| Foon Yue Tseng   | Sales Rep | American Souvenirs Inc             |
| Pamela Castillo  | Sales Rep | Amica Models & Co.                 |
| NULL             | NULL      | ANG Resellers                      |
| Andy Fixter      | Sales Rep | Anna's Decorations, Ltd            |
| NULL             | NULL      | Anton Designs, Ltd.                |
| NULL             | NULL      | Asian Shopping Network, Co         |
| NULL             | NULL      | Asian Treasures, Inc.              |
| Gerard Hernandez | Sales Rep | Atelier graphique                  |
...
```
因为使用RIGHT JOIN，所有客户（右表）都出现在结果集中。有些客户的salseman 与 jobTitle 字段为 NULL。
