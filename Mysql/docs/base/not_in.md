MySQL NOT IN 运算符简介
运算符对运算NOT符取反IN：
```sql
value NOT IN (value1, value2, value2) 
```
NOT IN如果该值不等于列表中的任何值，则该运算符返回 1。否则，返回 0。

以下示例使用NOT IN运算符检查数字 1 是否是NOT IN列表 (1,2,3)：
```sql
SELECT 1 NOT IN (1,2,3); 
```
输出：
```
+------------------+
| 1 NOT IN (1,2,3) |
+------------------+
| 0 |
+------------------+
1 row in set (0.00 sec)
```
它返回 0 (false)，因为 1 是NOT IN列表 is false。

以下示例使用NOT IN运算符检查 0 是否为NOT IN列表 (1,2,3)：

```sql
SELECT 0 NOT IN (1,2,3); 
```
输出：
```
+------------------+
| 0 NOT IN (1,2,3) |
+------------------+
| 1 |
+------------------+
1 row in set (0.00 sec)
```
如果运算符左侧的值为NULL ，则该NOT IN运算符返回NULL 。例如：IN

```sql
SELECT NULL NOT IN (1,2,3);
```
输出：
```
+---------------------+
| NULL NOT IN (1,2,3) |
+---------------------+
| NULL |
+---------------------+
1 row in set (0.00 sec)
```
从技术上讲，该NOT IN运算符相当于以下内容：
```sql
NOT (value = value1 OR value = value2 OR value = valu3) 

// 或者：

value <> value1 AND value <> value2 AND value <> value3 
```
MySQL NOT IN 运算符示例
我们将使用offices示例数据库中的表来说明NOT IN运算符：

MySQL NOT IN 运算符示例
以下示例使用NOT IN运算符查找不在France和 中的办事处USA：
```sql
SELECT 
   officeCode, 
   city, 
   phone
   FROM
   offices
WHERE
   country NOT IN ('USA' , 'France')
ORDER BY 
city; 
```
输出：
```
+------------+--------+------------------+
| officeCode | city | phone |
+------------+--------+------------------+
| 7 | London | +44 20 7877 2041 |
| 6 | Sydney | +61 2 9264 2451 |
| 5 | Tokyo | +81 33 224 5000 |
+------------+--------+------------------+
3 rows in set (0.02 sec)
```
总结：

- 使用 MySQL NOT IN检查某个值是否与列表中的任何值都不匹配。

