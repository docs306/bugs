MySQL ADD COLUMN语句简介
要向现有表添加新列，请使用ALTER TABLE ADD COLUMN语句，如下所示：
```
ALTER TABLE table
ADD [COLUMN] column_name column_definition [FIRST|AFTER existing_column]; 
```
让我们更详细地研究一下这个陈述。

首先，在ALTER TABLE子句后指定表名。
其次，将新列及其定义放在ADD COLUMN子句之后。请注意，COLUMN关键字是可选的，因此您可以省略它。
第三，MySQL允许您通过指定FIRST关键字将新列添加为表的第一列。它还允许您使用AFTER existing_column子句在现有列之后添加新列。如果您没有明确指定新列的位置，MySQL会将其添加为最后一列。
若要同时向表中添加两个或更多列，请使用以下语法：

```
ALTER TABLE table
ADD [COLUMN] column_name_1 column_1_definition [FIRST|AFTER existing_column],
ADD [COLUMN] column_name_2 column_2_definition [FIRST|AFTER existing_column],
...; 
```
我们来看一些向现有表添加新列的示例。

MySQL ADD COLUMN示例
首先，我们使用以下语句创建一个表 命名vendors用以演示：

```
CREATE TABLE IF NOT EXISTS vendors (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
); 
```
其次，我们为vendors表添加一个名为phone新列。因为我们在name列之后明确指定phone列的位置，所以MySQL将遵守这一点。

ALTER TABLE vendors
ADD COLUMN phone VARCHAR(15) AFTER name; 
第三，我们再为vendors表添加一个名为vendor_group新列。此时，我们不指定新列vendor_group的位置，因此MySQL将列添加到 vendors表的最后一列。
```
ALTER TABLE vendors
ADD COLUMN vendor_group INT NOT NULL; 
```
让我们在vendors表中插入一些记录。
```
INSERT INTO vendors(name,phone,vendor_group)
VALUES('IBM','(408)-298-2987',1);
 
INSERT INTO vendors(name,phone,vendor_group)
VALUES('Microsoft','(408)-298-2988',1); 
```
我们可以查看vendors表的变化。

```
SELECT 
    id, name, phone,vendor_group
FROM
    vendors; 
+----+-----------+----------------+--------------+
| id | name      | phone          | vendor_group |
+----+-----------+----------------+--------------+
|  1 | IBM       | (408)-298-2987 |            1 |
|  2 | Microsoft | (408)-298-2988 |            1 |
+----+-----------+----------------+--------------+
2 rows in set (0.00 sec)
```
第四，再添加两列email并hourly_rate同时添加到vendors表中。
```
ALTER TABLE vendors
ADD COLUMN email VARCHAR(100) NOT NULL,
ADD COLUMN hourly_rate decimal(10,2) NOT NULL; 
```
注意：email和hourly_rate两个列都分配了  NOT NULL values但是，vendors表已经有数据。在这种情况下，MySQL将使用这些新列的默认值。

我们来检查vendors表格中的数据。

```
SELECT 
    id, name, phone, vendor_group, email, hourly_rate
FROM
    vendors; 
+----+-----------+----------------+--------------+-------+-------------+
| id | name      | phone          | vendor_group | email | hourly_rate |
+----+-----------+----------------+--------------+-------+-------------+
|  1 | IBM       | (408)-298-2987 |            1 |       |        0.00 |
|  2 | Microsoft | (408)-298-2988 |            1 |       |        0.00 |
+----+-----------+----------------+--------------+-------+-------------+
2 rows in set (0.00 sec)
```
email列中填充了空值，而不是NULL值。并且hourly_rate列填充了0.00值。

如果您不小心添加了表中已存在的列，MySQL将发出错误。例如，如果执行以下语句：

```
ALTER TABLE vendors
ADD COLUMN vendor_group INT NOT NULL; 
```
MySQL发出错误消息：

ERROR 1060 (42S21): Duplicate column name 'vendor_group'
对于包含几列的表，可以很容易地看到哪些列已经存在。但是，如果有一个包含数百列的大表，那就更难了。

在某些情况下，您希望在添加列之前检查表中是否已存在列。但是，没有ADD COLUMN IF NOT EXISTS可用的声明。幸运的是，您可以从information_schema数据库的columns表中获取此信息，如下所示：

```
SELECT 
    IF(count(*) = 1, 'Exist','Not Exist') AS result
FROM
    information_schema.columns
WHERE
    table_schema = 'classicmodels'
        AND table_name = 'vendors'
        AND column_name = 'phone'; 
+-----------+
| result    |
+-----------+
| Not Exist |
+-----------+
1 row in set (0.01 sec)
```
在WHERE子句中，我们传递了三个参数：表模式或数据库，表名和列名。我们使用IF函数来返回列是否存在。

