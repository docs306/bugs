MySQL事务 以及如何使用COMMIT和ROLLBACK语句来管理MySQL中的事务。

介绍MySQL事务
要了解MySQL中的事务，让我们看一下在示例数据库中添加新销售订单。添加销售订单的步骤如下所述：

从orders表中查询最新的销售订单编号，并使用下一个销售订单编号作为新的销售订单编号。
在orders表中插入新的销售订单 。
获取新插入的销售订单号
将新的销售订单项目与销售订单编号一起插入 orderdetails 表格中
从两个表orders和orderdetails表中选择数据  以确认更改
现在，想象一下如果上面的一个或多个步骤由于某些原因（例如表锁定）而失败，销售订单数据会发生什么？例如，如果将订单项添加到orderdetails表中的步骤失败，则您将有一个空的销售订单。

这就是交易处理拯救的原因。MySQL事务允许您执行一组MySQL操作，以确保数据库永远不会包含部分操作的结果。在一组操作中，如果其中一个操作失败，则会发生回滚以将数据库还原到其原始状态。如果没有发生错误，则将整个语句集提交给数据库。

MySQL事务声明
MySQL为我们提供了以下控制事务的重要声明：

- 要启动事务，请使用START TRANSACTION  语句。也可以使用START TRANSACTION的别名BEGIN或  BEGIN WORK
- 要提交当前事务并使其永久更改，请使用COMMIT语句。
- 要回滚当前事务并取消其更改，请使用ROLLBACK语句。
- 要禁用或启用当前事务的自动提交模式，请使用SET autocommit语句。
默认情况下，MySQL会自动将更改永久提交到数据库。要强制MySQL不自动提交更改，请使用以下语句：

```
SET autocommit = 0; 
```
或者
```
SET autocommit = OFF 
```
您可以使用以下语句显式启用自动提交模式：
```
SET autocommit = 1; 
```
或者
```
SET autocommit = ON; 
```
MySQL事务示例
我们将使用示例数据库中的  ordersand orderDetails表进行演示。

MySQL Transaction：orders＆orderDetails表

COMMIT示例
为了使用事务，首先必须将SQL语句分解为逻辑部分，并确定何时应提交或回滚数据。

以下说明了创建新销售订单的步骤：

使用START TRANSACTION  语句启动事务  。
从orders表中选择最新的销售订单编号，并使用下一个销售订单编号作为新的销售订单编号。
在orders表格中插入新的销售订单  。
将销售订单项插入  orderdetails表中。
使用COMMIT语句提交事务  。
（可选）您可以从两个表orders和orderdetails表中选择数据  以检查新的销售订单。

以下是执行上述步骤的脚本：
```
-- 1. 开始一个新手事物
START TRANSACTION;

-- 2. 获取最新的订单编号
SELECT 
    @orderNumber:=MAX(orderNUmber)+1
FROM
    orders;

-- 3. 向145客户插入新订单编号

INSERT INTO orders(orderNumber,
                   orderDate,
                   requiredDate,
                   shippedDate,
                   status,
                   customerNumber)
VALUES(@orderNumber,
       '2005-05-31',
       '2005-06-10',
       '2005-06-11',
       'In Process',
        145);
        
-- 4. 插入订单详情
INSERT INTO orderdetails(orderNumber,
                         productCode,
                         quantityOrdered,
                         priceEach,
                         orderLineNumber)
VALUES(@orderNumber,'S18_1749', 30, '136', 1),
      (@orderNumber,'S18_2248', 50, '55.09', 2); 
      
-- 5. 提交事实
COMMIT; 
```
运行结果：
```
+----------------------------------+
| @orderNumber:=MAX(orderNUmber)+1 |
+----------------------------------+
|                            10426 |
+----------------------------------+
1 row in set (0.02 sec)
```
要获取新创建的销售订单，请使用以下查询：

```sql
SELECT 
    a.orderNumber,
    orderDate,
    requiredDate,
    shippedDate,
    status,
    comments,
    customerNumber,
    orderLineNumber,
    productCode,
    quantityOrdered,
    priceEach
FROM
    orders a
        INNER JOIN
    orderdetails b USING (orderNumber)
WHERE
    a.ordernumber = 10426; 
```
这是输出：
```
+-------------+------------+--------------+-------------+------------+----------+----------------+-----------------+-------------+-----------------+-----------+
| orderNumber | orderDate  | requiredDate | shippedDate | status     | comments | customerNumber | orderLineNumber | productCode | quantityOrdered | priceEach |
+-------------+------------+--------------+-------------+------------+----------+----------------+-----------------+-------------+-----------------+-----------+
|       10426 | 2005-05-31 | 2005-06-10   | 2005-06-11  | In Process | NULL     |            145 |               1 | S18_1749    |              30 |    136.00 |
|       10426 | 2005-05-31 | 2005-06-10   | 2005-06-11  | In Process | NULL     |            145 |               2 | S18_2248    |              50 |     55.09 |
+-------------+------------+--------------+-------------+------------+----------+----------------+-----------------+-------------+-----------------+-----------+
2 rows in set (0.01 sec)
```
ROLLBACK实例
首先，登录MySQL数据库服务器并从订单表中删除数据：
```
mysql> START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> DELETE FROM orders;
Query OK, 327 rows affected (0.03 sec)
```
从输出中可以看出，MySQL确认orders表中的所有行都被删除了。

其次，在单独的会话中登录MySQL数据库服务器并从orders表中查询数据：

```
mysql> SELECT COUNT(*) FROM orders;
+----------+
| COUNT(*) |
+----------+
|      327 |
+----------+
1 row in set (0.00 sec)
```
在第二个会话中，我们仍然可以看到orders表中的数据。

我们在第一次会话上做了修改。但是，这些变化不是永久性的。在第一个会话中，我们可以提交或回滚更改。

出于演示目的，我们将在第一个会话中回滚更改。
```
mysql> ROLLBACK; 
Query OK, 0 rows affected (0.04 sec)
```
在第一个会话中，我们还将验证orders表的内容：
```
mysql> SELECT COUNT(*) FROM orders;
+----------+
| COUNT(*) |
+----------+
|      327 |
+----------+
1 row in set (0.00 sec)
```