MySQL主键简介
mysql主键主键是一列或一组列，用于唯一标识表中的每一行。为表定义主键时，必须遵循以下规则：

主键必须包含唯一值。如果主键由多列组成，则这些列中的值组合必须是唯一的。
主键列不能包含NULL值。这意味着您必须使用NOT NULL  属性声明主键列  。如果不这样做，MySQL将NOT NULL  隐式强制主键列。
一表只有一个主键。
因为MySQL使用整数运行得更快，所以主键列的数据类型应是整数，例如：INT, BIGINT。但是，您应确保主键的整数类型的值范围足以存储表可能具有的所有可能的行。

主键列通常具有自动为键AUTO_INCREMENT生成唯一序列的  属性。下一行的主键大于前一行。

为表定义主键时，MySQL会自动创建一个索引名为PRIMARY。

定义MySQL PRIMARY KEY约束
MySQL允许您通过在创建或修改表时定义主键约束来创建主键。

使用CREATE TABLE语句定义MySQL PRIMARY KEY约束
您通常在使用CREATE TABLE 语句创建表时定义主键。要PRIMARY KEY  为表创建约束，请PRIMARY KEY在主键列的定义中指定。

以下示例创建主键为user_id列的users表：
```
CREATE TABLE users(
   user_id INT AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(40),
   password VARCHAR(255),
   email VARCHAR(255)
); 
```
您还可以在CREATE TABLE  语句末尾指定PRIMARY KEY如下：
```
CREATE TABLE roles(
   role_id INT AUTO_INCREMENT,
   role_name VARCHAR(50),
   PRIMARY KEY(role_id)
); 
```
如果主键由多列组成，则必须在CREATE TABLE  语句末尾指定它们。您在括号中放置了以逗号分隔的主键列列表，并跟随PRIMARY KEY  关键字。
```
CREATE TABLE user_roles(
   user_id INT NOT NULL,
   role_id INT NOT NULL,
   PRIMARY KEY(user_id,role_id),
   FOREIGN KEY(user_id) REFERENCES users(user_id),
   FOREIGN KEY(role_id) REFERENCES roles(role_id)
); 
```
除了创建包含user_id和role_id列的主键之外，语句还创建了两个外键约束。

使用ALTER TABLE语句定义MySQL PRIMARY KEY约束
如果某个表由于某些原因没有主键，则可以使用ALTER TABLE语句定义表的主键，如以下语句所示：
```
ALTER TABLE table_name
ADD PRIMARY KEY(primary_key_column); 
```
以下示例将id列添加到主键。

首先，创建  t1表而不定义主键。
```
CREATE TABLE t1(
   id INT,
   title VARCHAR(255) NOT NULL
); 
```
其次，将id  列作为t1表的主键。
```
ALTER TABLE t1
ADD PRIMARY KEY(id); 
PRIMARY KEY与 UNIQUE KEY和 KEY
```
KEY是INDEX的同义词 。您可以使用 KEY为列或一组列创建索引，不是主键或唯一索引。

UNIQUE索引创建，其值必须是一列上的唯一值，与PRIMARY索引不同，MySQL允许UNIQUE索引中有NULL值。另外，表可以有多个UNIQUE索引。

例如，假设users表中的用户email和username 应是唯一的。您可以将email，username列定义为UNIQUE索引语句如下：

为username列添加 UNIQUE 索引。
```
ALTER TABLE users
ADD UNIQUE INDEX username_unique (username ASC) ;
``` 
为email列添加 UNIQUE 索引。
```
ALTER TABLE users
ADD UNIQUE INDEX email_unique (email ASC) ; 
```
可以使用如下命令查看表结果：
```
SHOW CREATE TABLE users; 
+-------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Table | Create Table                                                                                                                                                                                                                                                                                                                                 |
+-------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| users | CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_unique` (`username`),
  UNIQUE KEY `email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 |
+-------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)
```