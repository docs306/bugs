MySQL别名来提高查询的可读性。

MySQL支持两种别名，称为列别名和表别名。让我们详细检查每种别名。

MySQL 列别名
有时列的名称是不太好理解和记忆的，使得查询的输出很难理解。要为列提供描述性名称，请使用列别名。

以下语句说明了如何使用列别名：

```sql
SELECT 
 [column_1 | expression] AS descriptive_name
FROM table_name; 
```
要为列提供别名，请使用AS关键字后跟别名。如果别名包含空格，则必须按以下方式引用它（使用（`））：

```sql
SELECT 
 [column_1 | expression] AS `descriptive name`
FROM table_name; 
```
由于AS关键字是可选的，因此可以在语句中省略它。请注意，您还可以为表达式指定别名。

我们来看看示例数据库中的employees表。

```
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.01 sec)
```
以下查询选择员工的名字和姓氏，并将它们组合在一起以生成全名。CONCAT_WS函数用于连接名字和姓氏。

```sql
SELECT 
    CONCAT_WS(', ', lastName, firstname)
FROM
    employees; 
```
运行结果：

```
+--------------------------------------+
| CONCAT_WS(', ', lastName, firstname) |
+--------------------------------------+
| Murphy, Diane                        |
| Patterson, Mary                      |
| Firrelli, Jeff                       |
| Patterson, William                   |
| Bondur, Gerard                       |
...
```
列标题很难阅读。您可以为输出的标题指定列别名，使其更具可读性，如下面的查询：

```sql
SELECT
 CONCAT_WS(', ', lastName, firstname) AS `Full name`
FROM
 employees; 
```
运行结果：

```
+--------------------+
| Full name          |
+--------------------+
| Murphy, Diane      |
| Patterson, Mary    |
| Firrelli, Jeff     |
| Patterson, William |
| Bondur, Gerard     |
...
```
在MySQL中，可以使用ORDER BY，GROUP BY和HAVING子句中来引用列的别名。

以下查询使用ORDER BY子句中的列别名按字母顺序对员工的全名进行排序：

```sql
SELECT
 CONCAT_WS(', ', lastName, firstname) `Full name`
FROM
 employees
ORDER BY
 `Full name`; 
```
运行结果：
```
+--------------------+
| Full name          |
+--------------------+
| Bondur, Gerard     |
| Bondur, Loui       |
| Bott, Larry        |
| Bow, Anthony       |
| Castillo, Pamela   |
| Firrelli, Jeff     |
| Firrelli, Julie    |
...
```
以下语句选择总金额大于60000的订单。在GROUP BY和HAVING子句中使用列别名。

```sql
SELECT
 orderNumber `Order no.`,
 SUM(priceEach * quantityOrdered) total
FROM
 orderdetails
GROUP BY
 `Order no.`
HAVING
 total > 60000; 
```
运行结果：
```
+-----------+----------+
| Order no. | total    |
+-----------+----------+
|     10165 | 67392.85 |
|     10287 | 61402.00 |
|     10310 | 61234.67 |
+-----------+----------+
3 rows in set (0.02 sec)
```
注意：不能在WHERE子句中使用列别名。原因是当MySQL评估WHERE子句时，SELECT子句中指定的列的值可能尚未确定。

MySQL表别名
可以使用别名为表提供不同的名称。使用AS关键字为表分配别名，如下所示：

table_name AS table_alias 
表的别名称为表别名。与列别名一样，AS关键字是可选的，因此您可以省略它。

经常在INNER JOIN，LEFT JOIN，self JOIN子句和子查询的语句中使用表别名。

我们来看看customers和orders表。
```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)

+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| requiredDate   |
| shippedDate    |
| status         |
| comments       |
| customerNumber |
+----------------+
7 rows in set (0.01 sec)
```
两个表都具有相同的列名：customerNumber,如果不使用表别名来限定customerNumber列，您将收到如下错误消息：

```
Error Code: 1052. Column 'customerNumber' in on clause is ambiguous 
```
要避免此错误，请使用表别名来限定customerNumber列：

```sql
SELECT
 customerName,
 COUNT(o.orderNumber) total
FROM
 customers c
INNER JOIN orders o ON c.customerNumber = o.customerNumber
GROUP BY
 customerName
ORDER BY
 total DESC; 
```
运行结果：

```
+------------------------------------+-------+
| customerName                       | total |
+------------------------------------+-------+
| Euro+ Shopping Channel             |    26 |
| Mini Gifts Distributors Ltd.       |    17 |
| Down Under Souveniers, Inc         |     5 |
| Danish Wholesale Imports           |     5 |
| Australian Collectors, Co.         |     5 |
| Dragon Souveniers, Ltd.            |     5 |
...
```
上面的查询从customers和orders表中选择客户名称和订单数量。c用作customers表的表别名和o表orders的别名。customers和orders表中的列通过表别名引用。

如果不在上面的查询中使用别名，则必须使用表名来引用其列，这会使查询变得冗长且不太可读，如下所示：

```sql
SELECT
 customers.customerName,
 COUNT(orders.orderNumber) total
FROM
 customers
INNER JOIN orders ON customers.customerNumber = orders.customerNumber
GROUP BY
 customerName
ORDER BY
 total DESC 
```