MySQL INSERT IGNORE语句将数据插入表中。

MySQL INSERT IGNORE语句简介
当您使用INSERT语句向表中添加多行时，如果在处理过程中发生错误，MySQL将终止语句并返回错误。结果，没有行插入表中。

但是，如果使用INSERT IGNORE语句，将忽略导致错误的包含无效数据的行，并将具有有效数据的行插入到表中。

INSERT IGNORE语句的语法如下：

```sql
INSERT IGNORE INTO table(column_list)
VALUES( value_list),
      ( value_list),
      ... 
```
注意：IGNORE子句是MySQL对SQL标准的扩展。

MySQL INSERT IGNORE 实例
我们将创建一个的新表名为subscribers演示。

```sql
CREATE TABLE subscribers (
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL UNIQUE
); 
```
UNIQUE约束确保在email列中没有重复的电子邮件。

以下语句在subscribers表中插入一个新行   ：

```sql
INSERT INTO subscribers(email)
VALUES('begtut.com@gmail.com'); 
```
运行结果：

Query OK, 1 row affected (0.01 sec)
让我们执行另一个在 subscribers表中插入两行的语句 ：

```sql
INSERT INTO subscribers(email)
VALUES('begtut.com@gmail.com'), 
      ('jane.smith@ibm.com'); 
```
它返回一个错误。

ERROR 1062 (23000): Duplicate entry 'begtut.com@gmail.com' for key 'email'
如错误消息中所示，电子邮件begtut.com@gmail.com 违反了UNIQUE约束。

但是，如果您使用INSERT IGNORE语句。

```sql
INSERT IGNORE INTO subscribers(email)
VALUES('begtut.com@gmail.com'), 
      ('jane.smith@ibm.com'); 
```
MySQL返回一条消息，指示已插入一行而另一行被忽略。

```
Query OK, 1 row affected
Records: 2  Duplicates: 1  Warnings: 1
```
要查找警告的详细信息，可以使用SHOW WARNINGS如下所示的命令：

```sql
SHOW WARNINGS; 
mysql> SHOW WARNINGS;
+---------+------+--------------------------------------------------------+
| Level   | Code | Message                                                |
+---------+------+--------------------------------------------------------+
| Warning | 1062 | Duplicate entry 'begtut.com@gmail.com' for key 'email' |
+---------+------+--------------------------------------------------------+
2 rows in set
```
总之，当您使用INSERT IGNORE语句时，MySQL不会发出错误，而是在发生错误时发出警告。

如果从subscribers表中查询数据，您会发现实际只插入了一行，而导致错误的行则没有。

```
+----------------------+
| email                |
+----------------------+
| jane.smith@ibm.com   |
| begtut.com@gmail.com |
+----------------------+
2 rows in set (0.00 sec)
```
MySQL INSERT IGNORE和STRICT模式
当STRICT模式打开时，如果您尝试将无效值使用INSERT插入表中，MySQL将返回错误并中止语句。

但是，如果您使用INSERT IGNORE语句，MySQL将发出警告而不是错误。此外，在将值添加到表之前，它将尝试调整值以使其有效。

请考虑以下示例。

首先，我们创建一个的新表名为tokens：

```sql
CREATE TABLE tokens (
    s VARCHAR(6)
); 
```
在此表中，列s仅接受长度小于或等于6的字符串。

其次，将长度为7的字符串插入tokens表中。

INSERT INTO tokens VALUES('abcdefg'); 
MySQL发出以下错误，因为严格模式已启用。

ERROR 1406 (22001): Data too long for column 's' at row 1
第三，使用INSERT IGNORE语句插入相同的字符串。

INSERT IGNORE INTO tokens VALUES('abcdefg'); 
MySQL在将数据插入tokens表之前截断了数据。此外，它会发出警告。
```
+---------+------+----------------------------------------+
| Level   | Code | Message                                |
+---------+------+----------------------------------------+
| Warning | 1265 | Data truncated for column 's' at row 1 |
+---------+------+----------------------------------------+
1 row in set (0.00 sec)
```