# MySQL LEFT JOIN

LEFT JOIN 子句以及如何将其应用于从两个或多个数据库表中查询数据。

- MySQL LEFT JOIN子句允许您查询来自两个或多个数据库表的数据。
- LEFT JOIN子句是SELECT语句的可选部分，出现在FROM子句之后。

LEFT JOIN 连接两个表的语法：
```sql
SELECT t1.c1, t1.c2, t2.c1, t2.c2
FROM t1
LEFT JOIN t2 ON t1.c1 = t2.c1; 
```
使用LEFT JOIN子句将t1表连接到t2表，如果左表t1中的行与t2基于连接条件（t1.c1 = t2.c1）的右表中的行匹配，则此行将包含在结果集中。

如果左表，右表中的行(的值)不匹配，则依旧会选择左表中的行 并将其与 右表中的行组合 其值为 NULL。

换句话说，LEFT JOIN子句允许您从左表和右表中选择匹配的行，及左表（t1）中的所有行，即使在右表（t2）中找不到匹配的行也是如此。

*注：如果查询中有WHERE和 HAVING子句，则返回匹配条件的行。*


### LEFT JOIN 连接两个表
我们来看看示例数据库中的customers和orders表。
```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| phone                  |
+------------------------+

+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| status         |
| comments       |
| customerNumber |
+----------------+
```
在上面的数据库图表中：

orders表中的每个订单都必须属于customers表中的客户。
customers表中的每个客户可以在表中包含零个或多个订单orders。

要查找属于每个客户的所有订单，可以使用 LEFT JOIN：
```sql
SELECT
 c.customerNumber,
 c.customerName,
 o.orderNumber,
 o.status
FROM customers c
LEFT JOIN orders o ON c.customerNumber = o.customerNumber; 
```
运行结果：
```
+----------------+------------------------------------+-------------+------------+
| customerNumber | customerName                       | orderNumber | status     |
+----------------+------------------------------------+-------------+------------+
|            103 | Atelier graphique                  |       10123 | Shipped    |
|            103 | Atelier graphique                  |       10345 | Shipped    |
|            112 | Signal Gift Stores                 |       10124 | Shipped    |
|            112 | Signal Gift Stores                 |       10346 | Shipped    |
|            114 | Australian Collectors, Co.         |       10120 | Shipped    |
|            114 | Australian Collectors, Co.         |       10125 | Shipped    |
...
```
因此，左表customers是所有客户都包含在结果集中。但是，结果集中有的行具没有订单数据，例如168,169等。这些行中的订单数据是NULL。这意味着这些客户在orders表中没有任何订单。

因为我们使用相同的列名（customerNumber）来连接两个表，所以我们可以使用以下语法缩短查询：

```sql
SELECT
 c.customerNumber,
 c.customerName,
 o.orderNumber,
 o.status
FROM customers c
LEFT JOIN orders o USING (customerNumber); 
```
- USING 函数
```sql
USING (customerNumber) 
--相当于
ON c.customerNumber = o.customerNumber 
```
如果您用LEFT JOIN语句替换成INNER JOIN，将获得已下订单的客户。


- MySQL LEFT JOIN 查找不匹配的行

查找两个表之间的不匹配行，可以WHERE在SELECT语句中添加一个子句，以仅查询右表中列值包含NULL值的行。

例如查找未下订单的所有客户：
```sql
SELECT 
    c.customerNumber, 
    c.customerName, 
    o.orderNumber, 
    o.status
FROM customers c
LEFT JOIN orders o ON c.customerNumber = o.customerNumber
WHERE orderNumber IS NULL; 
```
运行结果：
```
+----------------+--------------------------------+-------------+--------+
| customerNumber | customerName                   | orderNumber | status |
+----------------+--------------------------------+-------------+--------+
|            125 | Havel & Zbyszek Co             |        NULL | NULL   |
|            168 | American Souvenirs Inc         |        NULL | NULL   |
|            247 | Messner Shopping Network       |        NULL | NULL   |
```

WHERE子句与ON子句中的条件
```sql
SELECT 
    o.orderNumber, 
    od.customerNumber, 
    od.productCode
FROM orders o
LEFT JOIN orderDetails od USING (orderNumber)
WHERE orderNumber = 10123; 
```
此例中使用 LEFT JOIN 子句来查询 orders和 orderDetails表中的数据。

运行结果：
```
+-------------+----------------+-------------+
| orderNumber | customerNumber | productCode |
+-------------+----------------+-------------+
|       10123 |            103 | S18_1589    |
|       10123 |            103 | S18_2870    |
|       10123 |            103 | S18_3685    |
+-------------+----------------+-------------+
```


如果将条件从WHERE子句移动到ON子句：
```sql
SELECT 
    o.orderNumber, 
    d.customerNumber, 
    d.productCode
FROM orders o
LEFT JOIN orderDetails d ON o.orderNumber = d.orderNumber AND o.orderNumber = 10123; 
```
这种情况下，查询将返回所有订单，但只有订单10123将具有与之关联的详细信息，如下所示。

运行结果：
```
+-------------+----------------+-------------+
| orderNumber | customerNumber | productCode |
+-------------+----------------+-------------+
|       10123 |            103 | S18_1589    |
|       10123 |            103 | S18_2870    |
|       10123 |            103 | S18_3685    |
|       10123 |            103 | S24_1628    |
|       10298 |            103 | NULL        |
|       10346 |            112 | NULL        |
```
*注：INNER JOIN子句中的ON子句条件等同于WHERE子句中的条件。*