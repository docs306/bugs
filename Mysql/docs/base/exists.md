MySQL EXISTS运算符以及何时使用它来提高查询性能。

MySQL EXISTS运算符简介
EXISTS是一个布尔运算符返回true或false。EXISTS经常使用的在一个子查询，以测试一个“存在”状态。

以下说明了EXISTS运营商的常见用法。

```sql
SELECT 
    select_list
FROM
    a_table
WHERE
    [NOT] EXISTS(subquery); 
```
如果子查询返回任何行，则EXISTS运算符返回true，否则返回false。

此外，EXISTS一旦找到匹配的行，立即终止进一步的处理。由于此特性，您可以EXISTS在某些情况下使用运算符来提高查询的性能。

NOT EXISTS是否定操作符。换句话说，如果子查询没有返回任何行，NOT EXISTS则返回true，否则返回false。

您可以使用SELECT *，SELECT column，SELECT a_constant，或在子查询任何东西。结果是相同的，因为MySQL忽略select_list在 SELECT子句中出现的结果。

MySQL EXISTS示例
让我们举一些使用EXISTS运算符来了解它是如何工作的例子。

MySQL SELECT EXISTS示例
我们来看看示例数据库中的customers和orders表。
```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)

+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| requiredDate   |
| shippedDate    |
| status         |
| comments       |
| customerNumber |
+----------------+
7 rows in set (0.00 sec)
```
假设您要查找至少有一个销售订单的客户，请按以下方式使用EXISTS运算符：

```sql
SELECT 
    customerNumber, customerName
FROM
    customers
WHERE
    EXISTS( SELECT 
            1
        FROM
            orders
        WHERE
            orders.customernumber = customers.customernumber); 
```
运行结果：
```
+----------------+------------------------------------+
| customerNumber | customerName                       |
+----------------+------------------------------------+
|            103 | Atelier graphique                  |
|            112 | Signal Gift Stores                 |
|            114 | Australian Collectors, Co.         |
|            119 | La Rochelle Gifts                  |
|            121 | Baane Mini Imports                 |
|            124 | Mini Gifts Distributors Ltd.       |
|            128 | Blauer See Auto, Co.               |
|            129 | Mini Wheels Co.                    |
|            131 | Land of Toys Inc.                  |
...
```
对于在customers表每一行，查询检查customerNumber 的orders表。

如果表customerNumber中出现的customers表存在于orders表中，则子查询返回第一个匹配的行。结果EXISTS运算符返回true并停止扫描orders表。否则，子查询不返回任何行EXISTS操作符返回false。

要获得未有任何销售订单的客户，请使用NOT EXISTS运算符作为以下语句：

```sql
SELECT 
    customerNumber, customerName
FROM
    customers
WHERE
    NOT EXISTS( SELECT 
            1
        FROM
            orders
        WHERE
            orders.customernumber = customers.customernumber); 
```
运行结果：

```
+----------------+--------------------------------+
| customerNumber | customerName                   |
+----------------+--------------------------------+
|            125 | Havel & Zbyszek Co             |
|            168 | American Souvenirs Inc         |
|            169 | Porto Imports Co.              |
|            206 | Asian Shopping Network, Co     |
|            223 | Natrlich Autos                 |
|            237 | ANG Resellers                  |
|            247 | Messner Shopping Network       |
|            273 | Franken Gifts, Co              |
...
```
MySQL UPDATE EXISTS示例
假设您必须更新在旧金山办事处工作的员工的电话分机。

要查找在San Franciso办公室工作的员工，请使用EXISTS运算符作为以下UPDATE语句：

```sql
SELECT 
    employeenumber, firstname, lastname, extension
FROM
    employees
WHERE
    EXISTS( SELECT 
            1
        FROM
            offices
        WHERE
            city = 'San Francisco'
                AND offices.officeCode = employees.officeCode); 
```
运行结果：
```
+----------------+-----------+-----------+-----------+
| employeenumber | firstname | lastname  | extension |
+----------------+-----------+-----------+-----------+
|           1002 | Diane     | Murphy    | x5800     |
|           1056 | Mary      | Patterson | x4611     |
|           1076 | Jeff      | Firrelli  | x9273     |
|           1143 | Anthony   | Bow       | x5428     |
|           1165 | Leslie    | Jennings  | x3291     |
|           1166 | Leslie    | Thompson  | x4065     |
+----------------+-----------+-----------+-----------+
6 rows in set (0.01 sec)
```
假设你想在谁在旧金山的办公室工作的员工的每一个电话分机添加的号码5，您可以在UPDATE语句中WHERE的子句使用EXISTS如下：

```sql
UPDATE employees 
SET 
    extension = CONCAT(extension, '1')
WHERE
    EXISTS( SELECT 
            1
        FROM
            offices
        WHERE
            city = 'San Francisco'
                AND offices.officeCode = employees.officeCode); 
```
MySQL INSERT EXISTS 实例
假设您要将未在任何销售订单中放置的客户归档到单独的表中。为此，请按照以下步骤操作。

首先，通过从customers表中复制结构来创建一个的新表用于归档客户。

```
CREATE TABLE customers_archive LIKE customers; 
```
其次，使用以下INSERT语句将没有任何销售订单的客户插入customers_archive表中。

```sql
INSERT INTO customers_archive
SELECT * FROM customers
WHERE NOT EXISTS( SELECT 
            1
        FROM
            orders
        WHERE
            orders.customernumber = customers.customernumber); 
```
第三，  从表中查询数据customers_archive以验证插入操作。

```sql
SELECT 
    *
FROM
    customers_archive; 
```
运行结果：
```
+----------------+--------------------------------+-----------------+------------------+------------------+----------------------------------+--------------+------------------+----------+------------+--------------+------------------------+-------------+
| customerNumber | customerName                   | contactLastName | contactFirstName | phone            | addressLine1                     | addressLine2 | city             | state    | postalCode | country      | salesRepEmployeeNumber | creditLimit |
+----------------+--------------------------------+-----------------+------------------+------------------+----------------------------------+--------------+------------------+----------+------------+--------------+------------------------+-------------+
|            125 | Havel & Zbyszek Co             | Piestrzeniewicz | Zbyszek          | (26) 642-7555    | ul. Filtrowa 68                  | NULL         | Warszawa         | NULL     | 01-012     | Poland       |                   NULL |        0.00 |
|            168 | American Souvenirs Inc         | Franco          | Keith            | 2035557845       | 149 Spinnaker Dr.                | Suite 101    | New Haven        | CT       | 97823      | USA          |                   1286 |        0.00 |
|            169 | Porto Imports Co.              | de Castro       | Isabel           | (1) 356-5555     | Estrada da sade n. 58            | NULL         | Lisboa           | NULL     | 1756       | Portugal     |                   NULL |        0.00 |
|            206 | Asian Shopping Network, Co     | Walker          | Brydey           | +612 9411 1555   | Suntec Tower Three               | 8 Temasek    | Singapore        | NULL     | 038988     | Singapore    |                   NULL |        0.00 |
...
```
MySQL DELETE EXISTS 实例
归档客户数据的最后一项任务是customers_archive从customers表中删除表中存在的客户。

为此，请使用DELETE语句中的WHERE子句中使用EXISTS，如下所示：

```sql
DELETE FROM customers
WHERE
    EXISTS( SELECT 
        1
    FROM
        customers_archive a
    
    WHERE
        a.customernumber = customers.customerNumber); 
```
MySQL EXISTS与IN
要查找至少有一个销售订单的客户，可以使用IN运算符，如下所示：

```sql
SELECT 
    customerNumber, customerName
FROM
    customers
WHERE
    customerNumber IN (SELECT 
            customerNumber
        FROM
            orders); 
```
让我们比较使用的查询IN操作与使用的一个EXISTS使用操作符EXPLAIN的语句。

```sql
EXPLAIN SELECT 
    customerNumber, customerName
FROM
    customers
WHERE
    EXISTS( SELECT 
            1
        FROM
            orders
        WHERE
            orders.customernumber = customers.customernumber); 
```
现在，检查使用IN运算符的查询的性能。

```sql
SELECT 
    customerNumber, customerName
FROM
    customers
WHERE
    customerNumber IN (SELECT 
            customerNumber
        FROM
            orders); 
```
MySQL EXISTS与IN-IN性能

使用EXISTS运算符的查询比使用IN运算符的查询要快得多。

原因是EXISTS操作员基于“至少找到”原则工作。它返回true并在找到至少一个匹配行后停止扫描表。

另一方面，当IN运算符与子查询组合时，MySQL必须首先处理子查询，然后使用子查询的结果来处理整个查询。

一般的经验法则是，如果子查询包含大量数据，则EXISTS运算符可提供更好的性能。

但是，如果从子查询返回的结果集非常小，则使用IN运算符的查询将执行得更快。

例如，以下声明使用IN操作员选择在旧金山办公室工作的所有员工。

```sql
SELECT 
    employeenumber, firstname, lastname
FROM
    employees
WHERE
    officeCode IN (SELECT 
            officeCode
        FROM
            offices
        WHERE
            offices.city = 'San Francisco'); 
```
我们来检查查询的性能。

MySQL IN vs EXISTS

它比我们在第一个示例中提到的使用EXISTS运算符的查询快一点。请参阅EXIST下面使用运算符的查询的性能：

MySQL EXISTS与IN-IN相比更快

在本教程中，我们讨论了MySQL EXISTS运算符，并向您介绍了使用EXISTS运算符提高查询性能的一些指导原则。