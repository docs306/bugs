MySQL LIMIT子句来约束查询返回的行数。

MySQL LIMIT子句简介
SELECT语句中使用LIMIT 子句来约束结果集中的行数。LIMIT子句接受一个或两个参数。两个参数的值必须为零或正整数。

以下说明了LIMIT带有两个参数的子句语法：

```sql
SELECT 
    column1,column2,...
FROM
    table
LIMIT offset , count; 
```
我们来看一下子LIMIT句参数：

offset第一行的偏移规定返回。offset第一行的是0，而不是1。
count指定要返回行的最大数目。
mysql限制偏移量

当您使用带有一个参数的 LIMIT 子句时，此参数将用于确定从结果集的开头返回的最大行数。

```sql
SELECT 
   select_list
FROM
   table
LIMIT count; 
```
上面的查询等效于以下查询，LIMIT子句接受两个参数：

```sql
SELECT 
    select_list
FROM
    table
LIMIT 0 , count; 
```
使用MySQL LIMIT获取前N行
您可以使用LIMIT子句 从表中选择第N 行，如以下查询所示：

```sql
SELECT 
    column1,column2,...
FROM
    table
LIMIT N; 
```
例如，此查询选择前10个客户：

```sql
SELECT
 customernumber,
 customername,
 creditlimit
FROM
 customers
LIMIT 10; 
```
运行结果：

```
+----------------+------------------------------+-------------+
| customernumber | customername                 | creditlimit |
+----------------+------------------------------+-------------+
|            103 | Atelier graphique            |    21000.00 |
|            112 | Signal Gift Stores           |    71800.00 |
|            114 | Australian Collectors, Co.   |   117300.00 |
|            119 | La Rochelle Gifts            |   118200.00 |
|            121 | Baane Mini Imports           |    81700.00 |
|            124 | Mini Gifts Distributors Ltd. |   210500.00 |
|            125 | Havel & Zbyszek Co           |        0.00 |
|            128 | Blauer See Auto, Co.         |    59700.00 |
|            129 | Mini Wheels Co.              |    64600.00 |
|            131 | Land of Toys Inc.            |   114900.00 |
+----------------+------------------------------+-------------+
10 rows in set (0.01 sec)
```
使用MySQL LIMIT获取最高和最低值
LIMIT经常与ORDER BY一起使用。

首先，使用ORDER BY  子句根据特定条件对结果集进行排序，然后使用 LIMIT子句查找最低或最高值。

请参阅示例数据库中的下customers表。

```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.02 sec)
```
以下示例返回信用额度最高的前五位客户：

```sql
SELECT
 customernumber,
 customername,
 creditlimit
FROM
 customers
ORDER BY
 creditlimit DESC
LIMIT 5; 
```
运行结果：

```
+----------------+------------------------------+-------------+
| customernumber | customername                 | creditlimit |
+----------------+------------------------------+-------------+
|            141 | Euro+ Shopping Channel       |   227600.00 |
|            124 | Mini Gifts Distributors Ltd. |   210500.00 |
|            298 | Vida Sport, Ltd              |   141300.00 |
|            151 | Muscle Machine Inc           |   138500.00 |
|            187 | AV Stores, Co.               |   136800.00 |
+----------------+------------------------------+-------------+
5 rows in set (0.00 sec)
```
以下查询返回信用额度最低的前五位客户：

```sql
SELECT
 customernumber,
 customername,
 creditlimit
FROM
 customers
ORDER BY
 creditlimit ASC
LIMIT 5; 
```
运行结果：

```
+----------------+----------------------------+-------------+
| customernumber | customername               | creditlimit |
+----------------+----------------------------+-------------+
|            223 | Natrlich Autos             |        0.00 |
|            168 | American Souvenirs Inc     |        0.00 |
|            169 | Porto Imports Co.          |        0.00 |
|            206 | Asian Shopping Network, Co |        0.00 |
|            125 | Havel & Zbyszek Co         |        0.00 |
+----------------+----------------------------+-------------+
5 rows in set (0.00 sec)
```
使用MySQL LIMIT获得第N个最高值
MySQL中最棘手的问题之一是选择结果集中的第N个最高值，例如，选择第二个（或第n个）最昂贵的产品，您无法使用MAX或MIN无法回答这些产品。但是，可以使用MySQL LIMIT来解决这些问题。

 按降序对结果集进行排序。
使用LIMIT条款获得第n个最昂贵的产品。
常见的查询模式如下：

```sql
SELECT 
    select_list
FROM
    table
ORDER BY sort_expression DESC
LIMIT nth-1, count; 
```
我们来看一个例子吧。我们将使用示例数据库中的products表进行演示。

```
+--------------------+
| products           |
+--------------------+
| productCode        |
| productName        |
| productLine        |
| productScale       |
| productVendor      |
| productDescription |
| quantityInStock    |
| buyPrice           |
| MSRP               |
+--------------------+
9 rows in set (0.02 sec)
```
请参阅以下产品结果集：

```sql
SELECT
 productName,
 buyprice
FROM
 products
ORDER BY
 buyprice DESC; 
```
运行结果：

```
+---------------------------------------------+----------+
| productName                                 | buyprice |
+---------------------------------------------+----------+
| 1962 LanciaA Delta 16V                      |   103.42 |
| 1998 Chrysler Plymouth Prowler              |   101.51 |
| 1952 Alpine Renault 1300                    |    98.58 |
| 1956 Porsche 356A Coupe                     |    98.30 |
| 2001 Ferrari Enzo                           |    95.59 |
| 1968 Ford Mustang                           |    95.34 |
```
我们的目的是从获得高价排序的产品中取第二昂贵的产品。因此，可以使用LIMIT子句选择第二行作为以下查询:(注意偏移从零开始）

```sql
SELECT
 productName,
 buyprice
FROM
 products
ORDER BY
 buyprice DESC
LIMIT 1, 1; 
```
运行结果：

```
+--------------------------------+----------+
| productName                    | buyprice |
+--------------------------------+----------+
| 1998 Chrysler Plymouth Prowler |   101.51 |
+--------------------------------+----------+
1 row in set (0.00 sec)
```