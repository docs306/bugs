MySQL DELETE语句从单个表中删除数据。

MySQL DELETE语句简介
要从表中删除数据，请使用MySQL DELETE语句。以下说明了DELETE语句的语法：

```sql
DELETE FROM table_name
WHERE condition; 
```
在这个声明中：

首先，指定从中删除数据的表。
其次，使用条件指定WHERE子句中要删除的行。如果行符合条件，则将删除行。
注意：WHERE子句是可选项。如果省略WHERE子句，DELETE语句将删除表中的所有行。

除了从表中删除数据外，DELETE语句还返回已删除的行数。

要使用单个DELETE语句从多个表中删除数据，请使用DELETE JOIN我们将在下一个教程中介绍的语句。

要删除表中的所有行而不需要知道删除了多少行，您应使用  TRUNCATE TABLE语句来获得更好的性能。

对于具有外键约束的表，当从父表中删除行时，将使用ON DELETE CASCADE选项自动删除子表中的行。

MySQL的DELETE例子
我们将使用示例数据库中的employees表进行演示。
```
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.04 sec)
```
注意：一旦删除数据，它就会消失。因此，您应在执行下一节中的语句之前备份数据库DELETE。
假设您要删除其officeNumber= 4的员工，请使用DELETE带有WHERE子句的语句，如以下查询所示：

```
DELETE FROM employees 
WHERE
    officeCode = 4; 
```
要删除employees表中的所有行，请使用DELETE不带WHERE子句的语句，如下所示：

DELETE FROM employees; 
employees表中的所有行都已删除。

MySQL DELETE和LIMIT子句
如果要限制要删除的行数，请使用以下LIMIT子句：

DELETE FROM table
LIMIT row_count; 
注意：表中的行顺序是未指定的，因此，在使用LIMIT子句时，应始终使用ORDER BY子句。

```sql
DELETE FROM table_name
ORDER BY c1, c2, ...
LIMIT row_count; 
```
请考虑示例数据库中的以下customers表：
```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.02 sec)
```
例如，以下语句按字母顺序按客户名称对客户进行排序，并删除前10个客户：

```sql
DELETE FROM customers
ORDER BY customerName
LIMIT 10; 
同样，以下DELETE语句选择客户France，按信用额度从低到高对其进行排序，并删除前5个客户：

DELETE FROM customers
WHERE country = 'France'
ORDER BY creditLimit
LIMIT 5; 
```