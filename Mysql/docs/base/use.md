mysql使用USE语句在程序和MySQL Workbench中选择数据库  。

要选择要使用的特定数据库，请USE按如下方式发出语句：

USE database_name; 
在此语句中，USE关键字后面是您要选择的数据库的名称。

通过命令行选择MySQL数据库
以下步骤演示了如何使用mysql程序登录MySQL Server 并选择要使用的数据库：

首先，使用指定用户登录MySQL，例如root：
```
>mysql -u root -p
```
在此命令中，我们已使用-uflag 指定用户root，然后使用-p标志。MySQL提示输入用户的密码。您需要输入密码并按Enter登录。

其次，发出USE命令来选择数据库，例如mysqldemo：
```
mysql> use mysqldemo;
Database changed
```
在此命令中，我们选择了mysqldemo数据库。如果数据库存在，MySQL发出消息“ Database changed”。

如果数据库不存在，MySQL会发出以下错误：
```
mysql> use abc; 
ERROR 1049 (42000): Unknown database 'abc'
```
第三，您可以使用以下database()函数获取当前连接的数据库的名称：
```
mysql> select database();
+------------+
| database() |
+------------+
| mysqldemo  |
+------------+
1 row in set (0.00 sec)
```
最后，要选择另一个数据库，您只需要使用USE语句和新数据库名称，例如：

```
mysql> use testdb;
Database changed
```
登录时选择数据库
如果在登录之前知道要使用哪个数据库，则可以使用-D标志指定它，如下所示：
```
>mysql -u root -D mysqldemo -p
```
在此命令中，我们mysqldemo在-D标志后面指定了数据库名称。因为我们使用了-pflag，所以MySQL提示输入root用户的密码。您只需提供密码并按下Enter即可登录。

登录后，您可以检查当前数据库：
```
mysql> select database();
+------------+
| database() |
+------------+
| mysqldemo  |
+------------+
1 row in set (0.00 sec)
```