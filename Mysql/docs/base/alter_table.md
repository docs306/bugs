ALTER TABLE 语句修改现有表的结构。它允许您添加列，删除列，更改列的数据类型，添加主键，重命名表等等。

以下说明了ALTER TABLE  语句语法：
```
ALTER TABLE table_name action1[,action2,…] 
```
要更改现有表的结构：

首先，在ALTER TABLE  子句后指定要更改的表名。
其次，列出要应用于表的一组操作。操作可以是任何操作，例如添加新列，添加主键或重命名表。ALTER TABLE  语句允许您在单个ALTER TABLE语句中应用多个操作，操作由逗号（，）分隔。
让我们创建一个的新表用于练习ALTER TABLE  语句。

下面的语句创建一个名为tasks新表中的  示例数据库：
```
CREATE TABLE tasks (
    task_id INT NOT NULL,
    subject VARCHAR(45) NULL,
    start_date DATE NULL,
    end_date DATE NULL,
    description VARCHAR(200) NULL,
    PRIMARY KEY (task_id),
    UNIQUE INDEX task_id_unique (task_id ASC)
); 
```
PHP MySQL更新：任务表

使用MySQL ALTER TABLE语句更改列
A）使用MySQL ALTER TABLE语句为列设置自动增量属性
假设您希望每次向 tasks表中插入新行时， task_id列的值都会自动增加一列。为此，您可以使用ALTER TABLE语句将列 task_id的属性设置为：AUTO_INCREMENT
```
ALTER TABLE tasks
CHANGE COLUMN task_id task_id INT(11) NOT NULL AUTO_INCREMENT; 
```
您可以通过向tasks  表中插入一些行来验证更改。
```
INSERT INTO tasks(subject,
                  start_date,
                  end_date,
   description)
VALUES('Learn MySQL ALTER TABLE',
       Now(),
       Now(),
      'Practicing MySQL ALTER TABLE statement');
 
INSERT INTO tasks(subject,
                  start_date,
                  end_date,
           description)
VALUES('Learn MySQL CREATE TABLE',
       Now(),
       Now(),
      'Practicing MySQL CREATE TABLE statement'); 
```
并且您可以查询数据以查看task_id  每次插入新行时列的值  是否增加1：
```
SELECT 
    task_id, description
FROM
    tasks; 
+---------+-----------------------------------------+
| task_id | description                             |
+---------+-----------------------------------------+
|       1 | Practicing MySQL ALTER TABLE statement  |
|       2 | Practicing MySQL CREATE TABLE statement |
+---------+-----------------------------------------+
2 rows in set (0.00 sec)
```
B）使用MySQL ALTER TABLE语句将新列添加到表示例中
由于对新的业务需求，需要添加一个新列名为complete  录制完成的百分比在每个任务tasks  。在这种情况下，您可以使用ALTER TABLE  向tasks表中添加新列，  如下所示：
```
ALTER TABLE tasks 
ADD COLUMN complete DECIMAL(2,1) NULL
AFTER description; 
```
C）使用MySQL ALTER TABLE从表示例中删除列
假设您不想在tasks  表中存储任务描述，则必须将其删除。以下语句允许您删除表tasks的 description 列：
```
ALTER TABLE tasks
DROP COLUMN description; 
```
使用MySQL ALTER TABLE 语句重命名表
您可以使用ALTER TABLE  语句重命名表。请注意，在重命名表之前，应认真考虑是否更改会影响数据库和应用程序层。

以下语句将tasks 表重新命名为work_items表：
```
ALTER TABLE tasks
RENAME TO work_items; 
```