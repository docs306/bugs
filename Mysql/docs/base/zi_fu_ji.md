MySQL字符集简介
MySQL字符集是一组在字符串中合法的字符。例如，有一个字母，字母从a到z。我们分配每个字母的数，例如，a = 1，b = 2等。a 是一个符号，和数字1 以字母相关联a 是的编码。来自a到z的所有字母及其对应编码的组合是字符集。

每个字符集都有一个或多个排序规则，这些排序规则定义了一组用于比较字符集中字符的规则。查看  MySQL排序规则  教程，了解MySQL中的排序规则。

MySQL支持各种字符集，允许您存储字符串中的几乎每个字符。要获取MySQL数据库服务器中的所有可用字符集，请使用以下  SHOW CHARACTER SET  语句：

SHOW CHARACTER SET; 
+----------+---------------------------------+---------------------+--------+
| Charset  | Description                     | Default collation   | Maxlen |
+----------+---------------------------------+---------------------+--------+
| big5     | Big5 Traditional Chinese        | big5_chinese_ci     |      2 |
| dec8     | DEC West European               | dec8_swedish_ci     |      1 |
| cp850    | DOS West European               | cp850_general_ci    |      1 |
| hp8      | HP West European                | hp8_english_ci      |      1 |
| koi8r    | KOI8-R Relcom Russian           | koi8r_general_ci    |      1 |
| latin1   | cp1252 West European            | latin1_swedish_ci   |      1 |
| latin2   | ISO 8859-2 Central European     | latin2_general_ci   |      1 |
| swe7     | 7bit Swedish                    | swe7_swedish_ci     |      1 |
| ascii    | US ASCII                        | ascii_general_ci    |      1 |
| ujis     | EUC-JP Japanese                 | ujis_japanese_ci    |      3 |
| sjis     | Shift-JIS Japanese              | sjis_japanese_ci    |      2 |
| hebrew   | ISO 8859-8 Hebrew               | hebrew_general_ci   |      1 |
| tis620   | TIS620 Thai                     | tis620_thai_ci      |      1 |
| euckr    | EUC-KR Korean                   | euckr_korean_ci     |      2 |
| koi8u    | KOI8-U Ukrainian                | koi8u_general_ci    |      1 |
| gb2312   | GB2312 Simplified Chinese       | gb2312_chinese_ci   |      2 |
| greek    | ISO 8859-7 Greek                | greek_general_ci    |      1 |
| cp1250   | Windows Central European        | cp1250_general_ci   |      1 |
| gbk      | GBK Simplified Chinese          | gbk_chinese_ci      |      2 |
| latin5   | ISO 8859-9 Turkish              | latin5_turkish_ci   |      1 |
| armscii8 | ARMSCII-8 Armenian              | armscii8_general_ci |      1 |
| utf8     | UTF-8 Unicode                   | utf8_general_ci     |      3 |
| ucs2     | UCS-2 Unicode                   | ucs2_general_ci     |      2 |
| cp866    | DOS Russian                     | cp866_general_ci    |      1 |
| keybcs2  | DOS Kamenicky Czech-Slovak      | keybcs2_general_ci  |      1 |
| macce    | Mac Central European            | macce_general_ci    |      1 |
| macroman | Mac West European               | macroman_general_ci |      1 |
| cp852    | DOS Central European            | cp852_general_ci    |      1 |
| latin7   | ISO 8859-13 Baltic              | latin7_general_ci   |      1 |
| utf8mb4  | UTF-8 Unicode                   | utf8mb4_general_ci  |      4 |
| cp1251   | Windows Cyrillic                | cp1251_general_ci   |      1 |
| utf16    | UTF-16 Unicode                  | utf16_general_ci    |      4 |
| utf16le  | UTF-16LE Unicode                | utf16le_general_ci  |      4 |
| cp1256   | Windows Arabic                  | cp1256_general_ci   |      1 |
| cp1257   | Windows Baltic                  | cp1257_general_ci   |      1 |
| utf32    | UTF-32 Unicode                  | utf32_general_ci    |      4 |
| binary   | Binary pseudo charset           | binary              |      1 |
| geostd8  | GEOSTD8 Georgian                | geostd8_general_ci  |      1 |
| cp932    | SJIS for Windows Japanese       | cp932_japanese_ci   |      2 |
| eucjpms  | UJIS for Windows Japanese       | eucjpms_japanese_ci |      3 |
| gb18030  | China National Standard GB18030 | gb18030_chinese_ci  |      4 |
+----------+---------------------------------+---------------------+--------+
41 rows in set (0.00 sec)
MySQL中的默认字符集是latin1。如果要将多种语言的字符存储在单个列中，可以使用Unicode字符集，即utf8或ucs2。

Maxlen列中的值指定字符集中的字符所包含的字节数。某些字符集包含单字节字符例如latin1，latin2，cp850等等，而其它字符集包含多字节字符。

MySQL提供了LENGTH以字节为单位获取字符串长度的函数，以及以字符为单位获取字符串长度的 CHAR_LENGTH 函数。如果字符串包含多字节字符，则  LENGTH函数的结果大于函数的结果  CHAR_LENGTH()。请参阅以下示例：

SET @str = CONVERT('MySQL Character Set' USING ucs2);
SELECT LENGTH(@str), CHAR_LENGTH(@str); 
+--------------+-------------------+
| LENGTH(@str) | CHAR_LENGTH(@str) |
+--------------+-------------------+
|           38 |                19 |
+--------------+-------------------+
1 row in set (0.00 sec)
CONVERT函数将字符串转换为特定字符集。在此示例中，它将MySQL Character Set  字符串的字符集转换为ucs2。由于ucs2字符集包含2字节字符，因此@str  字符串的长度（以字节为单位）大于字符长度。

请注意，某些字符集包含多字节字符，但它们的字符串可能只包含单字节字符，例如，utf8  如以下语句所示：

SET @str = CONVERT('MySQL Character Set' USING utf8);
SELECT LENGTH(@str), CHAR_LENGTH(@str); 
+--------------+-------------------+
| LENGTH(@str) | CHAR_LENGTH(@str) |
+--------------+-------------------+
|           19 |                19 |
+--------------+-------------------+
1 row in set (0.00 sec)
但是，如果utf8一串包含特殊字符如 ü 中 pingüino 串; 它的长度（以字节为单位）不同，请参见以下示例：

SET @str = CONVERT('pingüino' USING utf8);
SELECT LENGTH(@str), CHAR_LENGTH(@str); 
+--------------+-------------------+
| LENGTH(@str) | CHAR_LENGTH(@str) |
+--------------+-------------------+
|            9 |                 8 |
+--------------+-------------------+
1 row in set (0.00 sec)
在不同的字符集之间转换
MySQL提供了两个函数，允许在不同的字符集之间转换字符串：CONVERT和CAST。在上面的例子中多次使用过CONVERT这个函数。

CONVERT函数的语法如下：

CONVERT(expression USING character_set_name) 
CAST功能类似于CONVERT功能。它将字符串转换为不同的字符集：

CAST(string AS character_type CHARACTER SET character_set_name) 
使用CAST函数：

SELECT CAST(_latin1'MySQL character set' AS CHAR CHARACTER SET utf8); 
设置客户端连接的字符集
当应用程序与MySQL数据库服务器交换数据时，默认字符集为latin1。但是，如果数据库在utf8字符集中存储Unicode字符串，则使用latin1应用程序中的字符集是不够的。因此，应用程序在连接到MySQL数据库服务器时需要指定正确的字符集。

要为客户端连接配置字符集，可以执行以下方法之一：

SET NAME  在客户端连接到MySQL数据库服务器之后发出语句。例如，要设置Unicode字符集utf8，请使用以下语句：
SET NAMES 'utf8'; 
如果应用程序支持--default-character-set 选项，则可以使用它来设置字符集。例如，mysql客户端工具支持--default-character-set，可以在配置文件中进行如下设置：
[mysql]
default-character-set=utf8 
某些MySQL连接器允许您设置字符集，例如，如果使用PHP PDO，则可以在数据源名称中设置字符集，如下所示：