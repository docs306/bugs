MySQL 数据类型
简介：在本教程中，您将了解  MySQL数据类型 以及如何在MySQL中设计数据库时有效地使用它们。

数据库表包含多个具有特定数据类型的列，如数字或字符串。MySQL提供的数据类型不仅仅是数字或字符串。MySQL中的每种数据类型都可以通过以下特征来确定：

它代表的那种价值。
占用的空间以及值是固定长度还是可变长度。
可以索引或不索引数据类型的值。
MySQL如何比较特定数据类型的值。
```
数据类型	指定值和范围
CHAR	String(0~255)
VARCHAR	String(0~255)
TINYTEXT	String(0~255)
TEXT	String(0~65536)
BLOB	String(0~65536)
MEDIUMTEXT	String(0~16777215)
MEDIUMBLOB	String(0~16777215)
LONGBLOB	String(0~4294967295)
LONGTEXT	String(0~4294967295)
 TINYINT	Integer(-128~127)
SMALLINT	Integer(-32768~32767)
MEDIUMINT	Integer(-8388608~8388607)
INT	Integer(-214847668~214847667)
BIGINT	Integer(-9223372036854775808~9223372036854775807)
FLOAT	decimal(精确到23位小数)
DOUBLE	decimal(24~54位小数)
DECIMAL	将DOUBLE转储为字符串形式
DATE	YYYY-MM-DD
DATETIME	YYYY-MM-DD HH:MM:SS
TIMESTAMP	YYYYMMDDHHMMSS
TIME	HH:MM:SS
ENUM	选项值之一
SET	选项值子集
BOOLEAN	tinyint(1)
```
MySQL数字数据类型
在MySQL中，您可以找到所有SQL标准数字类型，包括精确数字数据类型和近似数字数据类型，包括整数，定点和浮点。此外，MySQL还具有BIT 用于存储位值的数据类型。除BIT类型外，数字类型可以是有符号或无符号的。

下表显示了MySQL中数字类型的摘要：
```
数字类型	描述
 TINYINT	一个非常小的整数
 SMALLINT	一个小整数
 MEDIUMINT	一个中等大小的整数
 INT	标准整数
 BIGINT	一个大整数
 DECIMAL	定点数
 FLOAT	单精度浮点数
 DOUBLE	双精度浮点数
 BIT	一个字节段
```
MySQL布尔数据类型
MySQL没有内置 BOOLEAN或BOOL数据类型。为了表示布尔值，MySQL使用最小的整数类型TINYINT(1)。换句话说，是 BOOLEAN和BOOL的同义词TINYINT(1).

MySQL 字符串数据类型
在MySQL中字符串可以包含从纯文本到二进制数据（如图像或文件）的任何内容。通过使用LIKE运算符，  正则表达式和全文搜索，可以基于模式匹配来比较和搜索字符串。

下表显示了MySQL中的字符串数据类型：
```
字符串类型	描述
 CHAR	固定长度的非二进制（字符）字符串
 VARCHAR	可变长度的非二进制字符串
 BINARY	固定长度的二进制字符串
 VARBINARY	可变长度的二进制字符串
 TINYBLOB	一个非常小的BLOB（二进制大对象）
 BLOB	一个小BLOB
 MEDIUMBLOB	一个中等大小的BLOB
 LONGBLOB	一个大BLOB
 TINYTEXT	一个非常小的非二进制字符串
 TEXT	一个小的非二进制字符串
 MEDIUMTEXT	中等大小的非二进制字符串
 LONGTEXT	一个很大的非二进制字符串
 ENUM	枚举; 可以为每个列值分配一个枚举成员
 SET	集合; 可以为每个列值分配零个或多个SET成员
```
MySQL日期和时间数据类型
MySQL提供日期和时间类型以及日期和时间的组合。此外，MySQL支持  时间戳数据类型，用于跟踪表中一行的更改。如果您只想存储没有日期和月份的年份，则可以使用YEAR数据类型。

下表说明了MySQL日期和时间数据类型：
```
日期和时间类型	描述
 DATE	YYYY-MM-DD格式日期值
 TIME	hh:mm:ss格式时间值
 DATETIME	YYYY-MM-DD hh:mm:ss 格式化日期和时间值
 TIMESTAMP	 YYYY-MM-DD hh:mm:ss时间戳格式化
 YEAR	YYYY 或 YY格式年值
```


MySQL空间数据类型
MySQL支持许多包含各种几何和地理值的空间数据类型，如下表所示：
```
空间数据类型	描述
 GEOMETRY	任何类型的空间值
 POINT	一个点(一对X-Y坐标)
 LINESTRING	曲线(一个或多个POINT值)
 POLYGON	多边形
 GEOMETRYCOLLECTION	GEOMETRY值的集合
 MULTILINESTRING	 LINESTRING值的集合
 MULTIPOINT	POINT值的集合
 MULTIPOLYGON	POLYGON值的集合
```