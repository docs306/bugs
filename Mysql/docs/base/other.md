# 咋乱知识点

select 项列表包括了要取回的列，
可以指定一个字段、
一个表达式或者使用*号。

### SELECT 中只包含一个*号，意味着从所有表中取回所有的列。
而xxx.*，表示取回一个指定表中的所有列。
在SELECT列表中使用非限定的*，可能会产生解析错误。
```sql
SELECT sf.*, shop.title shopName FROM staff sf
LEFT JOIN shop ON (sf.sid = shop.id)
WHERE sf.uid = 1;
```