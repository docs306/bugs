MySQL DROP COLUMN语句简介
在某些情况下，您希望从表中删除一个或多个列。在这种情况下，您使用ALTER TABLE DROP COLUMN语句，如下所示：
```
ALTER TABLE table
DROP COLUMN column; 
```
让我们更详细地研究一下这个语句：

首先，在ALTER TABLE子句后指定包含要删除的列的表。
其次，您将列的名称放在DROP COLUMN子句之后。
请注意，关键字COLUMN是可选的，因此您可以使用较短的语句，如下所示：
```
ALTER TABLE table
DROP column; 
```
若要同时从表中删除多个列，请使用以下语法：

```
ALTER TABLE table
DROP COLUMN column_1,
DROP COLUMN column_2,
```
在从表中删除列之前，您应记住一些重要的要点：

从表中删除列会使依赖于列的所有数据库对象（如存储过程，视图，触发器等）无效。例如，您可能有一个引用列的存储过程。删除列时，存储过程将变为无效。要修复它，您必须手动手动更改存储过程的代码。
必须同时更改依赖于已删除列的其他应用程序的代码，这需要时间和精力。
从大表中删除列可能会影响数据库的性能。
MySQL DROP COLUMN示例
首先，我们创建一个posts 表演示用。

```
CREATE TABLE posts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    excerpt VARCHAR(400),
    content TEXT,
    created_at DATETIME,
    updated_at DATETIME
); 
```
其次，要删除excerpt列，请使用以下ALTER TABLE语句：

```
ALTER TABLE posts
DROP COLUMN excerpt; 
```
第三，要同时删除两个列created_at和updated_at列，请使用以下语句：

```
ALTER TABLE posts
DROP COLUMN created_at,
DROP COLUMN updated_at; 
```
MySQL删除一个带有外键列的示例
如果删除作为外键的列，MySQL将发出错误。让我们展示一下这个想法。

首先，创建一个名为categories的表：
```
CREATE TABLE categories (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
); 
```
其次，将category_id列添加到posts表中。
```
ALTER TABLE posts ADD COLUMN category_id INT NOT NULL; 
```
第三，将category_id列作为引用categories表的id列的外键。
```
ALTER TABLE posts 
ADD CONSTRAINT fk_cat 
FOREIGN KEY (category_id) 
REFERENCES categories(id) ON DELETE CASCADE; 
```
第四，从posts表中删除category_id列。
```
ALTER TABLE posts
DROP COLUMN category_id; 
```
MySQL发出错误消息：
```
ERROR 1553 (HY000): Cannot drop index 'fk_cat': needed in a foreign key constraint
```
要避免此错误，必须在删除列之前删除外键约束。

