MySQL REPLACE 语句插入或更新数据库表中的数据。

MySQL REPLACE语句简介
MySQL REPLACE语句是标准SQL的MySQL扩展。MySQL REPLACE语句的工作原理如下：

如果新行已不存在，则MySQL REPLACE  语句将插入新行。
如果新行已存在，则  REPLACE  语句首先删除旧行，然后插入新行。在某些情况下，REPLACE语句仅更新现有行。
要确定表中是否已存在新行，MySQL使用PRIMARY KEY或UNIQUE KEY 索引。如果表没有这些索引之一，则REPLACE语句等同于INSERT语句。

使用MySQL REPLACE的语句，你需要有至少两个INSERT和DELETE权限。

注意：有一个  REPLACE 字符串函数  ，它不是REPLACE本教程中介绍的语句。

MySQL REPLACE语句示例
让我们看一下使用REPLACE语句更好地理解其工作原理的示例  。

首先，创建一个的新表名为cities如下：

```sql
CREATE TABLE cities (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    population INT NOT NULL
); 
```
接下来，在表cities中插入一些行：

```sql
INSERT INTO cities(name,population)
VALUES('New York',8008278),
   ('Los Angeles',3694825),
   ('San Diego',1223405); 
```
我们  从cities表中查询数据以验证插入操作。

```sql
SELECT 
    *
FROM
    cities; 
```
运行结果：
```
+----+-------------+------------+
| id | name        | population |
+----+-------------+------------+
|  1 | New York    |    8008278 |
|  2 | Los Angeles |    3694825 |
|  3 | San Diego   |    1223405 |
+----+-------------+------------+
3 rows in set (0.00 sec)
```
我们在cities表中有三个城市。

然后，假设我们想要将纽约市的人口更新为1008256.我们可以使用UPDATE语句，如下所示：

```sql
UPDATE cities 
SET 
    population = 1008256
WHERE
    id = 1; 
```
我们再次查询cities表中的数据以验证更新。

```sql
SELECT 
    *
FROM
    cities; 
```
UPDATE按预期更新了数据。
```
+----+-------------+------------+
| id | name        | population |
+----+-------------+------------+
|  1 | New York    |    1008256 |
|  2 | Los Angeles |    3694825 |
|  3 | San Diego   |    1223405 |
+----+-------------+------------+
3 rows in set (0.00 sec)
```
然后，使用REPLACE语句更新洛杉矶城市的人口3696820。

```
REPLACE INTO cities(id,population)
VALUES(2,3696820); 
```
最后，cities再次查询表的数据以验证替换。

```sql
SELECT 
    *
FROM
    cities; 
```
运行结果：
```
+----+-----------+------------+
| id | name      | population |
+----+-----------+------------+
|  1 | New York  |    1008256 |
|  2 | NULL      |    3696820 |
|  3 | San Diego |    1223405 |
+----+-----------+------------+
3 rows in set (0.00 sec)
```
您可能希望name列的值保持不变，但是名称列现在是NULL。REPLACE执行不是这样的。在这种情况下，REPLACE工作原理如下：

REPLACE语句首先使用列列表提供的信息将新行插入cities表中。插入失败，因为cities表中已存在id为2的行，因此，MySQL会引发重复键错误。
然后REPLACE语句将更新具有id列值中指定的键的行。在正常过程中，它将首先删除具有冲突ID的旧行，然后插入新行。
我们知道REPLACE语句没有删除旧行并插入新行，因为id列的值是2而不是4。

MySQL REPLACE和INSERT
REPLACE语句的第一种形式类似于INSERT语句，除了关键字INSERT被关键字替换REPLACE如下：

```
REPLACE INTO table_name(column_list)
VALUES(value_list); 
```
例如，如果要在cities表中插入新行，请使用以下查询：
```
REPLACE INTO cities(name,population)
VALUES('Phoenix',1321523); 
```
注意：未出现在REPLACE语句中的列的默认值将插入到相应的列中。如果列具有NOT NULL属性且没有默认值，并且您未在REPLACE语句中指定值，则MySQL将引发错误。这是REPLACE和INSERT语句之间的区别。

例如，在以下语句中，我们仅指定name列的值，而不是population列。MySQL引发错误消息。因为population列不接受NULL值，所以我们在定义cities表时没有为它指定默认值。
```
REPLACE INTO cities(name)
VALUES('Houston'); 
```
这是MySQL发出的错误消息：
```
Error Code: 1364. Field 'population' doesn't have a default value
MySQL REPLACE和UPDATE
```
第二种形式的  REPLACE 陈述类似于以下UPDATE陈述：
```sql
REPLACE INTO table
SET column1 = value1,
    column2 = value2; 
```
请注意，没有WHERE子句中的REPLACE语句。

例如，如果要更新Phoenix城市的人口1768980，请使用以下REPLACE语句：

```sql
REPLACE INTO cities
SET id = 4,
    name = 'Phoenix',
    population = 1768980; 
```
与UPDATE语句不同，如果未在SET子句中指定列的值，则REPLACE语句将使用列的默认值。

```sql
SELECT 
    *
FROM
    cities; 
```
运行结果：
```
+----+-----------+------------+
| id | name      | population |
+----+-----------+------------+
|  1 | New York  |    1008256 |
|  2 | NULL      |    3696820 |
|  3 | San Diego |    1223405 |
|  4 | Phoenix   |    1768980 |
+----+-----------+------------+
4 rows in set (0.00 sec)
```
MySQL REPLACE INTO和SELECT
第三种形式的   REPLACE 语句类似于INSERT INTO SELECT语句：

```sql
REPLACE INTO table_1(column_list)
SELECT column_list
FROM table_2
WHERE where_condition; 
```
假设您要复制id值为1的城市，请将REPLACE INTO SELECT语句用作以下查询：

```sql
REPLACE INTO cities(name,population)
SELECT name,population FROM cities 
WHERE id = 1; 
```
MySQL REPLACE语句用法
使用REPLACE语句时需要了解几个要点：

如果您开发的应用程序不仅支持MySQL数据库，还支持其他关系数据库管理系统（RDBMS），则应避免使用REPLACE语句，因为其他RDBMS可能不支持它。相反，您可以在事务中使用DELETE  和  INSERT语句的组合  。
如果您使用的是REPLACE在具有TABLE语句触发器和重复键错误发生的删除，触发器将按照以下顺序被解雇：BEFORE INSERT BEFORE DELETE，AFTER DELETE，AFTER INSERT  万一REPLACE语句删除当前行并插入新行。如果REPLACE语句更新当前行，则会触发BEFORE UPDATE和AFTER UPDATE触发器。