# MySQL JOIN 语句简介

关系数据库由使用公共列（称为外键列）链接在一起的多个相关表组成。因此，从业务角度来看，每个表中的数据都是不完整的。

例如，使用列 orderNum 链接的 orders 和 orderdetails 表。

要获得完整订单的数据，需要查询orders和orderdetails两个表的数据。

这就是使用MySQL JOIN的原因。

MySQL连接是一种基于表之间的公共列的值在一个（自连接）或 更多表之间链接数据的方法。

MySQL支持以下类型的连接：
- CROSS JOIN
- INNER JOIN
- LEFT JOIN
- RGIHT JOIN

*join子句用在子句SELECT后面的FROM语句中。*
*注：MySQL不支持完全外连接。*

为了便于理解下面创建测试表t1和t2：

```sql
CREATE TABLE t1 (id INT PRIMARY KEY,mode VARCHAR(50) NOT NULL);
 
CREATE TABLE t2 (id VARCHAR(50) PRIMARY KEY,mode VARCHAR(50) NOT NULL); 
```
两者t1和t2表都有mode列，这也是表之间的公共列。

以下语句将数据插入到表t1和t2表中：

```sql
INSERT INTO t1(id, mode)
VALUES(1,'Divot'), (2,'Brick'), (3,'Grid');
 
INSERT INTO t2(id, mode)
VALUES('A','Brick'), ('B','Grid'), ('C','Diamond'); 
```
而下面的图片说明，从两个数据t1和t2表：
```
-- t1表的数据
+----+---------+
| id | mode    |
+----+---------+
|  1 | Divot   |
|  2 | Brick   |
|  3 | Grid    |
+----+---------+

-- t2表的数据
+----+---------+
| id | mode    |
+----+---------+
| A  | Brick   |
| B  | Grid    |
| C  | Diamond |
+----+---------+
```

### MySQL CROSS JOIN
CROSS JOIN使得从多个表行笛卡尔积。假设使用CROSS JOIN连接t1和t2表，结果集将包括t1表中行与 t2表中行的组合 。

要执行交叉连接，请使用CROSS JOIN以下语句中的子句：

```sql
SELECT t1.id, t2.id
FROM t1
CROSS JOIN t2; 
```
以下显示了查询的结果集：
```
+----+----+
| id | id |
+----+----+
|  1 | A  |
|  2 | A  |
|  3 | A  |
|  1 | B  |
|  2 | B  |
|  3 | B  |
|  1 | C  |
|  2 | C  |
|  3 | C  |
+----+----+
```
如您所见，t1表中的每一行都与t2表中的行组合以形成笛卡尔积。


### MySQL INNER JOIN
要形成一个INNER JOIN，必需要一个连接字段条件。INNER JOIN要求两个连接表中的行具有匹配的值。INNER JOIN返回的记录是通过两表连接字段相同的记录。

连接两个表将 INNER JOIN 第一个表中的每一行与第二个表中的每一行进行比较，以查找满足连接谓词的行对。


使用 INNER JOIN 子句连接t1和t2表：

```sql
SELECT t1.id, t2.id
FROM t1
INNER JOIN t2 ON t1.mode = t2.mode; 
// 此语句中表达式关联条件
t1.mode = t2.mode 
```
这意味着t1和t2表中的行必须在mode列中具有相同的值才能包含在结果中。

以下说明了查询的结果：
```
+----+----+
| id | id |
+----+----+
|  2 | A  |
|  3 | B  |
+----+----+
```


### MySQL LEFT JOIN

LEFT JOIN也需要连接条件。使用LEFT JOIN连接两个表时，会引入左表和右表的概念。

LEFT JOIN返回左表中的所有行，包括满足连接条件行和不满足连接条件的行。对于与条件不匹配的行，右表的列中值将为 NULL。

LEFT JOIN子句连接t1和t2表：
```sql
SELECT t1.id, t2.id
FROM t1
LEFT JOIN t2 ON t1.mode = t2.mode
ORDER BY t1.id; 
```
运行结果：
```
+----+------+
| id | id   |
+----+------+
|  1 | NULL |
|  2 | A    |
|  3 | B    |
+----+------+
```
如您所见，t1表中的所有行都包含在结果集中。对于t1表（左表）中没有表中任何匹配行t2（右表）的行，NULL用于表中的t2列。


### MySQL RIGHT JOIN
RIGHT JOIN 也类似于LEFT JOIN表格的处理相反的情况。使用  RIGHT JOIN，右表（t2）中的每一行都将出现在结果集中。对于右表中没有左表（t1）中匹配行的行，左表( t1) 中的列显示NULL。

以下语句连接t1和t2表使用RIGHT JOIN：
```sql
SELECT t1.id, t2.id
FROM t1
RIGHT JOIN t2 on t1.mode = t2.mode
ORDER BY t2.id; 
```
运行结果：
```
+------+----+
| id   | id |
+------+----+
|    2 | A  |
|    3 | B  |
| NULL | C  |
+------+----+
```
在此结果中，右表（t2）中的所有行都显示在结果集中。对于右表（t2）中左表（t1）中没有匹配行的行，左表(t1) 的列显示NULL 。

