MySQL LIKE运算符根据指定的模式查询数据。

MySQL LIKE 运算符简介
LIKE运算符检测一个字符串是否包含指定的或不包含。以下是LIKE运算符的语法：

```sql
expression LIKE pattern ESCAPE escape_character 
```
LIKE运算符通常用于SELECT，UPDATE和DELETE语句的WHERE子句中。

MySQL提供了两个通配符供 LIKE 使用：百分号%和下划线_。

百分号（%）通配符匹配任何零个或多个字符的字符串。
下划线（_）通配符匹配任何单个字符。
例如：

s%匹配任何字符串以s字符开头，例如sun和six。
se_匹配以 se 开头，后面跟着的任何一个字符，如see和sea。
MySQL LIKE运算符实例
让我们练习一些使用LIKE运算符的例子。我们将使用示例数据库中的下employees表进行演示：

```sql
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.03 sec)
```
A）MySQL LIKE 使用百分比（％）通配符示例
此示例使用LIKE 运算符查找名字以a开头的员工：

```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    firstName LIKE 'a%'; 
```
运行结果：

```
+----------------+----------+-----------+
| employeeNumber | lastName | firstName |
+----------------+----------+-----------+
|           1143 | Bow      | Anthony   |
|           1611 | Fixter   | Andy      |
+----------------+----------+-----------+
2 rows in set (0.01 sec)
```
在此实例中，MySQL扫描整个employees表以查找名字以字符a开头并且后跟任意数量字符的员工。

此示例使用LIKE查找雇员姓氏以on结尾例如：Patterson，Thompson：

```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    lastName LIKE '%on'; 
```
运行结果：

```
+----------------+-----------+-----------+
| employeeNumber | lastName  | firstName |
+----------------+-----------+-----------+
|           1056 | Patterson | Mary      |
|           1088 | Patterson | William   |
|           1166 | Thompson  | Leslie    |
|           1216 | Patterson | Steve     |
+----------------+-----------+-----------+
4 rows in set (0.00 sec)
```
如果我们想搜索到的字符串嵌入在字符串中间，则可以%在模式的开头和结尾使用通配符。

例如，要查找姓氏包含on 的所有员工，请对模式使用%on%以下查询

```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    lastname LIKE '%on%'; 
```
运行结果：

```
+----------------+-----------+-----------+
| employeeNumber | lastName  | firstName |
+----------------+-----------+-----------+
|           1056 | Patterson | Mary      |
|           1088 | Patterson | William   |
|           1102 | Bondur    | Gerard    |
|           1166 | Thompson  | Leslie    |
|           1216 | Patterson | Steve     |
|           1337 | Bondur    | Loui      |
|           1504 | Jones     | Barry     |
+----------------+-----------+-----------+
7 rows in set (0.00 sec)
```
B）MySQL LIKE使用下划线（_）通配符实例
如果想要找到名字以 T开头m结尾，中间只包含一个任意字符。例如Tom，Tim  脚本如下：

```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    firstname LIKE 'T_m'; 
```
运行结果：

```
+----------------+----------+-----------+
| employeeNumber | lastName | firstName |
+----------------+----------+-----------+
|           1619 | King     | Tom       |
+----------------+----------+-----------+
1 row in set (0.00 sec
```
C）MySQL LIKE和NOT运算符实例
MySQL可以使用NOT运算符与LIKE运算符组合以查找与特定模式不匹配的字符串。

假设我们要搜索姓氏不以B字符开头的员工，可以使用NOT LIKE以下查询中显示的模式：

```sql
SELECT 
    employeeNumber, 
    lastName, 
    firstName
FROM
    employees
WHERE
    lastName NOT LIKE 'B%'; 
```
运行结果：

```
+----------------+-----------+-----------+
| employeeNumber | lastName  | firstName |
+----------------+-----------+-----------+
|           1002 | Murphy    | Diane     |
|           1056 | Patterson | Mary      |
|           1076 | Firrelli  | Jeff      |
|           1088 | Patterson | William   |
|           1165 | Jennings  | Leslie    |
|           1166 | Thompson  | Leslie    |
|           1188 | Firrelli  | Julie     |
|           1216 | Patterson | Steve     |
|           1286 | Tseng     | Foon Yue  |
...
```
注意：查询不区分大小写，因此b%和B%模式返回的结果相同。

MySQL LIKE 与ESCAPE子句
有时我们要匹配的模式包含通配符，例如10％，_20等。在这种情况下，您可以使用ESCAPE子句指定转义字符，以便MySQL将通配符解释为文字字符。如果未明确指定转义字符，则反斜杠字符\是缺省转义字符。

例如，如果要查找产品代码包含字符串的产品_20，可以使用%\_20%以下查询中显示的模式：

```sql
SELECT 
    productCode, 
    productName
FROM
    products
WHERE
    productCode LIKE '%\_20%'; 
```
或者我们可以指定不同的转义字符，例如，以下ESCAPE子句使用$：

```sql
SELECT 
    productCode, 
    productName
FROM
    products
WHERE
    productCode LIKE '%$_20%' ESCAPE '$'; 
```
运行结果：

```
+-------------+-------------------------------------------+
| productCode | productName                               |
+-------------+-------------------------------------------+
| S10_2016    | 1996 Moto Guzzi 1100i                     |
| S24_2000    | 1960 BSA Gold Star DBD34                  |
| S24_2011    | 18th century schooner                     |
| S24_2022    | 1938 Cadillac V-16 Presidential Limousine |
| S700_2047   | HMS Bounty                                |
+-------------+-------------------------------------------+
5 rows in set (0.00 sec)
```
在模式中%$_20%匹配包含_20字符串的任何字符串。