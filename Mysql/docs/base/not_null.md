MySQL NOT NULL约束简介
NOT NULL约束是列约束，迫使一列的值非NULL值。

NOT NULL约束的语法如下：

column_name data_type NOT NULL; 
列NOT NULL只包含一个约束，它指定列不得包含任何NULL值的规则。

以下CREATE TABLE语句创建tasks表：
```
CREATE TABLE tasks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE
); 
```
title与start_date列具有NOT NULL约束。id列具有PRIMARY KEY约束，因此，它隐含地包含NOT NULL约束。

end_date列可以包含NULL值。这是因为当我们添加新任务时，我们可能不知道它的结束日期。

NOT NULL除非您有充分的理由不这样做，否则最好在表的每一列中都有约束。

通常，NULL值会使您的查询更加复杂。在这种情况下，您可以使用NOT NULL约束并为列提供默认值。请参阅以下示例：
```
CREATE TABLE inventory (
    material_no VARCHAR(18),
    warehouse_no VARCHAR(10),
    quantity DECIMAL(19 , 2 ) NOT NULL DEFAULT 0,
    base_unit VARCHAR(10) NOT NULL,
    PRIMARY KEY (material_no , warehouse_no)
); 
```
在此示例中，quantity列的默认值为0.因为在我们向inventory表中添加行时，quantity列的值应为0，而不是NULL。

向现有列添加 NOT NULL约束
通常，NOT NULL在创建表时向列添加约束。但是，有时，您希望向现有表的NULL-able列添加NOT NULL约束。在这种情况下，您使用以下步骤：

检查列的当前值。
将NULL值更新为非空值。
添加NOT NULL约束
我们来看一个例子吧。

我们将数据插入到tasks演示表中。
```
INSERT INTO tasks(title ,start_date, end_date)
VALUES('Learn MySQL NOT NULL constraint', '2017-02-01','2017-02-02'),
      ('Check and update NOT NULL constraint to your database', '2017-02-01',NULL); 
```
现在，假设您要强制用户在创建新任务时给出估计的结束日期。为此，您需要将NOT NULL约束添加到tasks表的end_date列。

首先，检查end_date表的值。我们使用IS NULL运算符来检查列中的值是否为NULL：
```
SELECT 
    *
FROM
    tasks
WHERE
    end_date IS NULL; 
+----+-------------------------------------------------------+------------+----------+
| id | title                                                 | start_date | end_date |
+----+-------------------------------------------------------+------------+----------+
|  2 | Check and update NOT NULL constraint to your database | 2017-02-01 | NULL     |
+----+-------------------------------------------------------+------------+----------+
1 row in set (0.00 sec)
```
查询返回一行与end_date值为NULL。

其次，将值更新NULL为非空值。在这种情况下，我们可以创建一个规则，如果end_date是NULL，我们在开始日期后一周生成结束日期。
```
UPDATE tasks 
SET 
    end_date = start_date + 7
WHERE
    end_date IS NULL; 
```
我们来检查一下这个变化：
```
SELECT 
    *
FROM
    tasks; 
+----+-------------------------------------------------------+------------+------------+
| id | title                                                 | start_date | end_date   |
+----+-------------------------------------------------------+------------+------------+
|  1 | Learn MySQL NOT NULL constraint                       | 2017-02-01 | 2017-02-02 |
|  2 | Check and update NOT NULL constraint to your database | 2017-02-01 | 2017-02-08 |
+----+-------------------------------------------------------+------------+------------+
2 rows in set (0.00 sec)
```
第三，将NOT NULL约束添加到end_date列。为此，您使用以下ALTER TABLE语句：
```
ALTER TABLE table_name
CHANGE old_column_name new_column_name new_column_definition; 
```
在我们的示例中，除了具有NOT NULL约束的列定义之外，旧列名和新列名必须相同：
```
ALTER TABLE tasks 
CHANGE end_date end_date DATE NOT NULL; 
```
让我们使用以下DESCRIBE语句验证更改：
```
DESCRIBE tasks; 
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | int(11)      | NO   | PRI | NULL    | auto_increment |
| title      | varchar(255) | NO   |     | NULL    |                |
| start_date | date         | NO   |     | NULL    |                |
| end_date   | date         | NO   |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)
```