MySQL TRUNCATE TABLE语句简介
MySQL TRUNCATE TABLE语句允许您删除表中的所有数据。因此，就功能而言，TRUNCATE TABLE语句类似于没有WHERE子句的DELETE语句。但是，在某些情况下，MySQLTRUNCATE TABLE 语句比DELETE语句更有效。

MySQL TRUNCATE TABLE语句的语法如下：
```
TRUNCATE TABLE table_name; 
```
您可以在TRUNCATE TABLE子句后指定要删除所有数据的表名。

TABLE关键字是可选项。但是，您应使用它来区分  TRUNCATE TABLE语句和  TRUNCATE函数。

如果您使用InnoDB表，MySQL将在删除数据之前检查表中是否存在任何可用的外键约束。以下是案例：

如果表有任何外键约束，则TRUNCATE TABLE语句将逐个删除行。如果外键约束具有DELETE CASCADE操作，则还会删除子表中的相应行。
如果外键约束未指定DELETE CASCADE操作，则TRUNCATE TABLE逐个删除行，并且当遇到子表中的行引用的行时，它将停止并发出错误。
如果表没有任何外键约束，则TRUNCATE TABLE语句将删除表并重新创建一个具有相同结构的新空表，这比使用DELETE语句（尤其是对于大表）更快更有效。
如果您正在使用其他存储引擎，则TRUNCATE TABLE语句将丢弃并重新创建一个新表。

请注意，如果表具有AUTO_INCREMENT列，则TRUNCATE TABLE语句会将自动增量值重置为零。

此外，TRUNCATE TABLE语句不使用DELETE语句，因此，不会调用与表关联的DELETE触发器。

MySQL TRUNCATE TABLE示例
首先，创建一个新表命名为books作为演示：
```
CREATE DATABASE dbdemo;
 
CREATE TABLE books(
 id int auto_increment primary key,
        title varchar(255) not null
)ENGINE=InnoDB; 
```
接下来，使用以下存储过程填充books表的数据：
```
DELIMITER $$
 
CREATE PROCEDURE load_book_data(IN num int(4))
BEGIN
 DECLARE counter int(4) default 0;
 DECLARE book_title varchar(255) default '';
 
 WHILE counter < num DO
   SET book_title = concat('Book title #',counter);
   SET counter = counter + 1;
 
   INSERT INTO books(title)
   Values(book_title);
 END WHILE;
END$$
 
DELIMITER ; 
```
然后，将10,000行加载到books表中。这需要一段时间。
```
CALL load_book_data(10000); 
```
之后，检查books表中的数据：
```
-- 检查前十条
SELECT * FROM books LIMIT 10;

-- 或者
-- 检查总数
SELECT COUNT(*) FROM books; 
运行结果如下：

mysql> SELECT * FROM books LIMIT 10;
+----+---------------+
| id | title         |
+----+---------------+
|  1 | Book title #0 |
|  2 | Book title #1 |
|  3 | Book title #2 |
|  4 | Book title #3 |
|  5 | Book title #4 |
|  6 | Book title #5 |
|  7 | Book title #6 |
|  8 | Book title #7 |
|  9 | Book title #8 |
| 10 | Book title #9 |
+----+---------------+
10 rows in set (0.00 sec)
mysql> SELECT COUNT(*) FROM books;
+----------+
| COUNT(*) |
+----------+
|    10000 |
+----------+
1 row in set (0.00 sec)
```
最后，使用TRUNCATE TABLE语句查看与DELETE语句相比它的执行速度。
```
TRUNCATE TABLE books; 
mysql> CALL load_book_data(100000);
Query OK, 1 row affected (10.26 sec)

mysql> TRUNCATE TABLE books;
Query OK, 0 rows affected (0.01 sec)

mysql> CALL load_book_data(100000);
Query OK, 1 row affected (10.59 sec)

mysql> DELETE FROM books;
Query OK, 100000 rows affected (0.20 sec)
```
在上面的实例中我们看出 TRUNCATE TABLE 删除 十万条记录只用了 0.01秒，而 DELETE 删除同样记录使用了 0.2 秒，数据越大差距会越明显，有兴趣的同学可以测试更新大的数据