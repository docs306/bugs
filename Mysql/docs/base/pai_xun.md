MySQL排序规则简介
MySQL排序规则是一组用于比较特定字符集中的字符的规则。MySQL中的每个字符集都可以有多个排序规则，并且至少有一个默认排序规则。两个字符集不能具有相同的排序规则。

MySQL为您提供了一个SHOW CHARACTER SET  语句，允许您获取字符集的默认排序规则，如下所示：

SHOW CHARACTER SET; 
+----------+---------------------------------+---------------------+--------+
| Charset  | Description                     | Default collation   | Maxlen |
+----------+---------------------------------+---------------------+--------+
| big5     | Big5 Traditional Chinese        | big5_chinese_ci     |      2 |
| dec8     | DEC West European               | dec8_swedish_ci     |      1 |
| cp850    | DOS West European               | cp850_general_ci    |      1 |
| hp8      | HP West European                | hp8_english_ci      |      1 |
| koi8r    | KOI8-R Relcom Russian           | koi8r_general_ci    |      1 |
| latin1   | cp1252 West European            | latin1_swedish_ci   |      1 |
| latin2   | ISO 8859-2 Central European     | latin2_general_ci   |      1 |
| swe7     | 7bit Swedish                    | swe7_swedish_ci     |      1 |
| ascii    | US ASCII                        | ascii_general_ci    |      1 |
| ujis     | EUC-JP Japanese                 | ujis_japanese_ci    |      3 |
| sjis     | Shift-JIS Japanese              | sjis_japanese_ci    |      2 |
| hebrew   | ISO 8859-8 Hebrew               | hebrew_general_ci   |      1 |
| tis620   | TIS620 Thai                     | tis620_thai_ci      |      1 |
| euckr    | EUC-KR Korean                   | euckr_korean_ci     |      2 |
| koi8u    | KOI8-U Ukrainian                | koi8u_general_ci    |      1 |
| gb2312   | GB2312 Simplified Chinese       | gb2312_chinese_ci   |      2 |
| greek    | ISO 8859-7 Greek                | greek_general_ci    |      1 |
| cp1250   | Windows Central European        | cp1250_general_ci   |      1 |
| gbk      | GBK Simplified Chinese          | gbk_chinese_ci      |      2 |
| latin5   | ISO 8859-9 Turkish              | latin5_turkish_ci   |      1 |
| armscii8 | ARMSCII-8 Armenian              | armscii8_general_ci |      1 |
| utf8     | UTF-8 Unicode                   | utf8_general_ci     |      3 |
| ucs2     | UCS-2 Unicode                   | ucs2_general_ci     |      2 |
| cp866    | DOS Russian                     | cp866_general_ci    |      1 |
| keybcs2  | DOS Kamenicky Czech-Slovak      | keybcs2_general_ci  |      1 |
| macce    | Mac Central European            | macce_general_ci    |      1 |
| macroman | Mac West European               | macroman_general_ci |      1 |
| cp852    | DOS Central European            | cp852_general_ci    |      1 |
| latin7   | ISO 8859-13 Baltic              | latin7_general_ci   |      1 |
| utf8mb4  | UTF-8 Unicode                   | utf8mb4_general_ci  |      4 |
| cp1251   | Windows Cyrillic                | cp1251_general_ci   |      1 |
| utf16    | UTF-16 Unicode                  | utf16_general_ci    |      4 |
| utf16le  | UTF-16LE Unicode                | utf16le_general_ci  |      4 |
| cp1256   | Windows Arabic                  | cp1256_general_ci   |      1 |
| cp1257   | Windows Baltic                  | cp1257_general_ci   |      1 |
| utf32    | UTF-32 Unicode                  | utf32_general_ci    |      4 |
| binary   | Binary pseudo charset           | binary              |      1 |
| geostd8  | GEOSTD8 Georgian                | geostd8_general_ci  |      1 |
| cp932    | SJIS for Windows Japanese       | cp932_japanese_ci   |      2 |
| eucjpms  | UJIS for Windows Japanese       | eucjpms_japanese_ci |      3 |
| gb18030  | China National Standard GB18030 | gb18030_chinese_ci  |      4 |
+----------+---------------------------------+---------------------+--------+
41 rows in set (0.00 sec)
默认排序规则列的值指定字符集的默认排序规则。

按照惯例，字符集的排序规则以字符集名称开头，以_ci（不区分大小写）_cs  （区分大小写）或_bin  （二进制）结束。

要获取给定字符集的所有排序规则，请使用以下SHOW COLLATION  语句：

SHOW COLLATION LIKE 'character_set_name%'; 
例如，要获取latin1字符集的所有排序规则，请使用以下语句：

SHOW COLLATION LIKE 'latin1%'; 
+-------------------+---------+----+---------+----------+---------+
| Collation         | Charset | Id | Default | Compiled | Sortlen |
+-------------------+---------+----+---------+----------+---------+
| latin1_german1_ci | latin1  |  5 |         | Yes      |       1 |
| latin1_swedish_ci | latin1  |  8 | Yes     | Yes      |       1 |
| latin1_danish_ci  | latin1  | 15 |         | Yes      |       1 |
| latin1_german2_ci | latin1  | 31 |         | Yes      |       2 |
| latin1_bin        | latin1  | 47 |         | Yes      |       1 |
| latin1_general_ci | latin1  | 48 |         | Yes      |       1 |
| latin1_general_cs | latin1  | 49 |         | Yes      |       1 |
| latin1_spanish_ci | latin1  | 94 |         | Yes      |       1 |
+-------------------+---------+----+---------+----------+---------+
8 rows in set (0.00 sec)
用于latin1字符集的MySQL排序规则
如上所述，每个字符集具有默认排序规则，例如，latin1_swedish_ci是latin1字符集的默认排序规则。

设置字符集和排序规则
MySQL允许您在四个级别指定字符集和排序规则：服务器，数据库，表和列。

在服务器级别设置字符集和排序规则
注意MySQL使用latin1默认字符集，因此，它的默认排序规则是latin1_swedish_ci。您可以在服务器启动时更改这些设置。

如果在服务器启动时仅指定字符集，MySQL将使用字符集的默认排序规则。如果明确指定字符集和排序规则，MySQL将对数据库服务器中创建的所有数据库使用字符集和排序规则。

以下语句通过命令行为服务器设置utf8字符集和utf8_unicode_cs排序规则：

>mysqld --character-set-server=utf8 --collation-server=utf8_unicode_ci 
在数据库级别设置字符集和排序规则
创建数据库时，如果未指定其字符集和排序规则，MySQL将使用数据库的默认字符集和服务器排序规则。

您可以使用CREATE DATABASE或ALTER DATABASE语句覆盖数据库级别的默认设置，如下所示：

CREATE DATABASE database_name
CHARACTER SET character_set_name;
COLLATE collation_name 
ALTER DATABASE database_name
CHARACTER SET character_set_name
COLLATE collation_name; 
MySQL在数据库级别为数据库中创建的所有表使用字符集和排序规则。

在表级别设置字符集和排序规则
数据库可能包含具有字符集和排序规则的表，这些表与默认数据库的字符集和排序规则不同。

在使用CREATE TABLE语句创建表时或使用ALTER TABLE 语句更改表的结构时，可以为表指定缺省字符集和排序规则 。

CREATE TABLE table_name(
   ...
)
CHARACTER SET character_set_name
COLLATE collation_name 
ALTER TABLE table_name(
  ...
)
CHARACTER SET character_set_name
COLLATE collation_name 
在列级别设置字符集和排序规则
类型的列CHAR，VARCHAR或者TEXT可以具有自己的字符集和排序规则，排序规则与表的默认字符集和排序规则不同。

您可以在列的任一CREATE TABLE或ALTER TABLE  语句的定义中为列指定字符集和排序规则，如下所示：

column_name [CHAR | VARCHAR | TEXT] (length)
CHARACTER SET character_set_name
COLLATE collation_name 
这些是设置字符集和排序规则的规则：

如果明确指定字符集和排序规则，则使用字符集和排序规则。
如果指定字符集并省略排序规则，则使用字符集的默认排序规则。
如果指定不带字符集的排序规则，则使用与排序规则关联的字符集。
如果省略字符集和排序规则，则使用默认字符集和排序规则。
让我们看一下设置字符集和排序规则的一些示例。

设置字符集和排序规则的示例
首先，我们使用utf8作为字符集和utf8_unicode_ci默认排序规则创建一个新数据库：

CREATE DATABASE mydbdemo
CHARACTER SET utf8
COLLATE utf8_unicode_ci; 
因为我们明确指定了mydbdemo数据库的字符集和排序规则，所以mydbdemo不会在服务器级别采用默认字符集和排序规则。

其次，我们创建一个t1在mydbdemo数据库中命名的新表：

USE mydbdemo; 
CREATE TABLE t1(
c1 char(25)
); 
我们没有为t1表格指定字符集和排序规则; MySQL将检查数据库级别以确定表的字符集和排序规则t1  。在这种情况下，t1  表具有utf8默认字符集和utf8_unicode_ci默认排序规则。

第三，对于t1  表，我们将其字符集更改latin1为其排序规则latin1_german1_ci：

ALTER TABLE t1
CHARACTER SET latin1
COLLATE latin1_german1_ci; 
表中的c1列t1具有latin1字符集和latin1_german1_ci排序规则。

第四，让我们将c1  列的字符集更改为latin1：

ALTER TABLE t2
MODIFY c1 VARCHAR(25)
CHARACTER SET latin1; 
现在，c1  列具有  latin1  字符集，但它的排序规则呢？它是否继承了latin1_german1_ci表的整理的排序规则？不，因为latin1字符集的默认排序规则是latin1_swedish_ci，c1列具有  latin1_swedish_ci排序规则。