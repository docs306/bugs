导出 CSV
简介：在本教程中，您将学习如何将MySQL表导出为CSV文件的各种技术。

CSV代表逗号分隔值。您经常使用CSV文件格式在Microsoft Excel，Open Office，Google Docs等应用程序之间交换数据。

以CSV文件格式从MySQL数据库获取数据将非常有用，因为您可以按照自己的方式分析和格式化数据。

MySQL提供了一种将查询结果导出到驻留在数据库服务器中的CSV文件的简便方法。

在导出数据之前，您必须确保：

MySQL服务器的进程具有对包含目标CSV文件的目标文件夹的写访问权。
目标CSV文件必须不存在。
以下查询从orders表中选择已取消的订单  ：

SELECT 
    orderNumber, status, orderDate, requiredDate, comments
FROM
    orders
WHERE
    status = 'Cancelled'; 
要将此结果集导出为CSV文件，请在上面的查询中添加一些子句，如下所示：

SELECT 
    orderNumber, status, orderDate, requiredDate, comments
FROM
    orders
WHERE
    status = 'Cancelled' 
INTO OUTFILE 'C:/tmp/cancelled_orders.csv' 
FIELDS ENCLOSED BY '"' 
TERMINATED BY ';' 
ESCAPED BY '"' 
LINES TERMINATED BY '\r\n'; 
语句创建了一个CSV文件cancelled_orders.csv，文件在包含结果集的 C:\tmp文件夹中命名  。

CSV文件包含结果集中的行数。每一行都由一系列回车符和LINES TERMINATED BY '\r\n'子句指定的换行符终止。每行包含结果集中行的每列的值。

每个值都用FIELDS ENCLOSED BY '”'子句指示的双引号括起来  。这可以防止可能包含逗号（，）的值被解释为字段分隔符。使用双引号括起值时，值内的逗号不会被识别为字段分隔符。

将数据导出到文件名为timestamp的CSV文件
您经常需要将数据导出到CSV文件中，文件的名称包含创建文件的时间戳。为此，您需要使用MySQL预处理语句。

以下命令将整个订单表导出为CSV文件，其中时间戳作为文件名的一部分。

SET @TS = DATE_FORMAT(NOW(),'_%Y_%m_%d_%H_%i_%s');
 
SET @FOLDER = 'c:/tmp/';
SET @PREFIX = 'orders';
SET @EXT    = '.csv';
 
SET @CMD = CONCAT("SELECT * FROM orders INTO OUTFILE '",@FOLDER,@PREFIX,@TS,@EXT,
    "' FIELDS ENCLOSED BY '\"' TERMINATED BY ';' ESCAPED BY '\"'",
    "  LINES TERMINATED BY '\r\n';");
 
PREPARE statement FROM @CMD;
 
EXECUTE statement; 
让我们更详细地研究上面的命令。

首先，我们构造了一个查询，其中当前时间戳作为文件名的一部分。
其次，我们使用PREPARE语句 准备了执行语句。
第三，我们使用EXECUTE命令执行了语句。
您可以通过事件包装命令，并在需要时定期运行事件。

使用列标题导出数据
如果CSV文件包含第一行作为列标题将是方便的，这样文件更容易理解。

要添加列标题，您需要使用UNION语句，如下所示：

(SELECT 'Order Number','Order Date','Status')
UNION 
(SELECT orderNumber,orderDate, status
FROM orders
INTO OUTFILE 'C:/tmp/orders.csv'
FIELDS ENCLOSED BY '"' TERMINATED BY ';' ESCAPED BY '"'
LINES TERMINATED BY '\r\n'); 
如查询所示，您需要包含每列的列标题。

处理NULL值
如果结果集中的值包含NULL值，则目标文件将包含  "N而不是NULL。要解决此问题，您需要使用IFNULL函数将NULL值替换为另一个值，例如，不适用（N/A），如下面的查询：

SELECT 
    orderNumber, orderDate, IFNULL(shippedDate, 'N/A')
FROM
    orders INTO OUTFILE 'C:/tmp/orders2.csv' 
    FIELDS ENCLOSED BY '"' 
    TERMINATED BY ';' 
    ESCAPED BY '"' LINES 
    TERMINATED BY '\r\n'; 
我们用N/A字符串替换shippedDate列中的 NULL值。CSV文件显示 N/A 而不是NULL值。