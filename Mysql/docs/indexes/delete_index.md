MySQL 索引删除

MySQL DROP INDEX语法
要从表中删除现有索引，请使用以下DROP INDEX语句：
```
DROP INDEX index_name ON table_name
[algorithm_option | lock_option]; 
```
在这个语法中：

首先，指定要在DROP INDEX关键字后删除的索引的名称。
其次，指定索引所属的表的名称。
算法
algorithm_option允许您指定用于索引中删除特定的算法。以下显示了algorithm_option子句的语法：
```
ALGORITHM [=] {DEFAULT|INPLACE|COPY} 
```
对于索引删除，支持以下算法：

- COPY：表逐行复制到新表，DROP INDEX然后在原始表的副本上执行。并发数据操作语句例如INSERT和UPDATE不允许。
- INPLACE：表被重建到位而不是复制到新表。MySQL在索引删除操作的准备和执行阶段期间在表上发出独占元数据锁。算法允许并发数据操作语句。
- 请注意，ALGORITHM子句是可选项。如果你跳过它，MySQL会使用INPLACE。如果INPLACE不支持，MySQL使用COPY。

使用DEFAULT与省略ALGORITHM子句具有相同的效果。

锁
lock_option控制并发的电平读出并同时索引被移除的表写操作。

以下显示了以下语法lock_option：
```
LOCK [=] {DEFAULT|NONE|SHARED|EXCLUSIVE} 
```
支持以下锁定模式：

- DEFAULT：这允许您具有给定算法的最大并发级别。首先，如果支持，它允许并发读取和写入。如果不支持，如果支持则允许并发读取。如果不是，请强制执行独占访问。
- NONE：如果NONE支持，则可以进行并发读写。否则，MySQL会发出错误。
- SHARED：如果SHARED支持，则可以进行并发读取，但不能写入。如果不支持并发读取，MySQL会发出错误。
- EXCLUSIVE：这会强制执行独占访问。

MySQL DROP INDEX示例
```
CREATE TABLE leads(
    lead_id INT AUTO_INCREMENT,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL,
    information_source VARCHAR(255),
    INDEX name(first_name,last_name),
    UNIQUE email(email),
    PRIMARY KEY(lead_id)
); 
```
以下语句从leads表中删除name索引：

DROP INDEX name ON leads; 
以下语句使用特定算法并锁定leads表中的email索引：
```
DROP INDEX email ON leads
ALGORITHM = INPLACE 
LOCK = DEFAULT; 
```
MySQL DROP PRIMARY KEY索引
要删除PRIMARY主键的索引，请使用以下语句：
```
DROP INDEX `PRIMARY` ON table_name; 
```
以下语句创建一个以t主键命名的新表：
```
CREATE TABLE t(
    pk INT PRIMARY KEY,
    c VARCHAR(10)
); 
```
要删除主键索引，请使用以下语句：
```
DROP INDEX `PRIMARY` ON t; 
```