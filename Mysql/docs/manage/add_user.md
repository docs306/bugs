MySQL 用户创建

MySQL 中的用户帐户简介
在MySQL中，您不仅可以指定谁可以连接到数据库服务器，还可以指定用户连接的主机。因此，MySQL中的用户帐户包含用户名和由@字符分隔的主机名。

例如，如果admin 用户连接到MySQL数据库服务器localhost，则用户帐户为 admin@localhost

admin用户只能从连接到MySQL数据库服务器localhost，而不是从远程主机如begtut.com。这使得MySQL数据库服务器更加安全。

此外，通过组合用户名和主机，可以设置多个具有相同名称的帐户，但可以使用不同的权限从不同的主机进行连接。

MySQL将用户帐户存储在mysql数据库的user授权表中。

使用MySQL CREATE USER语句创建用户帐户
MySQL提供了CREATE USER  允许您创建新用户帐户的语句。CREATE USER语句的语法如下：
```
CREATE USER user_account IDENTIFIED BY password; 
```
user_account格式'username'@'hostname'  之后的CREATE USER语句。

密码在IDENTIFIED BY子句中指定。密码必须是明文。在将用户帐户保存到user表中之前，MySQL将加密密码。

例如，要创建一个使用 密码dbadmin连接到MySQL数据库服务器  的新用户，请使用以下  语句：localhostsecretCREATE USER
```
CREATE USER dbadmin@localhost 
IDENTIFIED BY 'secret'; 
```
要查看用户帐户的权限，请使用以下SHOW GRANTS语句：
```
SHOW GRANTS FOR dbadmin@localhost; 
+---------------------------------------------+
| Grants for dbadmin@localhost                |
+---------------------------------------------+
| GRANT USAGE ON *.* TO `dbadmin`@`localhost` |
+---------------------------------------------+
1 row in set (0.08 sec)
```
结果中的*.*表示用户帐户dbadmin只能登录数据库服务器，没有其他权限。要向用户授予权限，请使用GRANT语句，语句将在下一个教程中介绍。

请注意，点（.）前面的部分代表数据库，点（.）后面的部分代表表格，例如， database.table

要允许用户帐户从任何主机进行连接，请使用百分比（%) 通配符，如以下示例所示：
```
CREATE USER superadmin@'%'
IDENTIFIED BY 'secret'; 
```
百分比通配符% 与在LIKE运算符中使用的效果相同，例如，要允许mysqladmin用户帐户从begtut.com 主机的任何子域连接到数据库服务器  ，请使用百分比通配符%  ，如下所示：
```
CREATE USER mysqladmin@'%.begtut.com'
IDENTIFIED by 'secret'; 
```
请注意，您还可以在CREATE USER 语句中使用下划线通配符_ 。

如果省略hostname 用户帐户的一部分，MySQL将接受它并允许用户从任何主机连接。例如，以下语句创建一个名为remote 可以从任何主机连接到数据库服务器的新用户帐户：
```
CREATE USER remote; 
```
您可以看到授予remote用户帐户的权限，如下所示：
```
SHOW GRANTS FOR remote; 
+------------------------------------+
| Grants for remote@%                |
+------------------------------------+
| GRANT USAGE ON *.* TO `remote`@`%` |
+------------------------------------+
1 row in set (0.07 sec)
```
重要的是要注意引用非常重要，尤其是当用户帐户包含特殊字符（如_  或%）时。

如果您不小心引用了用户帐户'username@hostname'，MySQL会创建一个用户  username@hostname name 并允许用户从任何主机连接，这可能不是您所期望的。

例如，以下语句创建一个api@localhost可以从任何主机连接到MySQL数据库服务器的新用户。
```
CREATE USER 'api@localhost'; 
SHOW GRANTS FOR 'api@localhost'; 
+-------------------------------------------+
| Grants for api@localhost@%                |
+-------------------------------------------+
| GRANT USAGE ON *.* TO `api@localhost`@`%` |
+-------------------------------------------+
1 row in set (0.01 sec)
```
如果您创建一个已存在的用户，MySQL将发出错误。例如，以下语句创建一个remote已存在的名为的用户帐户：
```
CREATE USER remote; 
```
MySQL发出以下错误消息：
```
ERROR 1396 (HY000): Operation CREATE USER failed for 'remote'@'%'
```
注意：CREATE USER  语句只是一个没有权限的新用户帐户。如果要向用户授予权限，请使用GRANT 语句。