MySQL 权限授予
简介：在本教程中，您将学习如何使用MySQL GRANT语句向MySQL用户授予权限。

MySQL GRANT语句简介
之后创建一个新的用户帐户，用户没有任何特权。要授予用户帐户权限，请使用GRANT语句。

以下说明了GRANT语句的语法：
```
GRANT privilege,[privilege],.. ON privilege_level 
TO user [IDENTIFIED BY password]
[REQUIRE tsl_option]
[WITH [GRANT_OPTION | resource_option]];
``` 
让我们GRANT更详细地研究一下这个陈述。

- 首先，在GRANT关键字后指定一个或多个权限。如果授予用户多个权限，则每个权限由逗号分隔。（请参阅下表中的权限列表）。
- 接下来，指定  privilege_level 确定权限应用级别的。MySQL支持global（*.*），database（database.*），table（database.table）和列级别。如果使用列权限级别，则必须在每个权限之后指定一个或逗号分隔列的列表。
- 然后，放置user 您要授予的权限。如果用户已存在，则GRANT语句将修改其权限。否则，GRANT语句将创建一个新用户。可选子句IDENTIFIED BY允许您为用户设置新的密码。
- 之后，指定用户是否必须通过SSL，X059等安全连接连接到数据库服务器。
- 最后，可选 WITH GRANT OPTION 子句允许您授予其他用户或从其他用户中删除您拥有的权限。此外，您可以使用WITH子句分配MySQL数据库服务器的资源，例如，设置用户每小时可以使用的连接数或语句数。这在MySQL共享托管等共享环境中非常有用。

请注意，要使用GRANT语句，您必须具有GRANT OPTION您授予的权限和权限。如果read_only 启用了系统变量，则需要具有SUPER执行GRANT语句的权限。

让我们练习一些使用MySQL GRANT语句的例子来更好地理解。

MySQL GRANT示例
通常，我们首先使用CREATE USER语句创建新用户帐户，然后使用GRANT语句向用户授予权限。

例如，以下CREATE USER语句创建一个新的超级用户帐户。
```
CREATE USER super@localhost IDENTIFIED BY 'dolphin'; 
```
要显示分配给super @ localhost用户的特权，请使用SHOW GRANTSstatement。
```
SHOW GRANTS FOR super@localhost; 
+-------------------------------------------+
| Grants for super@localhost                |
+-------------------------------------------+
| GRANT USAGE ON *.* TO `super`@`localhost` |
+-------------------------------------------+
1 row in set (0.03 sec)
```
要授予super @ localhost用户帐户的所有权限，请使用以下语句。
```
GRANT ALL ON *.* TO 'super'@'localhost' WITH GRANT OPTION; 
```
ON *.*子句表示数据库中的所有数据库和所有对象。在WITH GRANT OPTION允许super@localhost授予权限给其他用户。

现在，如果SHOW GRANTS再次使用语句，您将看到super @ localhost的权限已更新。
```
SHOW GRANTS FOR super@localhost; 
+----------------------------------------------------------------------+
| Grants for super@localhost                                           |
+----------------------------------------------------------------------+
| GRANT ALL PRIVILEGES ON *.* TO `super`@`localhost` WITH GRANT OPTION |
+----------------------------------------------------------------------+
1 row in set (0.00 sec) 
```
要创建在mysqldemo 示例数据库中具有所有权限的用户，请使用以下语句：
```
CREATE USER auditor@localhost IDENTIFIED BY 'whale';
GRANT ALL ON mysqldemo.* TO auditor@localhost; 
```
您可以在单个GRANT 语句中授予多个权限。例如，您可以 使用以下语句创建一个可以对mysqldemo数据库执行SELECT，INSERT和UPDATE语句的用户 ：
```
CREATE USER rfc IDENTIFIED BY 'shark';
 
GRANT SELECT, UPDATE, DELETE ON mysqldemo.* TO rfc; 
```
现在，如果您使用rfc用户帐户登录MySQL服务器并发出以下语句：
```
CREATE TABLE city(
   id INT PRIMARY KEY AUTO_INCREMENT, 
   name VARCHAR(255)
); 
```
MySQL发出以下错误消息：
```
ERROR 1142 (42000): CREATE command denied to user 'rfc'@'localhost' for table 'city'
```
GRANT的允许权限
下表说明了可用于GRANT和REVOKE语句的所有允许权限：
```
特权	说明	级别
全局	数据库	表	列	程序	代理
ALL [PRIVILEGES]	授予除GRANT OPTION之外的指定访问级别的所有权限						
ALTER	允许用户使用ALTER TABLE语句	X	X	X			
ALTER ROUTINE	允许用户更改或删除存储的例程	X	X			X	
CREATE	允许用户创建数据库和表	X	X	X			
CREATE ROUTINE	允许用户创建存储的例程	X	X				
CREATE TABLESPACE	允许用户创建，更改或删除表空间和日志文件组	X					
CREATE TEMPORARY TABLES	允许用户使用CREATE TEMPORARY TABLE创建临时表	X	X				
CREATE USER	允许用户使用CREATE USER，DROP USER，RENAME USER和REVOKE ALL PRIVILEGES语句。	X					
CREATE VIEW	允许用户创建或修改视图。	X	X	X			
DELETE	允许用户使用DELETE	X	X	X			
DROP	允许用户删除数据库，表和视图	X	X	X			
EVENT	启用事件计划程序的事件使用。	X	X				
EXECUTE	允许用户执行存储的例程	X	X	X			
FILE	允许用户读取数据库目录中的任何文件。	X					
GRANT OPTION	允许用户拥有授予或撤消其他帐户权限的权限。	X	X	X		X	X
INDEX	允许用户创建或删除索引。	X	X	X			
INSERT	允许用户使用INSERT语句	X	X	X	X		
LOCK TABLES	允许用户对具有SELECT权限的表使用LOCK TABLES	X	X				
PROCESS	允许用户使用SHOW PROCESSLIST语句查看所有进程。	X					
PROXY	启用用户代理。						
REFERENCES	允许用户创建外键	X	X	X	X		
RELOAD	允许用户使用FLUSH操作	X					
REPLICATION CLIENT	允许用户查询以查看主服务器或从属服务器的位置	X					
REPLICATION SLAVE	允许用户使用复制从属从主服务器读取二进制日志事件。	X					
SELECT	允许用户使用SELECT语句	X	X	X	X		
SHOW DATABASES	允许用户显示所有数据库	X					
SHOW VIEW	允许用户使用SHOW CREATE VIEW语句	X	X	X			
SHUTDOWN	允许用户使用mysqladmin shutdown命令	X					
SUPER	允许用户使用其他管理操作，例如CHANGE MASTER TO，KILL，PURGE BINARY LOGS，SET GLOBAL和mysqladmin命令	X					
TRIGGER	允许用户使用TRIGGER操作。	X	X	X			
UPDATE	允许用户使用UPDATE语句	X	X	X	X		
USAGE	相当于“没有特权”
```