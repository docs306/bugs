MySQL 用户密码
摘要：在本教程中，您将学习如何使用各种语句，如  UPDATE，SET PASSWORD,和ALTER USER 语句修改MySQL用户密码。

在更改MySQL用户  帐户的密码之前  ，您应考虑以下重要问题：

您想要更改密码的用户帐户是什么？
什么应用程序正在使用其密码被更改的用户帐户？如果在不更改使用用户帐户的应用程序的连接字符串的情况下更改密码，则应用程序将无法连接到数据库服务器。
在回答完这些问题之后。您可以开始更改用户帐户的密码。

MySQL提供，您可以使用更改，包括用户的密码，各种报表  UPDATE，SET PASSWORD, 和GRANT USAGE 语句。

使用UPDATE语句更改MySQL用户密码
更改密码的第一种方法是使用UPDATE 语句来更新mysql数据库user表。

执行UPDATE语句后，还需要执行FLUSH PRIVILEGES 语句以从mysql数据库中的grant表重新加载权限。

假设你想为dbadmin更改密码，则需要执行以下语句：
```
USE mysql;
 
UPDATE user 
SET password = PASSWORD('dolphin')
WHERE user = 'dbadmin' AND 
      host = 'localhost';
 
FLUSH PRIVILEGES; 
```
请注意，从MySQL 5.7.6开始，用户表authentication_string仅使用列来存储密码。此外，它删除了password列。
因此，如果您使用MySQL 5.7.6+，则必须使用UPDATE语句中的authentication_string列：
```
USE mysql;
 
UPDATE user 
SET authentication_string = PASSWORD('dolphin')
WHERE user = 'dbadmin' AND 
      host = 'localhost';
 
FLUSH PRIVILEGES; 
```
注意：PASSWORD()函数计算纯文本的哈希值，同时在MySQL 8.0.11版本中PASSWORD被移除。

使用SET PASSWORD语句更改MySQL用户密码
更改密码的第二种方法是使用SET PASSWORD  语句。

您使用user@host 格式的用户帐户来更新密码。如果您需要更改其他帐户的密码，您的帐户至少需要拥有UPDATE权限。

通过使用SET PASSOWORD语句，您无需执行FLUSH PRIVILEGES 语句即可从授权表重新加载权限。

以下语句dbadmin 使用SET PASSWORD  语句更改用户帐户的密码   。

SET PASSWORD FOR 'dbadmin'@'localhost' = PASSWORD('bigshark'); 
请注意，从版本5.7.6开始，MySQL对此语法进行了折旧，并可能在将来的版本中将其删除。相反，它使用明文密码如下：

SET PASSWORD FOR 'dbadmin'@'localhost' = 'bigshark'; 
使用ALTER USER语句更改MySQL用户密码
更改用户帐户密码的第三种方法是使用ALTER USER 带有IDENTIFIED BY  子句的语句  。

以下ALTER USER 语句将dbadmin 用户的密码更改  为littlewhale：
```
ALTER USER dbadmin@localhost IDENTIFIED BY 'littlewhale'; 
```
如果要重置MySQL root 帐户的密码，则需要强制MySQL数据库服务器停止并重新启动，而不使用授权表验证。