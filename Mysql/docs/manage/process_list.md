MySQL 进程列表
简介：在本教程中，您将学习如何使用SHOW PROCESSLIST命令查找当前正在运行的线程。

有时，您可能会收到MySQL服务器返回的“太多连接”错误。要找出原因，可以使用SHOW PROCESSLIST命令。

SHOW PROCESSLIST命令返回所有当前运行的线程。然后，您可以使用KILL语句终止空闲线程。

以下显示了SHOW PROCESSLIST命令的语法：

SHOW [FULL] PROCESSLIST; 
具有PROCESS 权限的帐户 可以查看所有线程。否则，他们只能查看与其帐户关联的线程。

以下显示了SHOW PROCESSLIST命令输出的示例：
```
mysql> SHOW PROCESSLIST;
+------+-----------------+-----------+-------+---------+------+-----------------------------+------------------+
| Id   | User            | Host      | db    | Command | Time | State                       | Info             |
+------+-----------------+-----------+-------+---------+------+-----------------------------+------------------+
| 2040 | root            | localhost | mysql | Query   |    0 | init                        | SHOW PROCESSLIST |
| 2049 | event_scheduler | localhost | NULL  | Daemon  |   46 | Waiting for next activation | NULL             |
+------+-----------------+-----------+-------+---------+------+-----------------------------+------------------+
2 rows in set (0.00 sec)
```
SHOW PROCESSLIST命令的输出包含以下列：

ID
客户端进程的ID

User
与线程关联的用户名。

Host
客户端连接的主机

DB
如果另外选择了一个默认数据库 NULL

Command
命令类型

Time
当前线程处于当前状态的秒数。

State
线程状态，表示指示正在执行的线程的操作，事件或状态。

Info
语句正在执行，或者NULL如果它没有执行任何语句。如果FULL在SHOW PROCESSLIST命令中不使用关键字，则只在Info列中返回每个语句的前100个字符。