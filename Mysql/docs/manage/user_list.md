MySQL 用户列表


MySQL show users：列出所有用户
你在寻找MySQL SHOW USERS命令吗？不幸的是，MySQL没有SHOW USERS像SHOW DATABASES，SHOW TABLES等等的命令，因此要列出MySQL数据库服务器中的所有用户，请使用以下查询：
```
SELECT 
   user 
FROM 
   mysql.user; 
```
在此语句中，我们从数据库user表中查询用户数据mysql。

要执行此查询，您必须以管理员身份登录MySQL数据库服务器。
```
>mysql -u root -p
Enter password: ***********
mysql> use mysql;
Database changed
mysql> SELECT user FROM user; 
```
以下显示了上述查询的输出：
```
+------+
| user |
+------+
| root |
| root |
| root |
|      |
| root |
|      |
+------+
6 rows in set (0.00 sec)
```
如您所见，我们的本地数据库中有6个用户。

要获取有关user表的更多信息，可以使用以下命令预览其列：

DESC user; 
例如，要显示用户和其他信息（如主机，帐户锁定和密码过期状态），请使用以下查询：
```
SELECT 
    user, 
    host, 
    account_locked, 
    password_expired
FROM
    user; 
```
这是查询的输出。

显示当前用户
要获取有关当前用户的信息，请使用user()以下语句中所示的函数：
```
mysql> SELECT user();
+----------------+
| user()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)
```
或者您使用current_user()功能：
```
mysql> SELECT current_user();
+----------------+
| current_user() |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)
```
在这种情况下，当前用户是root@localhost。

显示当前记录的用户
要列出当前登录MySQL数据库服务器的所有用户，请执行以下语句：
```
SELECT 
    user, 
    host, 
    db, 
    command 
FROM 
    information_schema.processlist; 
+-----------------+-----------+-------+---------+
| user            | host      | db    | command |
+-----------------+-----------+-------+---------+
| root            | localhost | mysql | Query   |
| event_scheduler | localhost | NULL  | Daemon  |
+-----------------+-----------+-------+---------+
2 rows in set (0.01 sec)
```