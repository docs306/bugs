MySQL 表列表

要列出MySQL数据库中的表，请按照下列步骤操作：

使用MySQL等客户端登录MySQL数据库服务器
使用USE语句切换到特定数据库。
使用SHOW TABLES命令。
以下说明了MySQL SHOW TABLES命令的语法  ：

SHOW TABLES; 
MySQL SHOW TABLES示例
以下示例说明如何在classicmodels数据库中列出表。

步骤1.连接到MySQL数据库服务器：
```
>mysql -u root -p
Enter password: **********
mysql> 
```
步骤2.切换到classicmodels数据库：
```
mysql> use mysqldemo;
Database changed
mysql> 
```
步骤3.在mysqldemo数据库中显示表：
```
mysql> show tables;
+---------------------+
| Tables_in_mysqldemo |
+---------------------+
| buildings           |
| cities              |
| customers           |
| customers_archive   |
| departments         |
| employees           |
| items               |
| messages            |
| offices             |
| orderdetails        |
| orders              |
| payments            |
| productlines        |
| products            |
| projects            |
| rooms               |
| sales               |
| subscribers         |
| suppliers           |
| t1                  |
| t2                  |
| tasks               |
| tbl                 |
| tokens              |
+---------------------+
24 rows in set (0.01 sec)
```
SHOW TABLES命令允许您显示表是基表还是视图。要在结果中包含表类型，请使用以下形式的SHOW TABLES语句。

SHOW FULL TABLES; 
让我们在classicmodels数据库中创建一个名为的视图contacts，其中包括演示的表格employees和customers表格中的名字，姓氏和电话。
```
CREATE VIEW contacts 
AS 
SELECT lastName, firstName, extension as phone 
FROM employees 
UNION
SELECT contactFirstName, contactLastName, phone 
FROM customers; 
```
现在，您发出SHOW FULL TABLES命令：
```
mysql> SHOW FULL TABLES;
+---------------------+------------+
| Tables_in_mysqldemo | Table_type |
+---------------------+------------+
| buildings           | BASE TABLE |
| cities              | BASE TABLE |
| contacts            | VIEW       |
| customers           | BASE TABLE |
| customers_archive   | BASE TABLE |
| departments         | BASE TABLE |
| employees           | BASE TABLE |
| items               | BASE TABLE |
| messages            | BASE TABLE |
| offices             | BASE TABLE |
| orderdetails        | BASE TABLE |
| orders              | BASE TABLE |
| payments            | BASE TABLE |
| productlines        | BASE TABLE |
| products            | BASE TABLE |
| projects            | BASE TABLE |
| rooms               | BASE TABLE |
| sales               | BASE TABLE |
| subscribers         | BASE TABLE |
| suppliers           | BASE TABLE |
| t1                  | BASE TABLE |
| t2                  | BASE TABLE |
| tasks               | BASE TABLE |
| tbl                 | BASE TABLE |
| tokens              | BASE TABLE |
+---------------------+------------+
25 rows in set (0.00 sec)
```
如您所见，除了作为contacts视图的表之外，所有表都是基表。

对于具有许多表的数据库，一次显示所有表可能不直观。

幸运的是，SHOW TABLES命令为您提供了一个选项，允许您使用LIKE运算符或WHERE子句中的表达式过滤返回的表，如下所示：
```
SHOW TABLES LIKE pattern;
 
SHOW TABLES WHERE expression; 
```
例如，要显示classicmodels数据库中以字母开头的所有表p，请使用以下语句：
```
mysql> SHOW TABLES LIKE 'p%';
+--------------------------+
| Tables_in_mysqldemo (p%) |
+--------------------------+
| payments                 |
| productlines             |
| products                 |
| projects                 |
+--------------------------+
4 rows in set (0.00 sec)
```
或者要显示以字符串结尾的表'es'，请使用以下语句：
```
mysql> SHOW TABLES LIKE '%es';
+---------------------------+
| Tables_in_mysqldemo (%es) |
+---------------------------+
| cities                    |
| employees                 |
| messages                  |
| offices                   |
| productlines              |
| sales                     |
+---------------------------+
6 rows in set (0.00 sec)
```
以下语句说明如何在SHOW TABLES语句中使用WHERE子句列出mysqldemo数据库中的所有视图。
```
+---------------------+------------+
| Tables_in_mysqldemo | Table_type |
+---------------------+------------+
| contacts            | VIEW       |
+---------------------+------------+
1 row in set (0.01 sec)
```
有时，您希望查看数据库中未连接的表。在这种情况下，您可以使用SHOW TABLES语句的FROM子句指定要从中显示表的数据库。

以下示例演示如何显示以'time'; 开头的表;
```
+---------------------------+
| Tables_in_mysql (time%)   |
+---------------------------+
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
+---------------------------+
5 rows in set (0.01 sec)
```
以下语句等同于上面的语句，但它使用IN而不是FROM。

SHOW TABLES IN mysql LIKE 'time%'; 
请务必注意，如果您没有基表或视图的权限，则它不会显示在SHOW TABLES命令的结果集中。