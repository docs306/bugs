MySQL 变量
简介：在本教程中，您将学习如何在SQL语句中使用MySQL用户定义的变量。

MySQL用户定义变量简介
有时，您希望将值从SQL语句传递到另一个SQL语句。为此，您将值存储在第一个语句中的MySQL用户定义变量中，并在后续语句中引用它。

要创建用户定义的变量，请使用格式@variable_name，其中variable_name包含字母数字字符。从MySQL 5.7.5开始，用户定义变量的最大长度为64个字符

用户定义的变量不区分大小写。这意味着@id和@ID是一样的。

您可以将用户定义的变量分配给某些数据类型，例如整数，浮点，十进制，字符串或NULL。

由一个客户端定义的用户定义变量其他客户端不可见。换句话说，用户定义的变量是特定于会话的。

请注意，用户定义的变量是SQL标准的特定于MySQL的扩展。它们可能在其他数据库系统中不可用。

MySQL变量赋值
有两种方法可以为用户定义的变量赋值。

第一种方法是使用SET如下语句：

SET @variable_name := value; 
您可以使用:=或=作为SET语句中的赋值运算符。例如，语句将数字100赋给变量@counter。

SET @counter := 100; 
为变量赋值的第二种方法是使用SELECT语句。在这种情况下，必须使用：=赋值运算符，因为在SELECT语句中，MySQL将=运算符视为等于运算符。

SELECT @variable_name := value; 
在赋值之后，您可以在允许表达式的后续语句中使用变量，例如，在WHERE子句，INSERT或UPDATE语句中。

MySQL变量示例
以下语句获取products表中最昂贵的产品，并将价格分配给用户定义的变量@msrp：
```
SELECT 
    @msrp:=MAX(msrp)
FROM
    products; 
+------------------+
| @msrp:=MAX(msrp) |
+------------------+
|           214.30 |
+------------------+
1 row in set (0.00 sec)
```
以下语句使用@msrp变量来查询最昂贵产品的信息。
```
SELECT 
    productCode, productName, productLine, msrp
FROM
    products
WHERE
    msrp = @msrp; 
+-------------+--------------------------+--------------+--------+
| productCode | productName              | productLine  | msrp   |
+-------------+--------------------------+--------------+--------+
| S10_1949    | 1952 Alpine Renault 1300 | Classic Cars | 214.30 |
+-------------+--------------------------+--------------+--------+
1 row in set (0.00 sec)
```
有时，您希望在表中插入一行，获取最后一个插入ID，并使用它将数据插入另一个表。在这种情况下，您可以使用用户定义的变量来存储AUTO_INCREMENT列生成的最新ID ，如下所示。
```
SELECT @id:=LAST_INSERT_ID(); 
```
用户定义的变量只能包含单个值。如果SELECT语句返回多个值，则变量将采用结果中最后一行的值。
```
SELECT 
    @buyPrice:=buyprice
FROM
    products
WHERE
    buyprice > 95
ORDER BY buyprice; 
+---------------------+
| @buyPrice:=buyprice |
+---------------------+
|               95.34 |
|               95.59 |
|               98.30 |
|               98.58 |
|              101.51 |
|              103.42 |
+---------------------+
6 rows in set (0.01 sec)
SELECT @buyprice; 
+-----------+
| @buyprice |
+-----------+
|    103.42 |
+-----------+
1 row in set (0.00 sec)
```