MySQL 使用正则查询

正则表达式简介
正则表达式是描述搜索模式的特殊字符串。它是一个功能强大的工具，可以根据模式为您提供简明灵活的方法来识别文本字符串，例如字符和单词。

例如，您可以使用正则表达式来搜索电子邮件，IP地址，电话号码，社会安全号码或具有特定模式的任何内容。

正则表达式使用自己的语法，可以由正则表达式处理器解释。正则表达式广泛用于从编程语言到包括MySQL在内的数据库的几乎平台中。

使用正则表达式的优点是，您不仅可以根据LIKE运算符中带百分号（％）和下划线（_）的固定模式搜索字符串。正则表达式具有更多的元字符来构造灵活的模式。

使用正则表达式的缺点是很难理解和维护这种复杂的模式。因此，您应在SQL语句的注释中描述正则表达式的含义。此外，如果在正则表达式中使用复杂模式，则在某些情况下，数据检索的速度会降低。

正则表达式的缩写是regex或regexp。

MySQL REGEXP运算符
MySQL适应Henry Spencer实现的正则表达式。MySQL允许您使用REGEXP运算符在SQL语句中匹配模式。

以下说明REGEXP了WHERE  子句中运算符   的语法：
```
SELECT 
    column_list
FROM
    table_name
WHERE
    string_column REGEXP pattern; 
```
此语句执行 string_column对pattern的模式匹配。

如果string_column匹配的值为WHERE子句中pattern的表达式返回true，否则返回false。

如果string_column或者pattern是NULL，结果是NULL。

除了REGEXP运算符之外，您还可以使用RLIKE运算符，它是运算符的同义词REGEXP。

REGEXP运营商的否定形式是NOT REGEXP。

MySQL REGEXP示例
假设您要查找姓氏以字符A，B或C开头的所有产品。您可以在以下SELECT语句中使用正则表达式：
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^(A|B|C)'
ORDER BY productname; 
+--------------------------------+
| productname                    |
+--------------------------------+
| America West Airlines B757-200 |
| American Airlines: B767-300    |
| American Airlines: MD-11S      |
| ATA: B757-300                  |
| Boeing X-32A JSF               |
| Collectable Wooden Train       |
| Corsair F4U ( Bird Cage)       |
+--------------------------------+
7 rows in set (0.00 sec)
```
模式允许您查找名称以A，B或C开头的产品。

字符^表示从字符串的开头匹配。
字符| 意味着如果一个匹配不匹配则搜索替代方案。
下表说明了正则表达式中一些常用的元字符和构造。

元字符	描述
^	匹配搜索字符串开头的位置
$	匹配搜索字符串结果的位置
.	匹配任何字符
[…]	匹配方括号内指定的任何字符
[^…]	匹配方括号内未指定的任何字符
p1|p2	匹配任何模式p1或p2
*	匹配前面的字符零次或多次
+	匹配前一个字符一次或多次
{n}	匹配前一个字符的n个实例
{m,n}	匹配从前一个字符的m到n个实例
要查找名称以a字符开头的产品，请使用元字符  '^'在名称的开头匹配：
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^a'; 
+--------------------------------+
| productname                    |
+--------------------------------+
| American Airlines: B767-300    |
| America West Airlines B757-200 |
| ATA: B757-300                  |
| American Airlines: MD-11S      |
+--------------------------------+
4 rows in set (0.00 sec)
```
如果你想REGEXP操作员在大小写敏感的方式比较字符串，则可以使用二进制运算符来投一个字符串转换为二进制字符串。

因为MySQL逐字节而不是逐字符地比较二进制字符串。这允许字符串比较区分大小写。

例如，以下语句仅匹配"C"产品名称开头的大写。
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP BINARY '^C'; 
+--------------------------+
| productname              |
+--------------------------+
| Collectable Wooden Train |
| Corsair F4U ( Bird Cage) |
+--------------------------+
2 rows in set (0.00 sec)
```
要查找名称以f结尾的产品，您可以使用'f$'匹配字符串的结尾。
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP 'f$'; 
+------------------+
| productname      |
+------------------+
| Boeing X-32A JSF |
+------------------+
1 row in set (0.00 sec)
```
要查找名称中包含单词的产品"ford"，请使用以下查询：
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP 'ford'; 
+----------------------------------+
| productname                      |
+----------------------------------+
| 1968 Ford Mustang                |
| 1969 Ford Falcon                 |
| 1940 Ford Pickup Truck           |
| 1911 Ford Town Car               |
| 1932 Model A Ford J-Coupe        |
| 1926 Ford Fire Engine            |
| 1913 Ford Model T Speedster      |
| 1934 Ford V8 Coupe               |
| 1903 Ford Model A                |
| 1976 Ford Gran Torino            |
| 1940s Ford truck                 |
| 1957 Ford Thunderbird            |
| 1912 Ford Model T Delivery Wagon |
| 1940 Ford Delivery Sedan         |
| 1928 Ford Phaeton Deluxe         |
+----------------------------------+
15 rows in set (0.00 sec)
```
要查找名称中包含10个字符的产品，请使用“ ^'和” $来匹配产品名称的开头和结尾，并在中间重复{10}任何字符的.'时间，如以下查询所示：
```
SELECT 
    productname
FROM
    products
WHERE
    productname REGEXP '^.{10}$'; 
+-------------+
| productname |
+-------------+
| HMS Bounty  |
| Pont Yacht  |
+-------------+
2 rows in set (0.00 sec)
```