Mysql Like 优化
要提高Mysql 的查询效率最有效的办法是让所有的查询走索引字段，但是在Mysql中 Like 关键字只有对前缀查询("keyword%")走索引。

select * from table where name like "keyword%"
我们常常需要模糊查询（"%keyword%"）或后缀查询("%keyword")。

解决办法的思路是想办法让模糊查询和后缀查询都能走索引就可以达到目的。

后缀查询解决方案：使用新建字段反转索引然后关键字段反转变成前缀查询：

select * from table where rename like "drowyey%"