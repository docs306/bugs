MySQL 注释深入理解

注释
注释可用于记录SQL语句的目的或存储过程中代码块的逻辑。在解析SQL代码时，MySQL会忽略注释部分。除了可执行注释之外，它只执行SQL部分，我们将在下一节中讨论。

MySQL支持三种注释样式：

从'-- '一行到最后一行。双破折号注释样式在第二个破折号后至少需要空格或控制字符（空格，制表符，换行符等）。
SELECT * FROM users; -- This is a comment 
请注意，标准SQL在第二个破折号后不需要空格。MySQL使用空格来避免某些SQL构造的问题，例如：

SELECT 10--1; 
语句返回11.如果MySQL没有使用空格，它将返回10。

从'#'一行到最后一行。
```
SELECT 
    lastName, firstName
FROM
    employees
WHERE
    reportsTo = 1002; # get subordinates of Diane 
```
C风格的评论/**/可以跨越多行。您可以使用此注释样式来记录SQL代码块。
```
/*
    Get sales rep employees
    that reports to Anthony
*/
 
SELECT 
    lastName, firstName
FROM
    employees
WHERE
    reportsTo = 1143
        AND jobTitle = 'Sales Rep'; 
```
请注意，MySQL不支持嵌套注释。

可执行的注释
MySQL提供可执行注释以支持不同数据库之间的可移植性。这些注释允许您嵌入仅在MySQL中执行但不在其他数据库中执行的SQL代码。

以下说明了可执行注释语法：

/*! MySQL-specific code */ 
例如，以下语句使用可执行注释：

SELECT 1 /*! +1 */ 
语句返回2而不是1.但是，如果在其他数据库系统中执行它，它将返回1。

如果要从特定版本的MySQL执行注释，请使用以下语法：

/*!##### MySQL-specific code */ 
字符串'#####'表示可以执行注释的MySQL的最低版本。第一个＃是主要版本，例如5或8.第二个2号（##）是次要版本。最后2个是补丁级别。

例如，以下注释仅在MySQL 5.1.10或更高版本中可执行：
```
CREATE TABLE t1 (
    k INT AUTO_INCREMENT,
    KEY (k)
)  /*!50110 KEY_BLOCK_SIZE=1024; */ 
```