MySQL 获取当天日期

使用内置日期函数获取MySQL今天的日期
有时，您可能希望查询表中的数据以获取具有日期列的行，例如：
```
SELECT 
    column_list
FROM
    table_name
WHERE
    expired_date = today;
``` 
要获取今天的日期，请使用以下CURDATE()函数：
```
mysql> SELECT CURDATE() today;
+------------+
| today      |
+------------+
| 2019-08-30 |
+------------+
1 row in set (0.00 sec)
```
或者您可以从NOW()函数返回的当前时间获取日期部分：
```
mysql> SELECT DATE(NOW()) today;
+------------+
| today      |
+------------+
| 2019-08-30 |
+------------+
1 row in set (0.00 sec)
```
所以查询应改为：
```
SELECT 
    column_list
FROM
    table_name
WHERE
    expired_date = CURDATE(); 
```
如果expired_date列包含日期和时间部分，则应使用DATE()函数仅提取日期部分并将其与当前日期进行比较：
```
SELECT 
    column_list
FROM
    table_name
WHERE
    DATE(expired_date) = CURDATE(); 
```
创建你今天的MySQL存储函数
如果CURDATE()在查询中经常使用函数，并且希望用today()函数替换它以使查询更具可读性，则可以创建自己的存储函数，命名today()如下：
```
DELIMITER $$
CREATE FUNCTION today()
RETURNS DATE
BEGIN
RETURN CURDATE();
END$$
DELIMITER ; 
```
现在，您可以使用today()您创建的函数，如下所示：
```
+------------+
| today()    |
+------------+
| 2019-08-30 |
+------------+
1 row in set (0.00 sec)
```
明天怎么样？应这么简单：
```
+------------+
| Tomorrow   |
+------------+
| 2019-08-31 |
+------------+
1 row in set (0.00 sec)
```
昨天也很容易：
```
mysql> SELECT today() - interval 1 day Yesterday;
+------------+
| Yesterday  |
+------------+
| 2019-08-29 |
+------------+
1 row in set (0.00 sec)
```