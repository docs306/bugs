MySQL 表的复制
简介：在本教程中，您将学习如何使用CREATE TABLE和SELECT语句将表复制到同一数据库中或从一个数据库复制到另一个数据库。

MySQL将表复制到新表中
将数据从现有表复制到新表在某些情况下非常有用，例如备份数据和复制生产数据以进行测试。

要将数据从表复制到新表，请使用CREATE TABLE和SELECT语句，如下所示：
```
CREATE TABLE new_table 
SELECT col, col2, col3 
FROM
    existing_table; 
```
首先，MySQL创建一个新表，其名称在CREATE TABLE语句之后，新表的结构由SELECT语句的结果集定义。然后，MySQL将来自SELECT语句的数据填充到新表中。

要将现有表中的部分数据复制到新表，请在SELECT语句中使用WHERE子句，如下所示：
```
CREATE TABLE new_table 
SELECT col1, col2, col3 
FROM
    existing_table
WHERE
    conditions; 
```
在创建表之前检查您要创建的表是否已存在非常重要。为此，请IF NOT EXIST在CREATE TABLE语句中使用子句。将数据从现有表复制到新表的完整命令如下：
```
CREATE TABLE new_table 
SELECT col1, col2, col3 
FROM
    existing_table
WHERE
    conditions; 
```
请注意，上面的语句只是复制表及其数据。它不会复制与表关联的其他数据库对象，如索引，主键约束，外键约束，  触发器等。

要从一个表以及表的所有从属对象复制数据，请使用以下语句：
```
CREATE TABLE IF NOT EXISTS new_table LIKE existing_table;
 
INSERT new_table
SELECT * FROM existing_table; 
```
我们需要执行两个语句。第一个语句通过复制来创建一个新表 new_table。第二个语句将现有existing_table表中的数据插入到new_table。

MySQL复制表示例
从下面的语句复制数据offices表到一个名为新表offices_bk中。
```
CREATE TABLE IF NOT EXISTS offices_bk 
SELECT * FROM offices; 
```
我们可以通过查询office_bk表中的数据来验证副本，如下所示：
```
SELECT
    *
FROM
    offices_bk; 
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
| officeCode | city          | phone            | addressLine1             | addressLine2 | state | country   | postalCode | territory |
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
| 1          | San Francisco | +1 650 219 4782  | 100 Market Street        | Suite 300    | CA    | USA       | 94080      | NA        |
| 2          | Boston        | +1 215 837 0825  | 1550 Court Place         | Suite 102    | MA    | USA       | 02107      | NA        |
| 3          | NYC           | +1 212 555 3000  | 523 East 53rd Street     | apt. 5A      | NY    | USA       | 10022      | NA        |
| 4          | Paris         | +33 14 723 5555  | 43 Rue Jouffroy D'abbans | NULL         | NULL  | France    | 75017      | EMEA      |
| 5          | Beijing       | +86 33 224 5000  | 4-1 Haidian Area         | NULL         | BJ    | China     | 110000     | NA        |
| 6          | Sydney        | +61 2 9264 2451  | 5-11 Wentworth Avenue    | Floor #2     | NULL  | Australia | NSW 2010   | APAC      |
| 7          | London        | +44 20 7877 2041 | 25 Old Broad Street      | Level 7      | NULL  | UK        | EC2N 1HN   | EMEA      |
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
7 rows in set (0.00 sec)
```
如果我们只想复制美国的办事处，我们可以WHERE在SELECT声明中添加如下条款：
```
CREATE TABLE IF NOT EXISTS offices_usa 
SELECT * 
FROM
    offices
WHERE
    country = 'USA'; 
```
以下语句从offices_usa表中获取所有数据。
```
SELECT 
    *
FROM
    offices_usa; 
+------------+---------------+-----------------+----------------------+--------------+-------+---------+------------+-----------+
| officeCode | city          | phone           | addressLine1         | addressLine2 | state | country | postalCode | territory |
+------------+---------------+-----------------+----------------------+--------------+-------+---------+------------+-----------+
| 1          | San Francisco | +1 650 219 4782 | 100 Market Street    | Suite 300    | CA    | USA     | 94080      | NA        |
| 2          | Boston        | +1 215 837 0825 | 1550 Court Place     | Suite 102    | MA    | USA     | 02107      | NA        |
| 3          | NYC           | +1 212 555 3000 | 523 East 53rd Street | apt. 5A      | NY    | USA     | 10022      | NA        |
+------------+---------------+-----------------+----------------------+--------------+-------+---------+------------+-----------+
3 rows in set (0.00 sec)
```
假设，我们不仅要复制数据，还要复制与offices表关联的所有数据库对象，我们使用以下语句：
```
CREATE TABLE offices_dup LIKE offices;
 
INSERT offices_dup
SELECT * FROM offices; 
```
MySQL将表复制到另一个数据库
有时，您希望将表复制到其他数据库。在这种情况下，您使用以下语句：
```
CREATE TABLE destination_db.new_table 
LIKE source_db.existing_table;
 
INSERT destination_db.new_table 
SELECT *
FROM source_db.existing_table; 
```
第一个语句通过从源数据库（source_db）复制现有表（existing_table）在目标数据库（destination_db）中创建新表new_table。

第二个语句将数据从源数据库中的现有表复制到目标数据库中的新表。

我们来看下面的例子。

首先，我们使用以下语句创建名为testdb的数据库：
```
CREATE DATABASE IF NOT EXISTS testdb;
``` 
其次，我们通过从mysqldemo数据库中的offices表复制其结构来创建testdb库中的 offices表。
```
CREATE TABLE testdb.offices LIKE mysqldemo.offices; 
```
第三，我们将mysqldemo.offices表中的数据复制到testdb.offices表。
```
INSERT testdb.offices
SELECT *
FROM mysqldemo.offices; 
```
让我们验证testdb.offices表格中的数据。
```
SELECT
    *
FROM
    testdb.offices; 
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
| officeCode | city          | phone            | addressLine1             | addressLine2 | state | country   | postalCode | territory |
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
| 1          | San Francisco | +1 650 219 4782  | 100 Market Street        | Suite 300    | CA    | USA       | 94080      | NA        |
| 2          | Boston        | +1 215 837 0825  | 1550 Court Place         | Suite 102    | MA    | USA       | 02107      | NA        |
| 3          | NYC           | +1 212 555 3000  | 523 East 53rd Street     | apt. 5A      | NY    | USA       | 10022      | NA        |
| 4          | Paris         | +33 14 723 5555  | 43 Rue Jouffroy D'abbans | NULL         | NULL  | France    | 75017      | EMEA      |
| 5          | Beijing       | +86 33 224 5000  | 4-1 Haidian Area         | NULL         | BJ    | China     | 110000     | NA        |
| 6          | Sydney        | +61 2 9264 2451  | 5-11 Wentworth Avenue    | Floor #2     | NULL  | Australia | NSW 2010   | APAC      |
| 7          | London        | +44 20 7877 2041 | 25 Old Broad Street      | Level 7      | NULL  | UK        | EC2N 1HN   | EMEA      |
+------------+---------------+------------------+--------------------------+--------------+-------+-----------+------------+-----------+
7 rows in set (0.00 sec)
```