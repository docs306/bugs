CREATE TRIGGER语句在MySQL中创建触发器。

在继续学习本教程之前，您应先了解SQL触发器的并在MySQL中触发实现。

MySQL触发语法
要创建新触发器，请使用CREATE TRIGGER语句。以下说明了 CREATE TRIGGER语句的语法：
```
CREATE TRIGGER trigger_name trigger_time trigger_event
 ON table_name
 FOR EACH ROW
 BEGIN
 ...
 END; 
```
让我们更详细地研究上面的语法。

- 您在CREATE TRIGGER语句后面添加了触发器名称。触发器名称应遵循的命名约定[trigger time]_[table name]_[trigger event]，例如before_employees_update。
- 触发激活时间可以是BEFORE或AFTER。您必须在定义触发器时指定激活时间。使用BEFORE对表进行更改之前处理操作，然而需要在更改后处理操作，则使用 AFTER关键字。
- 触发事件可以是INSERT，UPDATE或者DELETE事件导致触发器被调用。只有一个事件可以调用触发器。要定义由多个事件调用的触发器，您必须定义多个触发器，每个事件对应一个触发器。
- 触发器必须与特定表关联。如果没有表触发器将不存在，因此您必须在ON关键字后指定表名。
您将SQL语句放在BEGIN和END阻止之间。您可以在此定义触发器的逻辑。
MySQL触发器示例
让我们开始在MySQL中创建一个触发器来记录employees表的更改。
```
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.00 sec)
```
首先，创建一个表名为 employees_audit保持更改employee表。以下语句创建employee_audit表。
```
CREATE TABLE employees_audit (
    id INT AUTO_INCREMENT PRIMARY KEY,
    employeeNumber INT NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    changedat DATETIME DEFAULT NULL,
    action VARCHAR(50) DEFAULT NULL
); 
```
接下来，创建BEFORE UPDATE在对employees表进行更改之前调用的触发器。
```
DELIMITER $$
CREATE TRIGGER before_employee_update 
	BEFORE UPDATE ON employees 
	FOR EACH ROW
BEGIN
	INSERT INTO employees_audit 
	SET action = 'update',
	employeeNumber = OLD.employeeNumber,
	lastname = OLD.lastname,
	changedat = NOW( );
END $$
DELIMITER ;
``` 
触发的定义里面，我们使用OLD 关键字来访问employeeNumber ，并lastname受到触发行的列。

注意：在为INSERT定义的触发器中，您只能使用NEW关键字。您无法使用OLD关键字。但是，在为DELETE定义的触发器中，没有新行，因此您只能使用OLD关键字。在UPDATE触发器中，OLD引用更新前NEW的行，并在更新后引用行。

然后，要查看当前数据库中的所有触发器，请使用SHOW TRIGGERS如下语句：
```
SHOW TRIGGERS; 
+------------------------+--------+-----------+------------------------------------------------------------------------------------------------------------------------------------------------+--------+------------------------+------------------------------------------------------------------------------------------------------------------------+----------------+----------------------+----------------------+--------------------+
| Trigger                | Event  | Table     | Statement                                                                                                                                      | Timing | Created                | sql_mode                                                                                                               | Definer        | character_set_client | collation_connection | Database Collation |
+------------------------+--------+-----------+------------------------------------------------------------------------------------------------------------------------------------------------+--------+------------------------+------------------------------------------------------------------------------------------------------------------------+----------------+----------------------+----------------------+--------------------+
| before_employee_update | UPDATE | employees | BEGIN
INSERT INTO employees_audit
SET action = 'update',
employeeNumber = OLD.employeeNumber,
lastname = OLD.lastname,
changedat = NOW( );
END | BEFORE | 2019-08-23 15:44:39.62 | STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION | root@localhost | utf8                 | utf8_general_ci      | latin1_swedish_ci  |
+------------------------+--------+-----------+------------------------------------------------------------------------------------------------------------------------------------------------+--------+------------------------+------------------------------------------------------------------------------------------------------------------------+----------------+----------------------+----------------------+--------------------+
1 row in set (0.00 sec)
```
之后，更新employees表以检查是否调用了触发器。
```
UPDATE employees 
SET 
    lastName = 'Phan'
WHERE
    employeeNumber = 1056; 
```
最后，要检查UPDATE语句是否调用了触发器，可以employees_audit使用以下查询查询表：
```
SELECT 
    *
FROM
    employees_audit; 
```
以下是查询的输出：
```
+----+----------------+-----------+---------------------+--------+
| id | employeeNumber | lastname  | changedat           | action |
+----+----------------+-----------+---------------------+--------+
|  1 |           1056 | Patterson | 2019-08-23 15:48:10 | update |
+----+----------------+-----------+---------------------+--------+
1 row in set (0.00 sec)
```