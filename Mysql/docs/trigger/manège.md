MySQL 触发器管理
简介：在本教程中，您将学习如何管理触发器，包括在MySQL数据库中显示，修改和删除触发器。

创建触发器之后可以在数据文件夹中显示，其中包含触发定义文件定义。触发器作为纯文本文件存储在以下数据库文件夹中：

 /data_folder/database_name/table_name.trg 
Linux 下查看部分数据列表如下：
```
...
-rw-r-----  1 _mysql  _mysql   20998 Aug 12 20:17 productlines.frm
-rw-r-----  1 _mysql  _mysql   98304 Aug 12 20:17 productlines.ibd
-rw-r-----  1 _mysql  _mysql     689 Aug 23 17:46 products.TRG
-rw-r-----  1 _mysql  _mysql    8994 Aug 23 17:24 products.frm
-rw-r-----  1 _mysql  _mysql  147456 Aug 23 17:46 products.ibd
...
```
MySQL为您提供了一种通过查询information_schema数据库中的triggers表来显示触发器的替代方法，如下所示：
```
SELECT 
    *
FROM
    information_schema.triggers
WHERE
    trigger_schema = 'database_name'
    AND trigger_name = 'trigger_name'; 
```
语句允许您查看触发器的内容及其元数据，例如关联的表名和定义器，这是创建触发器的MySQL用户的名称。

如果要检索特定information_schema数据库中的所有触发器，则需要使用以下SELECT  语句从数据库中的triggers表中查询数据：
```
SELECT 
    *
FROM
    information_schema.triggers
WHERE
    trigger_schema = 'database_name'; 
```
要查找与特定表关联的所有触发器，请使用以下查询：
```
SELECT 
    *
FROM
    information_schema.triggers
WHERE
    trigger_schema = 'database_name'
    AND event_object_table = 'table_name'; 
```
例如，以下语句返回与数据库中的employees表关联的所有触发器mysqldemo。
```
SELECT 
    *
FROM
    information_schema.triggers
WHERE
    trigger_schema = 'mysqldemo'
    AND event_object_table = 'employees'; 
```
//数据太多就不显示，一定要亲手操作一下
MySQL SHOW TRIGGERS声明
在特定数据库中显示触发器的另一种快速方法是使用SHOW TRIGGERS如下语句：
```
SHOW TRIGGERS [FROM|IN] database_name
[LIKE expr | WHERE expr]; 
```
例如，如果要查看当前数据库中的所有触发器，可以使用以下SHOW TRIGGERS语句：
```
SHOW TRIGGERS; 
```
要获取特定数据库中的所有触发器，请在SHOW TRIGGERS  语句中指定数据库名称，如下所示：
```
SHOW TRIGGERS FROM mysqldemo; 
```
它返回mysqldemo数据库中的所有触发器。

要获取与特定表关联的所有触发器，请使用WHERE语句中的子句SHOW TRIGGERS。以下语句返回与employees表关联的所有触发器：
```
SHOW TRIGGERS FROM mysqldemo
WHERE `table` = 'employees'; 
```
//数据太多就不显示，一定要亲手操作一下
请注意，我们使用反引号（`）来包装table列，因为它table是MySQL中的保留关键字。

执行SHOW TRIGGERS语句时，MySQL返回以下列。

- 触发器：存储触发器的名称，例如before_employee_update触发器。
- 事件：指定调用触发器的事件，例如INSERT，UPDATE或DELETE。
- 表：指定触发器与employees表格相关联的表格。
- 语句：存储在调用触发器时将要执行的语句或复合语句。
- 时间：接受两个值：BEFORE和AFTER。它指定触发器的激活时间。
- 已创建：记录创建触发器时创建的时间。
sql_mode：指定触发器执行时的SQL模式。
定义者：记录创建触发器的帐户。
请注意，要执行SHOW TRIGGERS语句，您必须具有SUPER权限。

删除触发器
要删除现有触发器，请使用以下DROP TRIGGER语句：

DROP TRIGGER table_name.trigger_name; 
例如，如果要删除与employees表关联的  before_employees_update触发器，可以执行以下语句：

DROP TRIGGER employees.before_employees_update; 
要修改触发器，必须先将其删除，然后使用新代码重新创建。MySQL中没有ALTER TRIGGER这样的语句，因此，您无法修改现有的触发器，如修改其他数据库对象，如表，视图和  存储过程。

