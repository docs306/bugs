# MySQL事件修改
简介：本教程向您展示如何使用ALTER EVENT语句修改现有的MySQL事件。在本教程之后，您将了解如何修改事件的计划，如何启用或禁用事件以及如何重命名事件。

MySQL允许您更改现有事件的各种属性。要更改现有事件，请使用以下  ALTER EVENT语句：
```
ALTER EVENT event_name
ON SCHEDULE schedule
ON COMPLETION [NOT] PRESERVE
RENAME TO new_event_name
ENABLE | DISABLE
DO
  event_body 
```
请注意，ALTER EVENT语句仅适用于现有事件。如果您尝试修改不存在的事件，MySQL将发出错误消息，因此，SHOW EVENTS在更改事件之前，应始终使用  语句检查事件是否存在。
```
SHOW EVENTS FROM classicmodels; 
```

### 更改事件示例
让我们创建一个示例事件来演示ALTER EVENT语句的各种功能  。

以下语句创建一个事件，每分钟将新记录插入messages表中。
```
CREATE EVENT test_event_04
ON SCHEDULE EVERY 1 MINUTE
DO
   INSERT INTO messages(message,created_at)
   VALUES('Test ALTER EVENT statement',NOW()); 
```
改变时间表
要使事件每2分钟运行一次，请使用以下语句：
```
ALTER EVENT test_event_04
ON SCHEDULE EVERY 2 MINUTE; 
```
改变活动体
您还可以通过指定新逻辑来更改事件的主体，如下所示：
```
ALTER EVENT test_event_04
DO
   INSERT INTO messages(message,created_at)
   VALUES('Message from event',NOW()); 
```
您可以等待2分钟并messages再次检查表格：
```
SELECT * FROM messages; 
```
禁用事件
要禁用事件，请使用以下语句：
```
ALTER EVENT test_event_04
DISABLE; 
```
您可以使用以下SHOW EVENTS语句检查事件的状态  ：
```
SHOW EVENTS FROM mysqldemo; 
```
启用事件
要启用已禁用的事件，请ENABLE在ALTER EVENT语句后使用关键字  ，如下所示：
```
ALTER EVENT test_event_04
ENABLE; 
```
重命名事件
MySQL没有为您提供  RENAME EVENT语句。幸运的是，您可以使用  ALTER EVENT重命名现有事件，如下所示：
```
ALTER EVENT test_event_04
RENAME TO test_event_05; 
```
将事件移动到另一个数据库
您可以使用以下RENAME TO子句将事件从数据库移动到另一个数据库  ：
```
ALTER EVENT mysqldemo.test_event_05
RENAME TO newdb.test_event_05 
```
假设newdb数据库在MySQL数据库服务器中可用。