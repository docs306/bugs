# MySQL 存储过程
简介：在本教程中，我们将向您展示如何列出MySQL数据库中的所有存储过程，并向您介绍一个非常有用的显示存储过程源代码的语句。

MySQL为我们提供了一些有用的语句，可以帮助您更有效地管理存储过程 这些语句包括列出存储过程和显示存储过程的源代码。

显示存储过程的特征
要显示存储过程的特征，请使用以下  SHOW PROCEDURE STATUS语句：
```
SHOW PROCEDURE STATUS [LIKE 'pattern' | WHERE expr]; 
```
SHOW PROCEDURE STATUS语句是SQL标准的MySQL扩展。此语句为您提供存储过程的特征，包括数据库，存储过程名称，类型，创建者等。

您可以使用  LIKEor WHERE子句根据各种条件过滤掉存储过程。

要列出您有权访问的数据库的所有存储过程，请使用以下  SHOW PROCEDURE STATUS语句：
```
SHOW PROCEDURE STATUS; 
```
如果要在特定数据库中仅显示存储过程，可以使用SHOW PROCEDURE STATUS语句中的WHERE子句  ：
```
SHOW PROCEDURE STATUS WHERE db = 'mysqldemo'; 
```
如果要显示具有特定模式的存储过程（例如，其名称包含）product，则可以使用LIKE作为以下命令：
```
SHOW PROCEDURE STATUS WHERE name LIKE '%product%'; 
```
显示存储过程的源代码
要显示特定存储过程的源代码，请使用以下  SHOW CREATE PROCEDURE语句：
```
SHOW CREATE PROCEDURE stored_procedure_name 
```
您可以在SHOW CREATE PROCEDURE关键字后指定存储过程的名称  。例如，要显示GetAllProducts存储过程的代码，请使用以下语句：
```
SHOW CREATE PROCEDURE GetAllProducts; 
```