# MySQL IF语句

MySQL IF语句允许您根据表达式的特定条件或值执行一组SQL语句。要在MySQL中形成表达式，您可以组合文字，变量，运算符甚至函数。表达式可以返回  TRUE FALSE，或NULL。

请注意，有一个IF函数与IF本教程中指定的语句不同。

MySQL IF语句语法
```
IF expression THEN 
   statements;
END IF; 
```
如果表达式求值为TRUE，则将执行语句，否则，控件将传递给后面的下一个语句END IF。

### MySQL IF-ELSE语句
如果要在表达式未计算为TRUE时执行语句，请使用以下IF-ELSE语句：
```
IF expression THEN
   statements;
ELSE
   else-statements;
END IF; 
```
以下流程图说明了IF ELSE语句：


### MySQL IF-ELSEIF-ELSE语句
如果要根据多个表达式有条件地执行语句，请使用以下IF-ELSEIF-ELSE语句：
```
IF expression THEN
   statements;
ELSEIF elseif-expression THEN
   elseif-statements;
...
ELSE
   else-statements;
END IF; 
```
如果表达式（expression）求值为TRUE，则IF分支中的语句（statements）将执行;如果表达式求值为FALSE，则如果elseif_expression的计算结果为TRUE，MySQL将执行elseif-expression，否则执行ELSE分支中的else-statements语句。

### MySQL IF语句示例
以下示例说明了如何使用IF-ESLEIF-ELSE语句。GetCustomerLevel()   存储过程接受两个参数的客户数量和客户的水平。

- 首先，它从customers表中获得信用额度。
- 然后，根据信用额度，它决定了客户等级：PLATINUM，GOLD，和SILVER。
- 参数p_customerlevel存储客户的级别，并由调用程序使用。

```
DELIMITER $$
 
CREATE PROCEDURE GetCustomerLevel(
    in  p_customerNumber int(11), 
    out p_customerLevel  varchar(10))
BEGIN
    DECLARE creditlim double;
 
    SELECT creditlimit INTO creditlim
    FROM customers
    WHERE customerNumber = p_customerNumber;
 
    IF creditlim > 50000 THEN
        SET p_customerLevel = 'PLATINUM';
    ELSEIF (creditlim <= 50000 AND creditlim >= 10000) THEN
        SET p_customerLevel = 'GOLD';
    ELSEIF creditlim < 10000 THEN
        SET p_customerLevel = 'SILVER';
    END IF;
 
END $$

DELIMITER ; 
```
执行结果：
```
mysql> call GetCustomerLevel(141,@p_customerLevel);
Query OK, 1 row affected (0.00 sec)

mysql> select @p_customerLevel;
+------------------+
| @p_customerLevel |
+------------------+
| PLATINUM         |
+------------------+
1 row in set (0.00 sec)
```
以下流程图演示了确定客户级别的逻辑。

MySQL IF语句流程图

