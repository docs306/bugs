MySQL存储过程
我们将开发一个简单的存储过程，以GetAllProducts()  帮助您熟悉语法。GetAllProducts()  存储过程选择所有产品从products 表。

启动mysql客户端工具并键入以下命令：
```sql
 DELIMITER //
 CREATE PROCEDURE GetAllProducts()
   BEGIN
   SELECT productCode, productName FROM products;
   END //
 DELIMITER ; 
```
执行命令：
```
mysql> DELIMITER //
mysql> CREATE PROCEDURE GetAllProducts()
-> BEGIN
-> SELECT productCode, productName FROM products;
-> END //
Query OK, 0 rows affected (0.02 sec)

mysql> DELIMITER ;
```
让我们更详细地检查存储过程：

第一个命令是DELIMITER //，与存储过程语法无关。DELIMITER语句将标准分隔符（分号（;）更改为另一个分号）。在这种情况下，分隔符从分号（;）更改为双斜线 //。为什么我们要更改分隔符？因为我们希望将存储过程作为一个整体传递给服务器，而不是让mysql工具一次解释每个语句。在END关键字之后，我们使用分隔符//  来指示存储过程的结束。最后一个命令（DELIMITER; ）将分隔符更改回分号（;）。
我们使用CREATE PROCEDURE  语句来创建一个新的存储过程。我们在CREATE PROCEDURE  语句后指定存储过程的名称。在这种情况下，存储过程的名称是GetAllProducts 。我们将括号放在存储过程的名称之后。
BEGIN和之间的部分  END  称为存储过程的主体。您将声明性SQL语句放在正文中以处理业务逻辑。在这个存储过程中，我们使用一个简单的SELECT语句来查询products表中的数据。
查看存储过程
我们可以使用如下命令查看指定的数据库中有哪些存储过程：
```
 select name from mysql.proc where db='mysqldemo'; 
运行结果：

+----------------+
| name           |
+----------------+
| GetAllProducts |
+----------------+
1 row in set (0.00 sec)
```
调用存储过程
要调用存储过程，请使用以下SQL命令：
```
CALL stored_procedure_name(); 
```
您使用CALL 语句调用存储过程，例如，要调用GetAllProducts() 存储过程，请使用以下语句：
```
CALL GetAllProducts(); 
```
如果执行上述语句，您将获得products表中的所有产品。
```
+-------------+---------------------------------------------+
| productCode | productName                                 |
+-------------+---------------------------------------------+
| S10_1678    | 1969 Harley Davidson Ultimate Chopper       |
| S10_1949    | 1952 Alpine Renault 1300                    |
| S10_2016    | 1996 Moto Guzzi 1100i                       |
| S10_4698    | 2003 Harley-Davidson Eagle Drag Bike        |
| S10_4757    | 1972 Alfa Romeo GTA                         |
| S10_4962    | 1962 LanciaA Delta 16V                      |
| S12_1099    | 1968 Ford Mustang                           |
| S12_1108    | 2001 Ferrari Enzo                           |
| S12_1666    | 1958 Setra Bus                              |
| S12_2823    | 2002 Suzuki XREO                            |
...
```