下载MySQL安装程序
如果要在Windows环境中安装MySQL，使用MySQL安装程序是最简单的方法。
MySQL Installer为您的所有MySQL软件需求提供易于使用，基于向导的安装体验。

产品包括最新版本：
[MySQL Installer](https://dev.mysql.com/downloads/installer/)
产品包括最新版本：
- MySQL服务器
- MySQL连接器
- MySQL Workbench和模型实例
- MySQL通知程序
- MySQL Visual Studio的工具
- MySQL Excel 工具
- MySQL示例数据库
- MySQL文档

Mysql包下载地址
[Mysql下载地址](https://dev.mysql.com/downloads/mysql/)


MySQL Workbench 导入实例数据库
- 步骤1.从MySQL实例数据库下载mysqldemo数据库。

- 步骤2.将下载的文件解压缩到临时文件夹中。您可以使用任何所需的文件夹。为简单起见，我们将其解压缩到C:\temp  文件夹

- 步骤3.从中启动MySQL Workbench应用程序Program Files > MySQL > MySQL Workbench 8.0。目前是使用最新版本的，根据自己的版本去使用

- 步骤4.要添加用于查询的新数据库连接，请单击+以下内容

- 步骤5.设置新连接：必须在此“安装新连接”窗口中输入所有连接参数。需要以下信息：
```
连接名称：如果你连接到localhost，我们就输入local。
主机名：  127.0.0.1 或 localhost。
用户名：我们使用管理员用户root。
端口号：默认3306

还可以提供以下信息：
密码：用于连接数据库的用户的密码。
默认库：是要连接的数据库。可以将其留空，然后使用use database命令选择。
```

- 步骤6.单击本地数据库连接以连接到MySQL数据库服务器。因为我们在上一步中没有提供密码，所以MySQL要求我们输入root帐户的密码。
通过窗口的布局，显示或隐藏一些窗口，Navigator 左边栏可以通过"Administration"、"Schemas" 切换更多的可视：

- 步骤7.通过选择File > Open SQL Script  或按Ctrl+Shift+O键盘快捷键打开SQL脚本。

- 步骤8.通过选择C:\temp\mysqldemo.sql

- 步骤10.要执行SQL脚本，请单击execute工具栏中的按钮

- 步骤11.右键单击Schemas面板内部，然后单击Refresh All按钮以更新面板。

```
use mysqldemo;
show tables;
查看数据库中的表列表
```