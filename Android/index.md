# Android 学习路线

### 基础

- 四大组件(Activity, Service, ContentProvider, BroadCastReceiver)

### Activity

### Service

### ContentProvider

### BroadCastReceiver



### Application

组件持有的Context



### View基础知识

- View工作原理
- View滑动
- View多触点
- 常用控件


### UI

- 动画

- 交互提示

- 导航栏



### 资源

- 布局单位

- 主题

- Drawable



### 屏幕适配



### 线程


### 文件流