# ViewModel

- 它是介于View（视图）和Model（数据模型）之间的桥梁
- 使视图和数据能够分离，也能保持通信

Model  <->  ViewModel  <->  View

### ViewModel之前出现的问题：

- 瞬态数据丢失
- 异步调用的内存泄露
- 类膨胀提高维护难度和测试难度

### ViewModel作用

- 它是介于View（视图）和Model（数据模型）之间的桥梁
- 使视图和数据能够分离，也能保持通信

### ViewModel的生命周期

ViewModel的生命周期长于Activity的生命周期，因此在使用ViewModel时，不要向ViewModel传入Activity的Context，因为这样会导致内存泄露！


如果硬要使用Context，那就使用AndroidViewModel中的Application


### LiveData

- MutableLiveData<Int> 对应的LiveData集合
- ObservableField<User> 对应单个字段


### LiveData的优势

确保界面符合数据状态
不会发生内存泄露
不会因Activity停止而导致崩溃
不再需要手动处理生命周期
数据始终保持最新状态
适当的配置更改
共享资源