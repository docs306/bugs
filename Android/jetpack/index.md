# Jetpack介绍

Jetpack: 是由多个库组成的套件，可帮助开发者遵循规范化的做法，减少样板代码并编写可在各版本和设备中一致的代码，让开发者集中精力编写业务代码。

## 1.为何使用Jetpack？

- 遵循最佳做法
组件采用最新的设计方法构建，具有向后兼容性，可以减少崩溃和内存泄露，

- 消除样板代码。
可以管理各种繁琐的Activity（如：后台任务、导航和生命周期管理），以便你可以专注于打造出色的应用。

- 减少不一致
这些库可在各种Android 版本和设备中以一致的方式运作，助你降低复杂性！


## Jetpack与AndroidX

- AndroidX命名空间中包含Android Jetpack库
- AndroidX代替Android Support Library
- AAC（Android Architecture Component） 中的组件并入AndroidX
- 其他一些需要频繁更新和迭代的特性也并入了AndroidX


## 组件

- LifeCycle
- LiveData
- ViewModel
- Navigation
- DataBinding
- Room
- Paging
- WorkManager