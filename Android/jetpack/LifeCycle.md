# LifeCycle应用

- 使用Lifecycle解耦页面与组件
- 使用LifecycleService解耦Service与组件
- 使用ProcessLifecycleOwner监听应用程序生命周期

### LifeCycle的好处
通过一下几个例子能够发现：

- 帮助开发者建立可感知生命周期的组件
- 组件在其内部管理自己的生命周期，从而降低模块耦合度，降低内存泄露发生的可能性
- Activity、Fragment、Service、Application均有LifeCycle支持


### 示例

- 1.自定义计时器控件

```kotlin
@SuppressLint("ViewConstructor")
class MyTimer : Chronometer, LifecycleObserver {
    constructor(context: Context, attr: AttributeSet) : super(context, attr)

    var elapsedTime: Long = 0

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun startMeter() {
        base = SystemClock.elapsedRealtime() - elapsedTime
        start()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE) // 
    open fun stopMeter() {
        elapsedTime = SystemClock.elapsedRealtime() - base
        stop()
    }
}

...
// 使用
class DemoActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
	        super.onCreate(savedInstanceState)
	        setContentView(R.layout.activity_main)
	        val chronometer = findViewById(R.id.chronometer)
	        lifecycle.addObserver(chronometer!!)
	    }
	}
}

```
开始与暂停计时分别在onResume与onPause方法



### 2.LifecycleService
```

public class LifecycleService extends Service implements LifecycleOwner {
   ...略
}

class MyLocationService : LifecycleService {

    constructor() {
        val observer = MyLocationObserver(this)
        lifecycle.addObserver(observer)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    private fun startLocation() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun stopLocation() {}
}

startService(Intent(this, MyLocationService::class.java))
stopService(Intent(this, MyLocationService::class.java))

```


### 3.自定义MyApplication

```
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle
            .addObserver(ApplicationObserver())
    }
}


class ApplicationObserver : LifecycleObserver {
    private val tag = "ApplicationObserver"

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        Log.d(tag, "Lifecycle.Event.ON_CREATE")
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        Log.d(tag, "Lifecycle.Event.ON_START")
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        Log.d(tag, "Lifecycle.Event.ON_RESUME")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        Log.d(tag, "Lifecycle.Event.ON_PAUSE")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        Log.d(tag, "Lifecycle.Event.ON_STOP")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        Log.d(tag, "Lifecycle.Event.ON_DESTROY")
    }
}

```

### 注意事项
这里的ProcessLifecycleOwner.get().lifecycle

针对整个应用程序的监听，与activity数量无关！
- Lifecycle.Event.ON_CREATE只会调用一次。
- Lifecycle.Event.ON_DESTROY永远不会被调用，Google不可能让开发者在这搞事情
