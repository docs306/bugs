# DataBinding

### DataBinding有什么作用呢？

让布局文件承担了部分原本属于页面的工作，使页面与布局耦合度进一步降低！

需要在对应AppModule的build.gradle开启dataBinding功能：
```
android {
    //...略
    defaultConfig {
	//...略
	dataBinding {
	//需要开启dataBinding功能
        enabled = true
    }
  }
}
```

布局改造
```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <data>
    
    </data>
    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        ...略

</layout>
```

布局里面多了一对<data>...</data>标签。
在这队标签里面将会放与布局相关的实体类以及辅助显示类。


数据

```
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <data>
    	<variable
            name="userInfo"  
            type="com.hqk.databinding.UserInfo" />
    </data>
    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        ...略

</layout>
```

使用了 variable 标签将对应的实体类与布局相互绑定；
- name 这个顾名思义，就是定义对应的别名，在布局里以及在逻辑代码里，将通过这个别名相互绑定数据
- type 这个表示，对应实体类的具体位置


辅助类使用

```
class StarUtils {
    companion object {

        @JvmStatic
        fun getStar(star: Int): String? {
            when (star) {
                1 -> return "一星"
                2 -> return "二星"
                3 -> return "三星"
                4 -> return "四星"
                5 -> return "五星"
            }
            return ""
        }
    }
}

	<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@{StarUtils.getStar(userInfo.star)}"
        tools:text="五星" />

	<TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@={userViewModel.userName}"
        tools:text="姓名" />

```

- 注意：当布局调用对应方法时，它是通过Java的方式，并非Kotlin方式，所以想要达到直接静态点出来的效果（不想出现companion关键字），需要在对应方法上加@JvmStatic注解。


@{xxx}  单向绑定数据

@={xxx} 双向绑定数据





