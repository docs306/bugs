Fragment 介绍


### 创建



- 静态添加fragment时需要注意两点：
1、name 字段填写的是要加载的Fragment类，继承自Fragment
2、id   属性提供唯一 ID。必须加上去，否则会报错

- 注意：使用这种方式添加Fragment,不能在运行时删除。
- 重启Activity时，系统可使用该标识符来恢复Fragment，从而执行某些事务，如将其移除

```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <fragment
        android:id="@+id/test_fragment"
        android:name="com.example.TestFragment"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />

</LinearLayout>

```

### 生命周期：

```
onAttach() 此Fragment已与Activity关联时调用（Activity传递到此方法内）

onCreate() 系统会在创建Fragment时调用此方法。当Fragment经历暂停或停止状态继而恢复后，如果您希望保留此Fragment的基本组件，则应在您的实现中将其初始化。

onCreateView() 系统会在Fragment首次绘制其界面时调用此方法。如要为您的Fragment绘制界面，您从此方法中返回的 View 必须是片段Fragment的根视图。如果片段未提供界面，您可以返回 null。

onActivityCreated()  当 Activity 的 onCreate() 方法已返回时进行调用。

onStart() 当Fragment 对用户可见时调用。这通常与Activity.onStart包含活动的生命周期有关

onResume() 当Fragment可见时调用

onPause() 系统会将此方法作为用户离开Fragment的第一个信号（但并不总是意味着此片段会被销毁）进行调用。 通常，您应在此方法内确认在当前用户会话结束后仍然有效的任何更改（因为用户可能不会返回）。

onStop() 当 Fragment 不再启动时调用。这通常与Activity.onStop包含活动的生命周期有关。

onDestroyView() 在移除与Fragment关联的视图层次结构时进行调用。

onDestroy() 在移除与Fragment关联的视图层次结构时进行调用。

onDetach() 在取消Fragment与 Activity 的关联时进行调用。

```

### 动态添加

```
动态添加分为以下几步:

1、添加一个Fragment的容器，比如FranmeLayout

2、拿到FragmentManager
FragmentManager fragmentManager = getSupportFragmentManager();

3、通过FragmentManager拿到FragmentTransaction
FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

4、实例化需要添加的Fragment
TestFragment testFragment = new TestFragment();

5、第一个参数是 ViewGroup，即应放置片段的位置，由资源 ID 指定，第二个参数是要添加的片段。
fragmentTransaction.add(R.id.fl_container, testFragment);

6、提交，通过 FragmentTransaction 做出了更改，就必须调用 commit() 以使更改生效。
fragmentTransaction.commit();

```

### FragmentManager Api

```

add() 将Fragment添加到Activity 如果多次调用add()没有调用remove()或者hide()函数的话则多个fragment会覆盖在一起。

hide() 隐藏添加的Fragment。

remove() 删除添加的Fragment。

replace() 往某个容器替换Fragment。

show() 显示以前隐藏的Fragment。

commit() 将事务提交。

attach() 会将view从UI中移除,和remove()不同,此时fragment的状态依然由FragmentManager维护。

detach() 重建view视图，附加到UI上并显示。

addToBackStack() 将事务添加到后台堆栈。这就意味着事务在提交之后将在堆栈里面，并切返回的时候将会从堆栈退出

```

### 传递和接收数据

```
TestFragment testFragment = new TestFragment();
// 传值
Bundle bundle = new Bundle();
bundle.putString("test", "test");
testFragment.setArguments(bundle);

// 获取值
Bundle arguments = getArguments();
if (arguments != null) {
    String result = arguments.getString("test");
    Log.d(TAG, "onViewCreated: " + result);
}

```



