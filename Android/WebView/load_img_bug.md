# H5打不开手机本地图片

### 1：先将访问本地存储的权限打开
### 2：设置webview的访问本地属性属性

```js
WebSettings webSettings=webView.getSettings();
//允许webview对文件的操作
webSettings.setAllowUniversalAccessFromFileURLs(true);
webSettings.setAllowFileAccess(true);
webSettings.setAllowFileAccessFromFileURLs(true);
```

### 3：从android访问的图片路径应该是：(注意一定是 file:/// 开头)
String path= "file://"+Environment.getExternalStorageDirectory()+"/xxx.jpg";