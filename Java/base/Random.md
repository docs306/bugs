Random.md

Random类中实现的随机算法是伪随机，也就是有规则的随机。在进行随机时，随机算法的起源数字称为种子数(seed)，在种子数的基础上进行一定的变换，从而产生需要的随机数字。

相同种子数的Random对象，相同次数生成的随机数字是完全相同的。也就是说，两个种子数相同的Random对象，第一次生成的随机数字完全相同，第二次生成的随机数字也完全相同。这点在生成多个随机数字时需要特别注意

### 构造方法：

2. public Random()
该构造方法使用一个和当前系统时间对应的相对时间有关的数字作为种子数，然后使用这个种子数构造Random对象。

2. public Random(long seed)
该构造方法可以通过制定一个种子数进行创建。

强调：种子数只是随机算法的起源数字，和生成的随机数字的区间无关


### Random()方法摘要：

- int next(int bits)：生成下一个伪随机数。
boolean nextBoolean()：返回下一个伪随机数，它是取自此随机数生成器序列的均匀分布的boolean值。
- void nextBytes(byte[] bytes)：生成随机字节并将其置于用户提供的 byte 数组中。
- double nextDouble()：返回下一个伪随机数，它是取自此随机数生成器序列的、在0.0和1.0之间均匀分布的 double值。
- float nextFloat()：返回下一个伪随机数，它是取自此随机数生成器序列的、在0.0和1.0之间均匀分布float值。
- double nextGaussian()：返回下一个伪随机数，它是取自此随机数生成器序列的、呈高斯（“正态”）分布的double值，其平均值是0.0标准差是1.0。
- int nextInt()：返回下一个伪随机数，它是此随机数生成器的序列中均匀分布的 int 值。
- int nextInt(int n)：返回一个伪随机数，它是取自此随机数生成器序列的、在（包括和指定值（不包括）之间均匀分布的int值。
- long nextLong()：返回下一个伪随机数，它是取自此随机数生成器序列的均匀分布的 long 值。
- void setSeed(long seed)：使用单个 long 种子设置此随机数生成器的种子。

##### 例子：

生成[0,1.0)区间的小数：
```
double d1 = r.nextDouble();
```

生成[0,5.0)区间的小数：
```
double d2 = r.nextDouble() * 5;
```

生成[1,2.5)区间的小数：
```
double d3 = r.nextDouble() * 1.5 + 1;
```

生成-231到231-1之间的整数：
```
int n = r.nextInt();
```

生成[0,10)区间的整数：
```
int n2 = r.nextInt(10); //方法一
n2 = Math.abs(r.nextInt() % 10); //方法二
```


### Math类中的random方法

其实在Math类中也有一个random方法，该random方法的工作是生成一个[0,1.0)区间的随机小数。

通过阅读Math类的源代码可以发现，Math类中的random方法就是直接调用Random类中的nextDouble方法实现的。

只是random方法的调用比较简单，所以很多程序员都习惯使用Math类的random方法来生成随机数字。

