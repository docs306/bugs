# Netty

### Netty

https://netty.io/


- Netty是一个Java NIO技术的开源异步事件驱动的网络编程框架，用于快速开发可维护的高性能协议服务器和客户端。

- 往通俗了讲，可以将Netty理解为：一个将Java NIO进行了大量封装，并大大降低Java NIO使用难度和上手门槛的超牛逼框架。