# Nginx

### Nginx 

https://nginx.org/

Nginx 是开源、高性能、高可靠的 Web 和反向代理服务器，而且支持热部署，同时也提供了 IMAP/POP3/SMTP 服务，可以不间断运行，提供热更新功能。占用内存少、并发能力强，最重要的是，Nginx 是免费的并可以商业化，配置使用都比较简单。


### 下载并解压安装包

##### 创建文件夹
mkdir nginx
cd nginx

##### 下载tar包
wget https://nginx.org/download/nginx-1.25.3.tar.gz

解压包
tar -xvf nginx-1.25.3.tar.gz

##### 进入目录
cd nginx-1.25.3

##### 一键安装依赖
yum -y install gcc zlib zlib-devel pcre-devel openssl openssl-devel

##### 执行命令
./configure

##### 执行make命令
make

##### 执行make install命令
make install

##### 新配置SSL模块

- 会在/usr/local/nginx 创建一个nginx

./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module

##### 下载OpenSSL库（没报错可忽略该步骤）
yum -y install openssl openssl-devel

##### 进入文件夹： 
cd /usr/local/nginx/sbin

##### 执行make命令
make

##### 启动springboot
nohup java –jar xxx.jar &命令让程序以后台运行的方式执行。
日志会被重定向到nohup.out文件中。可以用“>filename 2>&1” 来更改缺省的重定向文件名：

$ nohup java -jar hubing.jar >spring.log 2>&1 &

使用“>spring.log 2>&1”参数将系统的运行日志保存到spring.log中

 ps -ef | grep java 查看java进程
 kill -9 12577  杀死进程 (12577为进程id)

### 命令：
```sh
./nginx 			# 启动
./nginx -s stop     # 关闭
./nginx -s reload 	# 重启
./nginx -h 			# 查看帮助
./nginx -s reload  	# 向主进程发送信号，重新加载配置文件，热重启
./nginx -s reopen	# 重启 Nginx
./nginx -s quit    	# 等待工作进程处理完成后关闭
./nginx -T         	# 查看当前 Nginx 的配置
./nginx -t -c     	# 检查配置是否有问题，如果已经在配置目录，则不需要-c
```

允许 Nginx 的流量通过防火墙:
$ sudo ufw allow 'Nginx Full'

查看防火墙状态和规则:
$ sudo ufw status

查看nginx进程是否启动
$ ps -ef | grep nginx

### 命令操作
systemctl 是 Linux 系统应用管理工具 systemd 的主命令，用于管理系统，
也可以用它来对 Nginx 进行管理：

启动:
```sh`
$ sudo systemctl start nginx
```

停止:
```sh`
$ sudo systemctl stop nginx
```

开机自启:
```sh`
$ sudo systemctl enable nginx
$ sudo systemctl disable nginx
```

重新加载:
```sh`
$ sudo systemctl reload nginx
```

重启:
```sh`
$ sudo systemctl restart nginx
```

查看状态:
```sh`
$ sudo systemctl status nginx
```

### 系统防火墙
```sh
systemctl start firewalld  # 开启防火墙
systemctl stop firewalld   # 关闭防火墙
systemctl status firewalld # 查看防火墙开启状态，显示running则是正在运行
firewall-cmd --reload      # 重启防火墙，永久打开端口需要reload一下

# 添加开启端口，--permanent表示永久打开，不加是临时打开重启之后失效
firewall-cmd --permanent --zone=public --add-port=8888/tcp

# 查看防火墙，添加的端口也可以看到
firewall-cmd --list-all

```

### nginx.conf 结构图


├── events  # 配置影响 Nginx 服务器或与用户的网络连接
├── http    # 配置代理，缓存，日志定义等绝大多数功能和第三方模块的配置
│   ├── upstream # 配置后端服务器具体地址，负载均衡配置不可或缺的部分
│   ├── server   # 配置虚拟主机的相关参数，一个 http 块中可以有多个 server 块
│   ├── server
│   │   ├── location  # server 块可以包含多个 location 块，location 指令用于匹配 uri
│   │   ├── location
│   │   └── ...
│   └── ...
└── ...

##### 语法规则：

- 配置文件由指令与指令块构成；
- 每条指令以 ; 分号结尾，指令与参数间以空格符号分隔；
- 指令块以 {} 大括号将多条指令组织在一起；
- include 语句允许组合多个配置文件以提升可维护性；
- 使用 # 符号添加注释，提高可读性；
- 使用 $ 符号使用变量；
- 部分指令的参数支持正则表达式；

##### 指令后面：

- = 精确匹配路径，用于不含正则表达式的 uri 前，如果匹配成功，不再进行后续的查找；
- ^~ 用于不含正则表达式的 uri 前，表示如果该符号后面的字符是最佳匹配，采用该规则，不再进行后续的查找；
- ~ 表示用该符号后面的正则去匹配路径，区分大小写；
- ~* 表示用该符号后面的正则去匹配路径，不区分大小写。跟 ~ 优先级都比较低，如有多个location的正则能匹配的话，则使用正则表达式最长的那个；

如果 uri 包含正则表达式，则必须要有 ~ 或 ~* 标志


##### 全局变量
Nginx 有一些常用的全局变量，你可以在配置的任何位置使用它们，如下表：
```sh
# 全局变量名	 		功能
$host				请求信息中的 Host，如果请求中没有 Host 行，则等于设置的服务器名，不包含端口
$request_method		客户端请求类型，如 GET、POST
$remote_addr		客户端的 IP 地址
$args				请求中的参数
$arg_PARAMETER		GET 请求中变量名 PARAMETER 参数的值，例如：$http_user_agent(Uaer-Agent 值), $http_referer...
$content_length		请求头中的 Content-length 字段
$http_user_agent	客户端agent信息
$http_cookie		客户端cookie信息
$remote_addr		客户端的IP地址
$remote_port		客户端的端口
$http_user_agent	客户端agent信息
$server_protocol	请求使用的协议，如 HTTP/1.0、HTTP/1.1
$server_addr		服务器地址
$server_name		服务器名称
$server_port		服务器的端口号
$scheme				HTTP 方法（如http，https）

```