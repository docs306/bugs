deploy.md

运行部署

以往我们开发部Web项目时非常繁琐，而使用Spring Boot开发部署一个命令就能解决，不需要再关注容器的环境问题，专心写业务代码即可。

Spring Boot内嵌的内置Tomcat、Jetty等容器对项目部署带来了更多的改变，在服务器上仅仅需要几条命令即可部署项目。一般开发环境直接java -jar命令启动，正式环境需要将程序部署成服务。下面开始演示Spring Boot项目是如何运行、部署的。

1. 启动运行

简单就是直接启动jar包。启动jar包命令如下：

java -jar spring-boot-package-1.0.0.jar
复制
这种方式是前台运行的，只要将控制台关闭，服务就会停止。实际生产中我们肯定不会前台运行，一般使用后台运行的方式来启动。

nohup java -jar spring-boot-package-1.0.0.jar &
复制
上面的示例中，使用nohup java –jar xxx.jar &命令让程序以后台运行的方式执行。日志会被重定向到nohup.out文件中。也可以用“>filename 2>&1”来更改缺省的重定向文件名。命令如下：

nohup java -jar spring-boot-package-1.0.0.jar >spring.log 2>&1 &
复制
上面的示例中，使用“>spring.log 2>&1”参数将系统的运行日志保存到spring.log中。

以上就是简单的启动jar包的方式，使用简单。

Spring Boot支持在启动时添加定制，比如设置应用的堆内存、垃圾回收机制、日志路径等。

（1）设置jvm参数

通过设置jvm的参数，优化程序的性能。

java -Xms10m -Xmx80m -jar spring-boot-package-1.0.0.jar
复制
（2）选择运行环境

在配置多运行环境一节中，介绍了如何配置多运行环境。那么在启动项目时，选择对应的启动环境即可：

一般项目打包时指定默认的运行环境，在启动运行时也可以再次设置运行的环境。

2. 生产环境部署

上一节介绍的运行方式比较传统和简单的，实际生产环境中考虑到后期的运维，建议大家使用服务的方式来部署。

下面通过示例演示Spring Boot项目配置成系统服务。

步骤1：将之前的jar包
spring-boot-package-1.0.0.jar复制到/usr/local/目录下。

步骤2：进入服务文件目录，命令如下：

cd /etc/systemd/system/
复制
步骤3：使用vim springbootpackage.service创建服务文件，示例代码如下：

[Unit]

Description=springbootpackage

After=syslog.target

[Service]

ExecStart=/usr/java/jdk1.8.0_221-amd64/bin/java -Xmx4096m -Xms4096m -Xmn1536m -jar /usr/local/spring-boot-package-1.0.0.jar

[Install]

WantedBy=multi-user.target
复制
上面的示例中，主要是定义服务的名字，以及启动的命令和参数。使用只要修改Description和ExecStart即可。

步骤4：启动服务

// 启动服务

systemctl start springbootpackage

// 停止服务

systemctl stop springbootpackage

// 查看服务状态

systemctl status springbootpackage

// 查看服务日志

journalctl -u springbootpackage
复制
上面的命令，通过systemctl start|stop|status springbootpackage命令启动、停止创建的springbootpackage服务。

如上图所示就是使用systemctl status springbootpackage命令查看服务状态，同时还可以通过journalctl -u springbootpackage命令查看服务完整日志。

此外，还需要设置服务开机启动，使用如下命令：

// 开机启动

systemctl enable springbootpackage
复制
以上是打包成独立的jar包部署到服务器。如果是部署到Tomcat中，就按照Tomcat的相关命令来重新启动