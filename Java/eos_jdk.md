


1.下载jdk
$：wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.tar.gz

2.解压jdk
$：tar -zxvf jdk-17_linux-x64_bin.tar.gz

3.移动目录 jdk转移到 /usr/local/
$：sudo mv jdk-17.0.9 /usr/local/

4.配置环境变量
$：sudo vim /etc/profile

按 i 建开始编辑，
光标移到文件最后一行输入如下：(回车)

export JAVA_HOME=/usr/local/jdk-17.0.9      (回车)
export CLASSPATH=$JAVA_HOME/lib:$PATH       (回车)
export PATH=$JAVA_HOME/bin:$PATH            (回车)

按Esc 输入 :wq退出

5.配置环境生效
$：source /etc/profile


卸载jdk

1.查看是否有自带的JDK：
$: rpm -qa | grep jdk  or(rpm -qa | grep Java)


2.删除jdk 文件
$: rpm -e --nodeps xxx