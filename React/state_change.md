

监听states 变化

16.x 之前使用componentWillReceiveProps
componentWillReceiveProps (nextProps){
  if(this.props.visible !== nextProps.visible){
      //props 值改变做的事
  }
}
注意:有些时候componentWillReceiveProps在 props 值未变化也会触发,因为在生命周期的第一次render后不会被调用，但是会在之后的每次render中被调用 = 当父组件再次传送props

16.x 之后使用getDerivedStateFromProps

static getDerivedStateFromProps (nextProps){
    console.log('变化执行')
    return{
      changeFlag:'state 值变化执行'
    }
  }