React 事件系统


## 事件委托

把多个子元素的同一类型的监听逻辑，
合并到父元素上通过一个监听函数来管理的行为。


1. React事件系统是如何工作的
当事件在具体的DOM节点上被触发后最终都会冒泡到document上document上所绑定的统一事件处理程序会将事件分发到具体的组件实例

2. react合成事件

合成事件是React自定义事件，它符合W3C规范，在底层抹平了不同浏览器的差异，
在上层面向开发者暴露统一的，稳定的，与dom原生事件相同的事件接口，
虽然合成事件不是原生dom事件，但它保存了原生DOM事件的引用 

当想获取原生DOM属性可以通过 e.nativeEvent 获取

```
onClickEvent(e) {
	console.log(e.nativeEvent)
}
<button onClick={onClickEvent }>点击</button>

```

## React 事件系统工作流拆解

#### 1.事件绑定
事件的绑定是在completeWork中完成

1. completeWork 有三个关键动作：
- 创建DOM节点 createInstance
- 将dom节点插入到DOM树中 appendAllChildren
- 为DOM节点设置属性 finalizeInitialChildren

2. 工作流程
completeWork 
-> createInstance创建DOm节点，appendAllChildren将dom节点插入dom树 
-> finalizeInitialChildren设置DOM节点的属性 
-> setInitialProperties 
-> setInitialDomProperties设置DOM节点的初始化属性 
-> ensureListeningTo进入事件监听的注册流程 
-> legacy ListenToTopLevelEvent分发事件监听的注册逻辑 
-> 判断是捕获还是冒泡 ->事件需要捕获 ->trapCapturedEvent->addTrappedEventListener将事件注册到document上
					->事件需要冒泡 -> trapBubbleEvent->addTrappedEventListener将事件注册到document上

即便在React项目中多次调用了对同一事件的监听，也只会在document上触发一次注册

原因：因为React最注册到document上的并不是某一个DOM节点上对应的具体回调逻辑，而是一个统一的事件分发函数。

```
dispatchDiscreteEvent(...) {
	flushDiscreteUpdatesIfNeeded(...)
	discreteUpdates(...)
}

```
主要函数：
- dispatchDiscreteEvent
- dispatchUserBlockingUpdate
- dispatchEvent

最后绑定到document上的这个统一的事件分发函数，其实就是dispatchEvent