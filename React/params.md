# React项目页面之间的传值方式


### query
query方式类似于表单中的get方法，传递参数为明文

// 传参
this.props.history.push({
    pathname: '/select',
    query: {userTaskId: userTaskId},
});
// 使用
let {location} = this.props.history;
console.log(location.query.userTaskId)


### state
state方式类似于post方式，和query类似：

// 传参
this.props.history.push({
    pathname: '/select',
    state: {userTaskId: userTaskId},
});
// 使用
let {location} = this.props.history;
console.log(location.state.userTaskId)

### props.params

props.params方式是指定一个格式的path，然后指定通配符可以携带参数到指定的path：
javascript复制代码// 定义路由（定义必传的字段名）
<Route
    exact
    path='/report/:userTaskId'
    component={asyncLoad(() =>
      import(
        /* webpackChunkName:"../pages/report/index" */
        '../pages/report/index'
      )
    )}
/>

// 传参
this.props.history.push({
    pathname: `/select/${userTaskId}`
});
// 使用
let {params} = this.props.match；
console.log(params.userTaskId)


### 利用URL的search进行传值
上面几种方式中query和state的方式值都是存在内存中的，一旦刷新页面数据就没了，这样肯定是不利于查看页面数据的。那除了利用props.params这种方式将参数持久化存在url中，还有没有别的方式呢？
答案肯定是有的，在不利用react-router的方法在页面间传递参数的情况下，可以用url的search传递：
awk复制代码http://webapp.leke.cn/leke-ai-pad/#/operation?age=1
不过需要注意的是，这种方式传值时因为是放在hash后面的，直接用location.search是获取不到?后面的search参数的，需要取得hash值之后截取。然后我们需要传递一个JSON对象时要先转码，再解码，因为可能有中文，要保持编码一致性。


### 利用localStorage和sessionStorage进行传值
这种主要对于传递比较大的数据时，因为URL长度有限制（谷歌浏览器是8182个字符），所以对于字符数较多的用浏览器的本地存储会比较安全，不同浏览器容量不同，最低也比URL大一些的。



### params传参
优点：刷新页面，参数不丢失

缺点：1.只能传字符串，传值过多url会变得很长 2. 参数必须在路由上配置

{ path: '/detail/:id/:name', component: Detail },


### 路由跳转与获取路由参数

import { useHistory,useParams } from 'react-router-dom';
const history = useHistory();
// 跳转路由   地址栏：/detail/2/zora
history.push('/detail/2/zora')
// 获取路由参数
const params = useParams()  
console.log(params) // {id: "2",name:"zora"}



### search传参
优点：刷新页面，参数不丢失

缺点：只能传字符串，传值过多url会变得很长，获取参数需要自定义hooks

路由配置
{ path: '/detail', component: Detail },


import { useHistory } from 'react-router-dom';
const history = useHistory();
// 路由跳转  地址栏：/detail?id=2
history.push('/detail?id=2')  
// 或者
history.push({pathname:'/detail',search:'?id=2'})

/**
* 自定义hooks用于获取路由参数
* IE11及以下浏览器 不支持浏览器内置的URLSearchParams API
**/
function useQuery() {
  return new URLSearchParams(useLocation().search);
}
const query = useQuery()
const id = query.get('id') //2

/**
    自定义hooks
*/
import { useLocation } from 'react-router-dom';
import qs from 'query-string';

export function useQuery<T = any>(): T {
  const { search } = useLocation();
  return (qs.parse(search) as unknown) as T;
}
const query = useQuery<IRouteQuery>();
const {id} = query

### state传参🙌
优点：可以传对象

缺点： <HashRouter>刷新页面，参数丢失


{ path: '/detail', component: Detail },

路由跳转与获取路由参数
import { useHistory,useLocation } from 'react-router-dom';
const history = useHistory();
const item = {id:1,name:"zora"}
// 路由跳转
history.push(`/user/role/detail`, { id: item });
// 参数获取
const {state} = useLocation()
console.log(state)  // {id:1,name:"zora"}


重点 🏁 <HashRouter> 不支持 location.key 与 location.state，<HashRouter>通过state传递参数，刷新页面后参数丢失，官方建议使用<BrowserRouter>，<BrowserRouter>页面刷新参数也不会丢失。




