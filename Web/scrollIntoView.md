# scrollIntoView

scrollIntoView()方法存在于所有 HTML 元素上，可以滚动浏览器窗口或容器元素以便包含元 素进入视口。

alignToTop 是一个布尔值。
1. true:窗口滚动后元素的顶部与视口顶部对齐。  false:窗口滚动后元素的底部与视口底部对齐。
2. scrollIntoViewOptions 是一个选项对象。
3. behavior:定义过渡动画，可取的值为"smooth"和"auto"，默认为"auto"。
4. block:定义垂直方向的对齐，可取的值为"start"、"center"、"end"和"nearest"，默
认为 "start"。
5. inline:定义水平方向的对齐，可取的值为"start"、"center"、"end"和"nearest"，默
认为 "nearest"。
6. 不传参数等同于 alignToTop 为 true。
来看几个例子:
```js
// 确保元素可见 document.forms[0].scrollIntoView();
// 同上
document.forms[0].scrollIntoView(true); document.forms[0].scrollIntoView({block: 'start'});
// 尝试将元素平滑地滚入视口
document.forms[0].scrollIntoView({behavior: 'smooth', block: 'start'});
```
这个方法可以用来在页面上发生某个事件时引起用户关注。把焦点设置到一个元素上也会导致浏览 器将元素滚动到可见位置。
