

eventBind() {
    this.removeEvent();
    this.addEventListener("touchstart", this.onTouchStart);
    this.addEventListener("touchmove", this.onTouchMove);
    this.addEventListener("touchend", this.onTouchEnd);
    this.addEventListener("touchcancel", this.onTouchEnd);
  }

  removeEvent = () => {
    this.removeEventListener("touchstart", this.onTouchStart);
    this.removeEventListener("touchmove", this.onTouchMove);
    this.removeEventListener("touchend", this.onTouchEnd);
    this.addEventListener("touchcancel", this.onTouchEnd);
  };
