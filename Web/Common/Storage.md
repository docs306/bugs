Storage.md

# localStorage/sessionStorage区别:
     
### localStorage

- 的数据可以长期保留
- 一般5MB，各浏览器不同
- 改变时能监听
- 同一浏览器同源域名可以共享
  
### sessionStorage

- 当窗口或者标签页被关闭时会清除
- 5MB
- 改变时能监听
- 同一浏览器(同一窗口/iframe)的同源域名可以共享


### Api

- Storage.length;
- Storage.key(index);
- Storage.setItem(key, data);
- Storage.getItem(key);
- Storage.removeItem(key);
- Storage.clear();


监听数据变化
```

let originalSetItem = sessionStorage.setItem;
sessionStorage.setItem = function(key, newValue){
      const setItemEvent = new Event("setItemEvent");
      setItemEvent.newValue = newValue;
      window.dispatchEvent(setItemEvent);
      originalSetItem.apply(this,arguments);
}
window.addEventListener("setItemEvent", function (e) {
    alert(e.newValue);
});
sessionStorage.setItem("name","wang");
```