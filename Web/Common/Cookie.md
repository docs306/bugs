# Cookie与Session

### Cookie是小甜饼的意思，主要有以下特点：
1、顾名思义，Cookie 确实非常小，它的大小限制为4KB左右
2、主要用途是保存登录信息和标记用户(比如购物车)等，不过随着localStorage的出现，现在购物车的工作Cookie承担的较少了
3、一般由服务器生成，可设置失效时间。如果在浏览器端生成Cookie，默认是关闭浏览器后失效
4、每次都会携带在HTTP头中，如果使用cookie保存过多数据会带来性能问题
5、原生API不如storage友好，需要自己封装函数

用法
```
// 服务端向客户端发送的cookie(HTTP头,不带参数)：Set-Cookie: <cookie-name>=<cookie-value> (name可选)

// 服务端向客户端发送的cookie(HTTP头，带参数)：Set-Cookie: <cookie-name>=<cookie-value>;(可选参数1);(可选参数2)

// 客户端设置cookie：
document.cookie = "<cookie-name>=<cookie-value>;(可选参数1);(可选参数2)"
```

可选参数：
- Expires=<date>：cookie的最长有效时间，若不设置则cookie生命期与会话期相同
- Max-Age=<non-zero-digit>：cookie生成后失效的秒数
- Domain=<domain-value>：指定cookie可以送达的主机域名，若一级域名设置了则二级域名也能获取。
- Path=<path-value>：指定一个URL，例如指定path=/docs，则”/docs”、”/docs/Web/“、”/docs/Web/Http”均满足匹配条件
- Secure：必须在请求使用SSL或HTTPS协议的时候cookie才回被发送到服务器
- HttpOnly：客户端无法更改Cookie，客户端设置cookie时不能使用这个参数，一般是服务器端使用


可选前缀：
- __Secure-：以__Secure-为前缀的cookie，必须与secure属性一同设置，同时必须应用于安全页面（即使用HTTPS）

- __Host-：以__Host-为前缀的cookie，必须与secure属性一同设置，同时必须应用于安全页面（即使用HTTPS）。 必须不能设置domian属性（这样可以防止二级域名获取一级域名的cookie），path属性的值必须为”/“。



### Session

- Session是在无状态的HTTP协议下，服务端记录用户状态时用于标识具体用户的机制。它是在服务端保存的用来跟踪用户的状态的数据结构，可以保存在文件、数据库或者集群中。

- 在浏览器关闭后这次的Session就消失了，下次打开就不再拥有这个Session。其实并不是Session消失了，而是Session ID变了，服务器端可能还是存着你上次的Session ID及其Session 信息，只是他们是无主状态，也许一段时间后会被删除。

- 大多数的应用都是用Cookie来实现Session跟踪的，第一次创建Session的时候，服务端会在HTTP协议中告诉客户端，需要在Cookie里面记录一个SessionID，以后每次请求把这个会话ID发送到服务器


### 与Cookie的关系与区别：
1、Session是在服务端保存的一个数据结构，用来跟踪用户的状态，这个数据可以保存在集群、数据库、文件中，Cookie是客户端保存用户信息的一种机制，用来记录用户的一些信息，也是实现Session的一种方式。

2、Cookie的安全性一般，他人可通过分析存放在本地的Cookie并进行Cookie欺骗。在安全性第一的前提下，选择Session更优。重要交互信息比如权限等就要放在Session中，一般的信息记录放Cookie就好了。 

3、单个Cookie保存的数据不能超过4K，很多浏览器都限制一个站点最多保存20个Cookie。 
4、当访问增多时，Session会较大地占用服务器的性能。考虑到减轻服务器性能方面，应当适时使用Cookie。 

5、Session的运行依赖Session ID，而Session ID是存在 Cookie 中的。也就是说，如果浏览器禁用了Cookie,Session也会失效（但是可以通过其它方式实现，比如在url中传递Session ID,即sid=xxxx）。

