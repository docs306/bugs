EventListener.md

https://developer.mozilla.org/zh-CN/docs/Web/API/Window/devicemotion_event

- 判断刷新或关闭: {load时间 - unload时间} or {unload - beforeunload > 5}

````
window.addEventListener('load', (res) => {
  console.log('load', res)
});

window.addEventListener('blur', (res) => {
  console.log('blur', res)
});

window.addEventListener('focus', (res) => {
  console.log('focus', res)
});
// 刷新/关闭前监听
window.addEventListener('beforeunload', (event) => {
  console.log('beforeunload', event)
  event.preventDefault();
  // Chrome requires returnValue to be set.
  event.returnValue = "";
});
// 关闭监听
window.addEventListener('unload', (res) => {
  console.log('pageshow', res)
});
// 切换到后台/前台 监听
document.addEventListener("visibilitychange", function() {
  console.log('visibilitychange', document.visibilityState)
});
```