### HTML, CSS {#html-css}

- 请描述一下 Cookies，SessionStorage 和 localStorage 的区别
- 知道的网页制作会用到的图片格式有哪些
- 你如何理解 HTML 结构的语义化
- 谈谈以前端角度出发做好 SEO 需要考虑什么?
- BFC 是什么
- css 优先级算法如何计算
- 哪些 css 属性可以继承
- 事件委托，代理，冒泡，捕获
- iframe 的优缺点
- cookie 的缺点
- 如何延迟加载 js
- 哪些操作会造成内存泄露
- jsonp 的原理
- load 和 ready 的区别
- script defer, async, script 标签的位置有啥影响
- Bom 对象
- Dom 节点操作函数
- css link 和 @import 的区别
- 多 tab 通信
- flex布局，垂直居中，水平居中
- css 的解析会影响js的执行吗？css加载会影响吗？
- requestAnimationFrame 和 requestIdleCallback
- svg vs canvas
- 层叠上下文
- margin collapse，外边距合并
- 行内元素，块元素，盒模型


### 浏览器 {#浏览器}

浏览器进程模型，新开一个 tab 会有哪些线程
Compositing Layers 是什么，独立 layers 会有什么好处
聊 Web Workers 与 WebAssembly 技术
SSR, CSR
并行请求优化
Dom 树是怎么生成的
渲染进程及其相应线程
重绘与回流，reflow, relayout, repaint
event loop
宏任务，微任务
缓存原理，策略，强缓存和协商缓存的状态码是多少
三种刷新操作对 http 缓存的影响


### JavaScript 基础 {#javascript-基础}

WeakSet, WeakMap, Set, Map 的区别
闭包，缺点？
call, apply 的区别
基本数据类型
如何实现继承，创建对象的几种方式
垃圾回收机制
浮点数
类数组
作用域，this，作用域链，执行上下文
原型链
eval, with
Symbol, Proxy, Reflect
strict 模式
如何判断对象属于某个类
模块化, AMD、CMD、UMD、Common.js、ESModule
new 操作符做了什么
promise
typeof 类型判断
如何判断是 new 还是函数调用
常用设计模式
柯里化
instanceof 的原理
generator 函数内部是如何实现的, async, await


# 工程化 {#工程化}

### 微前端 {#微前端}

沙箱如何设计
qiankun, single-spa

### 项目 {#项目}

有哪些复杂的点，以及怎么解决的
比较有挑战性的地方
开发过程中遇到困难，如何解决
最具代表的项目

### 移动端 {#移动端}

移动端适配问题，rem和vw有啥区别

### 框架 {#框架}

MVVM, MVC 的区别
Redux， 状态管理,  React-Redux 的原理
前端路由


### React {#react}

谈谈虚拟 dom 的理解
运行原理，执行过程
Vnode 的理解
Fiber 原理
hook 原理，相对于 class 的优势,
为什么需要合成事件
组件间通信
生命周期
受控组件和非受控组件
context
React.memo，手写实现
高阶组件
React中 状态的更新是如何触发视图渲染的
为什么不能在条件语句中写 hook
useEffect 和 useLayoutEffect 区别
React.memo() 和 React.useMemo() 的区别
React.useCallback() 和 React.useMemo() 的区别
React.forwardRef 是什么及其作用
React dom diff 算法
React 性能优化手段


### Vue {#vue}

use 的实现逻辑
nextTick 原理
data 为啥是一个方法
双向绑定，如何实现的
生命周期
Proxy 相对 defineProperty 的优势
vue-router, vuex
vue3, 双端diff

### 性能 {#性能}

如何做性能优化
一个页面上有大量的图片（大型电商网站），加载很慢，你有哪些方法优化这些图片的加载，给用户更好的
如何对网站的文件和资源进行优化
性能优化有哪些指标能参考


### 网络 {#网络}

http, tcp, http 状态码
tcp 可以建立多个连接吗， 连接过程
为什么要三次握手，四次挥手
Get, Post 的区别
https 的握手过程
http2，与 http 1.1 的区别
Websocket
同源策略，跨域, Option 请求
DNS 解析过程


### 安全 {#安全}

xss, csrf

常用工具 {#常用工具}
Webpack {#webpack}

提高构建速度的方式?
原理？
loader 原理，输入什么，产出什么？
plugin 原理
HMR 原理
代码分割
Webpack5 模块联邦

### Babel {#babel}

原理？
插件原理

### Git {#git}

git merge 和 git rebase有什么区别

### Node.js {#node-dot-js}

谈谈对中间件的理解
怎么保证后端服务稳定性，怎么做容灾
require('module') 的流程
event loop

### 技术杂项 {#技术杂项}

为什么利用多个域名来存储网站资源会更有效
从输入 URL 到页面展示发生了什么
前端工程化
最近在研究什么技术
测试驱动开发
代码规范参考来源、如何落实开发代码规范
组件与脚手架工具设计
Code review
项目从开发到上线的流程做的事情
聊比较深入了解前端开源的库


https://juejin.cn/post/7176644710847479869