# IntersectionObserver

监听元素是否显示到屏幕当中。

IntersectionObserver 接口（从属于 Intersection Observer API）提供了一种异步观察目标元素与其祖先元素或顶级文档视口（viewport）交叉状态的方法。其祖先元素或视口被称为根（root）。

监听根中一段给定比例的可见区域。
一旦 IntersectionObserver 被创建，则无法更改其配置，所以一个给定的观察者对象只能用来监听可见区域的特定变化值；
可以在同一个观察者对象中配置监听多个目标元素。
rootMargin 只读
计算交叉时添加到根边界盒 (en-US)的矩形偏移量，可以有效的缩小或扩大根的判定范围从而满足计算需要。此属性返回的值可能与调用构造函数时指定的值不同，因此可能需要更改该值，以匹配内部要求。所有的偏移量均可用像素（px）或百分比（%）来表达，默认值为“0px 0px 0px 0px”。



// 获取你想要监听的元素
        const element = document.querySelector('.image-item3');

        // 创建一个Intersection Observer实例
        const observer = new IntersectionObserver(function (entries) {
          // entries是所有被监听元素的集合
          entries.forEach(function (entry) {
            // 如果元素进入可视区域
            if (entry.isIntersecting) {
              console.log('元素进入可视区域', index);
            } else {
              console.log('元素离开可视区域', index);
            }
          });
        });
        // 开始监听元素
        observer?.observe(e.target);