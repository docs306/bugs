
element.scrollIntoView({behavior: 'smooth', block: 'start'})

滑动当前网页到指定元素，

- behavior: 滑动模式，平滑，跳跃
- block:	  显示在屏幕上方，中间，下方

```js
<div className="dialog-item-box">
	<h1>撒呼呼1</h1>
	<h1>撒呼呼2</h1>
	<h1>撒呼呼3</h1>
	<h1>撒呼呼4</h1>
	<h1>撒呼呼5</h1>
	<h1>撒呼呼6</h1>
	<h1>撒呼呼7</h1>
	<h1>撒呼呼8</h1>
	<h1>撒呼呼9</h1>
	<h1>撒呼呼10</h1>
	<h1>撒呼呼11</h1>
	<h1>撒呼呼12</h1>
	<h1>撒呼呼13</h1>
	<h1>撒呼呼14</h1>
	<h1>撒呼呼15</h1>
	<h1>撒呼呼16</h1>
	<h1>撒呼呼17</h1>
	<h1>撒呼呼18</h1>
	<h1>撒呼呼19</h1>
	<h1 ref={ref3} >撒呼呼</h1>
	<h1>撒呼呼1</h1>
	<h1>撒呼呼3</h1>
	<h1>撒呼呼2</h1>
	<h1>撒呼呼2</h1>
	<h1>撒呼呼4</h1>
	<h1>撒呼呼123</h1>
	<h1>撒呼呼12</h1>
	<h1>撒呼呼1</h1>
	<h1>撒呼呼2</h1>
</div>

ref3.current.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'start'});
```