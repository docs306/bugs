前端页面如何禁止别人调试

1.暴力的可以当控制台打开后就立马通过window.close() 关闭调试窗口；
(() => {
    function block() {
        if (
            window.outerHeight - window.innerHeight > 200 ||
            window.outerWidth - window.innerWidth > 200
        ) {
            document.body.innerHTML =
                "检测到非法调试,请关闭后刷新重试!";
        }
        setInterval(() => {
            (function () {
                return false;
            }
                ["constructor"]("debugger")
                ["call"]());
        }, 50);
    }
    try {
        block();
    } catch (err) {}
})();

2.通过不断debugger的方法来疯狂输出断点,让控制台打开后程序就无法正常执行；

通过将debugger改写成Function("debugger")();

(() => {
    function block() {
        setInterval(() => {
            Function("debugger")();
        }, 50);
    }
    try {
        block();
    } catch (err) {}
})();


3.代码加密混淆；