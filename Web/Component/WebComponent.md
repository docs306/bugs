# WebComponent

官方文档地址：https://developer.mozilla.org/zh-CN/docs/Web/API/Web_components


```js
/**
 * Author: Meng
 * Date: 2023-09-26
 * Modify: 2023-09-26
 * Desc:
 *
 * HTMLElement子类:
 *    HTMLParagraphElement
 *    HTMLUListElement
 *    HTMLTableElement
 *    HTMLInputElement
 *    ...
 */

export default class CustomView extends HTMLParagraphElement {
  constructor() {
    // 必须首先调用 super 方法
    super();

    // 元素的功能代码写在这里
  }

  private create() {
    // 创建一个 shadow root
    const shadow = this.attachShadow({ mode: "open" });

    // 创建一个 spans
    const wrapper = document.createElement("span");
    wrapper.setAttribute("class", "wrapper");
    const icon = document.createElement("span");
    icon.setAttribute("class", "icon");
    icon.setAttribute("tabindex", "0");
    const info = document.createElement("span");
    info.setAttribute("class", "info");

    // 获取 text 属性上的内容，并添加到一个 span 标签内
    let text = this.getAttribute("text");
    info.textContent = text;

    // 插入 icon
    let imgUrl;
    if (this.hasAttribute("img")) {
      imgUrl = this.getAttribute("img") || "";
    } else {
      imgUrl = "img/default.png";
    }
    let img = document.createElement("img");
    img.src = imgUrl;
    icon.appendChild(img);

    // 创建一些 CSS，并应用到 shadow dom 上
    let style = document.createElement("style");

    style.textContent = ".wrapper {}";
    // 简洁起见，省略了具体的 CSS

    // 将创建的元素附加到 shadow dom

    shadow.appendChild(style);
    shadow.appendChild(wrapper);
    wrapper.appendChild(icon);
    wrapper.appendChild(info);

  }
}


customElements.define("custom", CustomView);
```
